-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 19, 2022 at 02:23 AM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 7.4.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tokomasyhur`
--

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `id` int(11) NOT NULL,
  `kode` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `unit` int(11) NOT NULL,
  `category` int(11) NOT NULL,
  `rak` varchar(255) DEFAULT NULL,
  `image` varchar(255) NOT NULL,
  `stock` varchar(25) NOT NULL,
  `harga_penjualan` varchar(25) NOT NULL,
  `harga_grosir` varchar(225) NOT NULL,
  `min_grosir` varchar(225) NOT NULL,
  `min_stock` varchar(25) NOT NULL,
  `note` text DEFAULT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`id`, `kode`, `nama`, `unit`, `category`, `rak`, `image`, `stock`, `harga_penjualan`, `harga_grosir`, `min_grosir`, `min_stock`, `note`, `created_at`) VALUES
(25, 'SKU0001', 'GAMIS PUTIH', 25, 14, '12', '28248489_B.jpeg', '45', '80000', '75000', '12', '5', 'Gamis Putih Bagus, Bahan Halus', '2022-06-12'),
(26, 'SKU0002', 'SET MELODY GAMIS', 25, 14, '12', 'a.jpeg', '40', '75000', '70000', '12', '5', 'Gamis bagus', '2022-06-12'),
(27, 'SKU0003', 'Kaftan Ateola Maxy', 25, 13, '12', 'aa.jpg', '35', '90000', '88000', '12', '5', 'Kaftan Ateola Maxy Bagus', '2022-06-12'),
(28, 'SKU0004', 'Shinta Kaftan', 25, 13, '12', 'ac40762b-1e60-46ff-8d61-4d2ffbef28be.jpg', '60', '92000', '90000', '12', '5', 'Shinta Kaftan', '2022-06-12'),
(29, 'SKU0005', 'TUNIK JAZIA', 25, 15, '12', 'baju.jpg', '50', '88000', '82000', '12', '5', 'TUNIK JAZIA BAGUS', '2022-06-12'),
(30, 'SKU0006', 'TUNIK HANSA', 25, 15, '12', 'Baju-Muslim-Korea.jpg', '45', '90000', '86000', '10', '5', 'TUNIK HANSA BAGUS', '2022-06-12');

-- --------------------------------------------------------

--
-- Table structure for table `barang_masuk`
--

CREATE TABLE `barang_masuk` (
  `id` int(11) NOT NULL,
  `kode` varchar(25) NOT NULL,
  `barang` int(11) NOT NULL,
  `supplier` int(11) NOT NULL,
  `harga_pembelian` varchar(25) NOT NULL,
  `total_pembelian` varchar(25) NOT NULL,
  `status` int(11) NOT NULL,
  `jumlah` varchar(25) NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang_masuk`
--

INSERT INTO `barang_masuk` (`id`, `kode`, `barang`, `supplier`, `harga_pembelian`, `total_pembelian`, `status`, `jumlah`, `created_at`) VALUES
(74, 'TR906458', 30, 15, '60000', '0', 1, '50', '2022-06-12'),
(75, 'TR594465', 29, 15, '65000', '0', 1, '55', '2022-06-12'),
(76, 'TR974201', 28, 14, '55000', '0', 1, '60', '2022-06-12'),
(77, 'TR182055', 27, 15, '75000', '0', 2, '40', '2022-06-12'),
(78, 'TR698697', 26, 14, '45000', '0', 2, '45', '2022-06-12'),
(79, 'TR151473', 25, 14, '40000', '0', 2, '50', '2022-06-12');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `nama`) VALUES
(13, 'KAFTAN'),
(14, 'GAMIS'),
(15, 'TUNIK');

-- --------------------------------------------------------

--
-- Table structure for table `penjualan`
--

CREATE TABLE `penjualan` (
  `id` int(11) NOT NULL,
  `kode` varchar(25) NOT NULL,
  `user` int(11) NOT NULL,
  `total_penjualan` varchar(25) NOT NULL,
  `bill` varchar(100) NOT NULL,
  `kembalian` varchar(100) NOT NULL,
  `pelanggan` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penjualan`
--

INSERT INTO `penjualan` (`id`, `kode`, `user`, `total_penjualan`, `bill`, `kembalian`, `pelanggan`, `created_at`) VALUES
(93, 'TRX586670-20220612', 1, '890000', '900000', '-10000', 'Pelanggan Terhormat', '2022-06-12 20:12:48');

-- --------------------------------------------------------

--
-- Table structure for table `penjualan_detail`
--

CREATE TABLE `penjualan_detail` (
  `id` int(11) NOT NULL,
  `barang` int(11) NOT NULL,
  `jumlah` varchar(25) NOT NULL,
  `harga_jual` varchar(10) NOT NULL,
  `total` varchar(25) NOT NULL,
  `penjualan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penjualan_detail`
--

INSERT INTO `penjualan_detail` (`id`, `barang`, `jumlah`, `harga_jual`, `total`, `penjualan`) VALUES
(128, 30, '5', '90000', '450000', 93),
(129, 29, '5', '88000', '440000', 93);

-- --------------------------------------------------------

--
-- Table structure for table `rak`
--

CREATE TABLE `rak` (
  `id` int(11) NOT NULL,
  `kode` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `deskripsi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rak`
--

INSERT INTO `rak` (`id`, `kode`, `nama`, `deskripsi`) VALUES
(12, 'RAK0001', 'RAK MELATI', 'RAK MELATI DI SEBELAH POJOK KANAN');

-- --------------------------------------------------------

--
-- Table structure for table `refund`
--

CREATE TABLE `refund` (
  `id` int(11) NOT NULL,
  `kode` varchar(25) NOT NULL,
  `barang_masuk` int(11) NOT NULL,
  `jumlah` varchar(25) NOT NULL,
  `supplier` int(11) NOT NULL,
  `harga_pembelian` varchar(225) NOT NULL,
  `note` text NOT NULL,
  `user` int(11) NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `refund`
--

INSERT INTO `refund` (`id`, `kode`, `barang_masuk`, `jumlah`, `supplier`, `harga_pembelian`, `note`, `user`, `created_at`) VALUES
(34, 'RTR182055-121206062222', 77, '5', 15, '75000', 'Barang Rusak', 1, '2022-06-12'),
(35, 'RTR698697-121206062222', 78, '5', 14, '45000', 'BARANG RUSAK', 1, '2022-06-12'),
(36, 'RTR151473-121206062222', 79, '5', 14, '40000', 'BARANG RUSAK', 1, '2022-06-12');

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `pemilik` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stock`
--

CREATE TABLE `stock` (
  `id` int(11) NOT NULL,
  `kode` varchar(255) NOT NULL,
  `barang_masuk` int(11) NOT NULL,
  `barang` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `supplier` int(11) NOT NULL,
  `harga_pembelian` varchar(25) NOT NULL,
  `harga_penjualan` varchar(25) NOT NULL,
  `amount` int(11) NOT NULL,
  `balance` int(11) NOT NULL,
  `note` text NOT NULL,
  `total_pembelian` varchar(225) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock`
--

INSERT INTO `stock` (`id`, `kode`, `barang_masuk`, `barang`, `user`, `supplier`, `harga_pembelian`, `harga_penjualan`, `amount`, `balance`, `note`, `total_pembelian`, `status`, `created_at`) VALUES
(210, 'STR906458-121206062222', 74, 30, 1, 15, '60000', '90000', 50, 45, 'Kondisi Ok', '3000000', 0, '2022-06-12'),
(211, 'STR594465-121206062222', 75, 29, 1, 15, '65000', '88000', 55, 50, 'Kondisi Ok', '3575000', 0, '2022-06-12'),
(212, 'STR974201-121206062222', 76, 28, 1, 14, '55000', '92000', 60, 60, 'Kondisi Ok', '3300000', 0, '2022-06-12'),
(213, 'STR182055-121206062222', 77, 27, 1, 15, '75000', '90000', 35, 35, 'Kondisi Ok', '2625000', 0, '2022-06-12'),
(214, 'STR698697-121206062222', 78, 26, 1, 14, '45000', '75000', 40, 40, 'Kondisi Ok', '1800000', 0, '2022-06-12'),
(215, 'STR151473-121206062222', 79, 25, 1, 14, '40000', '80000', 45, 45, 'Kondisi Ok', '1800000', 0, '2022-06-12'),
(216, 'OTR201612', 74, 30, 0, 15, '60000', '90000', -5, -5, 'Barang Ok', '450000', 1, '2022-06-12'),
(217, 'OTR137020', 75, 29, 0, 15, '65000', '88000', -5, -5, 'Barang Ok', '440000', 1, '2022-06-12');

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `id` int(11) NOT NULL,
  `kode` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `no_tlp` varchar(15) NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`id`, `kode`, `nama`, `alamat`, `no_tlp`, `created_at`) VALUES
(14, 'SP736600', 'Butik My Kebaya', 'Surakarta', '0813938338383', '2022-06-12'),
(15, 'SP286778', 'Butik Ayana', 'Surakarta', '081383733737', '2022-06-12');

-- --------------------------------------------------------

--
-- Table structure for table `unit`
--

CREATE TABLE `unit` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `unit`
--

INSERT INTO `unit` (`id`, `nama`) VALUES
(25, 'PCS');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `level` int(11) NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `name`, `email`, `password`, `image`, `level`, `created_at`) VALUES
(1, 'admingudang', 'Majidah Hasna', 'admingudang@mail.com', '21232f297a57a5a743894a0e4a801fc3', '', 1, '2020-03-21'),
(2, 'adminkasir', 'Munaroh Somad', 'adminkasir@mail.com', '21232f297a57a5a743894a0e4a801fc3', '', 2, '2020-03-21'),
(3, 'adminowner', 'Siska EE', 'adminowner@mail.com', '21232f297a57a5a743894a0e4a801fc3', '', 3, '2020-03-21');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `unit` (`unit`),
  ADD KEY `category` (`category`),
  ADD KEY `rak` (`rak`);

--
-- Indexes for table `barang_masuk`
--
ALTER TABLE `barang_masuk`
  ADD PRIMARY KEY (`id`),
  ADD KEY `barang` (`barang`),
  ADD KEY `supplier` (`supplier`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `penjualan`
--
ALTER TABLE `penjualan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user` (`user`);

--
-- Indexes for table `penjualan_detail`
--
ALTER TABLE `penjualan_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `penjualan` (`penjualan`);

--
-- Indexes for table `rak`
--
ALTER TABLE `rak`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `refund`
--
ALTER TABLE `refund`
  ADD PRIMARY KEY (`id`),
  ADD KEY `barang_masuk` (`barang_masuk`),
  ADD KEY `supplier` (`supplier`),
  ADD KEY `user` (`user`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stock`
--
ALTER TABLE `stock`
  ADD PRIMARY KEY (`id`),
  ADD KEY `barang` (`barang`),
  ADD KEY `user` (`user`),
  ADD KEY `barang_masuk``` (`barang_masuk`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `unit`
--
ALTER TABLE `unit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `barang`
--
ALTER TABLE `barang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `barang_masuk`
--
ALTER TABLE `barang_masuk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `penjualan`
--
ALTER TABLE `penjualan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;

--
-- AUTO_INCREMENT for table `penjualan_detail`
--
ALTER TABLE `penjualan_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=130;

--
-- AUTO_INCREMENT for table `rak`
--
ALTER TABLE `rak`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `refund`
--
ALTER TABLE `refund`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stock`
--
ALTER TABLE `stock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=218;

--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `unit`
--
ALTER TABLE `unit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
