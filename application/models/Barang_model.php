<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Barang_model extends CI_Model
{

    public $table = 'barang';
    public $id = 'id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // datatables
    function json()
    {
        $this->db->select('barang.kode as kode, barang.id as id, barang.image as image, barang.min_stock as stock, barang.nama as nama, unit.nama as unit, category.nama as category, rak.nama as rak');
        $this->db->from('barang');
        //add this line for join
        $this->db->join('unit', 'barang.unit = unit.id');
        $this->db->join('category', 'barang.category = category.id');
        $this->db->join('rak', 'barang.rak = rak.id');
        // $this->datatables->add_column('action', anchor(site_url('barang/read/$1'),'Read')." | ".anchor(site_url('barang/update/$1'),'Update')." | ".anchor(site_url('barang/delete/$1'),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'id');
        return $this->db->get()->result();
    }

    function product_bystock()
    {
        $this->db->select('barang.*, unit.nama as unit, category.nama as category, rak.nama as rak');
        $this->db->from('barang');
        //add this line for join
        $this->db->join('unit', 'barang.unit = unit.id');
        $this->db->join('category', 'barang.category = category.id');
        $this->db->join('rak', 'barang.rak = rak.id');
        // $this->datatables->add_column('action', anchor(site_url('barang/read/$1'),'Read')." | ".anchor(site_url('barang/update/$1'),'Update')." | ".anchor(site_url('barang/delete/$1'),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'id');
        return $this->db->get()->result();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    function total_produk_bymonth($month)
    {
        $this->db->where('MONTH(created_at) >=', 1);
        $this->db->where('MONTH(created_at) <=', $month);
        $this->db->select('COUNT(id) as jumlah_produk, SUM(stock) as total_stock');
        return $this->db->from('barang')->get()->row();
    }

    // get data by id
    function get_bykategori($id)
    {
        $this->db->where('category', $id);
        return $this->db->get($this->table)->result();
    }

    function get_data_product_byid($id)
    {

        $this->db->select('b.*, u.nama as unit_name, c.nama as category_name, r.nama as rak_name');
        $this->db->from('barang b');
        $this->db->join('unit u', 'u.id=b.unit', 'left');
        $this->db->join('category c', 'c.id=b.category', 'left');
        $this->db->join('rak r', 'r.id=b.rak', 'left');

        return $this->db->where('b.id', $id);
    }

    function get_data_product_all()
    {

        $this->db->select('b.*, u.nama as unit_name, c.nama as category_name, r.nama as rak_name');
        $this->db->from('barang b');
        $this->db->join('unit u', 'u.id=b.unit', 'left');
        $this->db->join('category c', 'c.id=b.category', 'left');
        return $this->db->join('rak r', 'r.id=b.rak', 'left')->get()->result();
    }

    function get_data_product_bykode($id)
    {

        $this->db->select('b.*, u.nama as unit_name, c.nama as category_name, r.nama as rak_name');
        $this->db->from('barang b');
        $this->db->join('unit u', 'u.id=b.unit', 'left');
        $this->db->join('category c', 'c.id=b.category', 'left');
        $this->db->join('rak r', 'r.id=b.rak', 'left');

        return $this->db->where('b.kode', $id);
    }

    function get_data_relational_byid($id)
    {

        $this->db->select('b.*, b.nama as barang_name, s.nama as supplier_name');
        $this->db->from('barang b');
        $this->db->join('stock st', 'st.barang=b.id', 'left');
        $this->db->join('barang_masuk bm', 'bm.barang=st.barang_masuk', 'left');
        $this->db->join('supplier s', 's.id=bm.supplier', 'left');
        return $this->db->where('st.barang', $id);
    }

    function get_data_relational_all()
    {

        $this->db->select('b.*, b.nama as barang_name, s.nama as supplier_name');
        $this->db->from('barang b');
        $this->db->join('stock st', 'st.barang=b.id', 'left');
        $this->db->join('barang_masuk bm', 'bm.barang=st.barang_masuk', 'left');
        return $this->db->join('supplier s', 's.id=bm.supplier', 'left');
    }

    // get total rows
    function total_rows($q = NULL)
    {
        $this->db->like('id', $q);
        $this->db->or_like('kode', $q);
        $this->db->or_like('nama', $q);
        $this->db->or_like('unit', $q);
        $this->db->or_like('category', $q);
        $this->db->or_like('image', $q);
        $this->db->or_like('stock', $q);
        $this->db->or_like('harga_penjualan', $q);
        $this->db->or_like('min_stock', $q);
        $this->db->or_like('created_at', $q);
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL)
    {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('id', $q);
        $this->db->or_like('kode', $q);
        $this->db->or_like('nama', $q);
        $this->db->or_like('unit', $q);
        $this->db->or_like('category', $q);
        $this->db->or_like('image', $q);
        $this->db->or_like('stock', $q);
        $this->db->or_like('harga_penjualan', $q);
        $this->db->or_like('min_stock', $q);
        $this->db->or_like('created_at', $q);
        $this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }
}

/* End of file Barang_model.php */
/* Location: ./application/models/Barang_model.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-03-21 14:22:19 */
/* http://harviacode.com */
