<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Auth_model extends CI_Model{
    
    function auth_login($username,$password){ // Cek Login
        $query=$this->db->query("SELECT * FROM user WHERE username='$username' AND password=MD5('$password') LIMIT 1");
        return $query;
    }

    function get_by_level()
    {
        $this->db->where("level", "1");
        $this->db->or_where("level", "2");
        return $this->db->get("user")->result();
    }

    function get_by_id($id)
    {
        $this->db->where("id", $id);
        return $this->db->get("user")->row();
    }

    function update($id, $data)
    {
        $this->db->where("id", $id);
        $this->db->update("user", $data);
    }
    function delete($id)
    {
        $this->db->where("id", $id);
        $this->db->delete("user");
    }

 
}