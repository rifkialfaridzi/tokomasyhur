<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Stock_model extends CI_Model
{

    public $table = 'stock';
    public $id = 'id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // datatables
    function json()
    {
        $this->datatables->select('id,kode,barang,user,supplier,harga_pembelian,harga_penjualan,amount,balance,note,created_at');
        $this->datatables->from('stock');
        //add this line for join
        //$this->datatables->join('table2', 'stock.field = table2.field');
        //$this->datatables->add_column('action', anchor(site_url('stock/read/$1'),'Read')." | ".anchor(site_url('stock/update/$1'),'Update')." | ".anchor(site_url('stock/delete/$1'),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'id');
        return $this->datatables->generate();
    }

    function stock_byid_product($id)
    {
        $this->db->select('stock.*,barang.image as image, barang.nama as nama_barang,barang.harga_grosir as harga_grosir, supplier.nama as nama_supplier');
        $this->db->from('stock');

        $this->db->join('barang', 'stock.barang = barang.id');
        $this->db->join('supplier', 'stock.supplier = supplier.id');
        $this->db->where('stock.barang', $id);
        $this->db->where('stock.status', "0");
        //$this->datatables->add_column('action', anchor(site_url('stock/read/$1'),'Read')." | ".anchor(site_url('stock/update/$1'),'Update')." | ".anchor(site_url('stock/delete/$1'),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'id');
        return $this->db->get()->result();
    }

    // STOCK MASUK BY RANGE

    function stockin_range($id,$dateStart,$dateEnd)
    {
        $this->db->where('barang', $id);
        $this->db->where('status', 0);
        $this->db->where('DATE(st.created_at) BETWEEN "' . date('Y-m-d', strtotime($dateStart)) . '" and "' . date('Y-m-d', strtotime($dateEnd)) . '"');
        $this->db->select('st.*,b.nama as barang_name,b.kode as barang_kode,s.nama as supplier_name');
        $this->db->join('barang b', 'b.id=st.barang', 'left');
        $this->db->join('supplier s', 's.id=st.supplier', 'left');
        return $this->db->from('stock st');
        
    }

    function stock_mutatio_byproduk_in($month, $produk)
    {
        $this->db->select('st.*, b.nama as barang_name,b.kode as barang_kode');
        $this->db->from('stock st');
        $this->db->join('barang b', 'b.id=st.barang', 'left');
        $this->db->where('st.status', 0);
        $this->db->where('st.barang', $produk);
        return $this->db->where('MONTH(st.created_at)', $month);
    }

    function stock_mutatio_byproduk_out($month, $produk)
    {
        $this->db->select('st.*, b.nama as barang_name,b.kode as barang_kode');
        $this->db->from('stock st');
        $this->db->join('barang b', 'b.id=st.barang', 'left');
        $this->db->where('st.status', 1);
        $this->db->where('st.barang', $produk);
        return $this->db->where('MONTH(st.created_at)', $month);
    }

    function stock_sisa($month, $produk){

        $this->db->select('st.*, b.nama as barang_name,b.kode as barang_kode');
        $this->db->from('stock st');
        $this->db->join('barang b', 'b.id=st.barang', 'left');
        $this->db->where('st.barang', $produk);
        return $this->db->where('DATE(st.created_at) <', $month);

    }
    
    function stock_mutatio_byproduk_all($month, $produk)
    {
        $this->db->select('st.*, b.nama as barang_name,b.kode as barang_kode');
        $this->db->from('stock st');
        $this->db->join('barang b', 'b.id=st.barang', 'left');
        $this->db->where('st.barang', $produk);
        return $this->db->where('MONTH(st.created_at)', $month);
    }

    function stock_mutatio_byproduk_all_range($dateStart,$dateEnd, $produk)
    {
        $this->db->select('st.*, b.nama as barang_name,b.kode as barang_kode');
        $this->db->from('stock st');
        $this->db->join('barang b', 'b.id=st.barang', 'left');
        $this->db->where('st.barang', $produk);
        return $this->db->where('DATE(st.created_at) BETWEEN "' . date('Y-m-d', strtotime($dateStart)) . '" and "' . date('Y-m-d', strtotime($dateEnd)) . '"');
    }

    function sum_stockin_bymonth($id, $month)
    {
        $this->db->where('MONTH(created_at)', $month);
        $this->db->where('barang', $id);
        $this->db->where('status', 0);
        $this->db->select('SUM(amount) as total_masuk');
        $this->db->from('stock');
        return $this->db->group_by('barang')->get()->row();
    }
    function sum_stockout_bymonth($id, $month)
    {
        $this->db->where('MONTH(created_at)', $month);
        $this->db->where('barang', $id);
        $this->db->where('status', 1);
        $this->db->select('SUM(amount) as total_keluar');
        $this->db->from('stock');
        return $this->db->group_by('barang')->get()->row();
    }
    function sum_stockavailable_bymonth($id, $month)
    {
        $this->db->where('MONTH(created_at)', $month);
        $this->db->where('barang', $id);
        $this->db->where('status', 0);
        $this->db->select('SUM(balance) as total_available');
        $this->db->from('stock');
        return $this->db->group_by('barang')->get()->row();
    }

    // BY DATERANGE ================================================= 


    function sum_stockin_byrange($id,$dateStart,$dateEnd)
    {
        $this->db->where('barang', $id);
        $this->db->where('status', 0);
        $this->db->where('DATE(created_at) BETWEEN "' . date('Y-m-d', strtotime($dateStart)) . '" and "' . date('Y-m-d', strtotime($dateEnd)) . '"');
        $this->db->select('SUM(amount) as total_masuk');
        $this->db->from('stock');
        return $this->db->group_by('barang')->get()->row();
    }

    function sum_stockout_byrange($id,$dateStart,$dateEnd)
    {
        $this->db->where('barang', $id);
        $this->db->where('status', 1);
        $this->db->where('DATE(created_at) BETWEEN "' . date('Y-m-d', strtotime($dateStart)) . '" and "' . date('Y-m-d', strtotime($dateEnd)) . '"');
        $this->db->select('SUM(amount) as total_keluar');
        $this->db->from('stock');
        return $this->db->group_by('barang')->get()->row();
    }

    function sum_stockavailable_byrange($id,$dateStart,$dateEnd)
    {
        $this->db->where('barang', $id);
        $this->db->where('status', 0);
        $this->db->where('DATE(created_at) BETWEEN "' . date('Y-m-d', strtotime($dateStart)) . '" and "' . date('Y-m-d', strtotime($dateEnd)) . '"');
        $this->db->select('SUM(balance) as total_available');
        $this->db->from('stock');
        return $this->db->group_by('barang')->get()->row();
    }


    // END OF BY DATERANGE ==========================================



    function stock_mutatio_byproduk_groupbydate($month, $produk)
    {
        $this->db->select('st.created_at as tanggal');
        $this->db->from('stock st');
        $this->db->join('barang b', 'b.id=st.barang', 'left');
        $this->db->where('st.barang', $produk);
        return $this->db->where('MONTH(st.created_at)', $month)->group_by('st.created_at');
    }


    function pembelian_terakhir()
    {
        $this->db->select('st.*, b.nama as barang_name,b.kode as barang_kode');
        $this->db->from('stock st');
        $this->db->join('barang b', 'b.id=st.barang', 'left');
        $this->db->where('st.status', "0");
        $this->db->order_by("st.id", "desc");
        // $this->db->join('barang b', 'b.id=pd.barang', 'left');
        return $this->db->limit('10')->get()->result();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }
    function get_data_relational_byid($id)
    {

        $this->db->select('st.*, b.nama as barang_name,b.kode as barang_kode, s.nama as supplier_name, s.alamat as supplier_alamat,s.no_tlp as supplier_tlp');
        $this->db->from('stock st');
        $this->db->join('barang_masuk bm', 'bm.id=st.barang_masuk', 'left');
        $this->db->join('supplier s', 's.id=bm.supplier', 'left');
        $this->db->join('barang b', 'b.id=bm.barang', 'left');
        // return $this->db->where('st.barang',$id);
        return $this->db->where('st.barang_masuk', $id);
    }

    function get_stock_byidproduct_asc($id)
    {

        $this->db->where('barang', $id)->where('balance !=', "0")->where('status', "0")->order_by('id', 'ASC');
        return $this->db->get($this->table)->result();
    }

    // get total rows
    function total_rows($q = NULL)
    {
        $this->db->like('id', $q);
        $this->db->or_like('kode', $q);
        $this->db->or_like('barang', $q);
        $this->db->or_like('user', $q);
        $this->db->or_like('supplier', $q);
        $this->db->or_like('harga_pembelian', $q);
        $this->db->or_like('harga_penjualan', $q);
        $this->db->or_like('amount', $q);
        $this->db->or_like('balance', $q);
        $this->db->or_like('note', $q);
        $this->db->or_like('created_at', $q);
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL)
    {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('id', $q);
        $this->db->or_like('kode', $q);
        $this->db->or_like('barang', $q);
        $this->db->or_like('user', $q);
        $this->db->or_like('supplier', $q);
        $this->db->or_like('harga_pembelian', $q);
        $this->db->or_like('harga_penjualan', $q);
        $this->db->or_like('amount', $q);
        $this->db->or_like('balance', $q);
        $this->db->or_like('note', $q);
        $this->db->or_like('created_at', $q);
        $this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }
}

/* End of file Stock_model.php */
/* Location: ./application/models/Stock_model.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-03-21 14:22:22 */
/* http://harviacode.com */
