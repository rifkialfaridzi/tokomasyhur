<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Penjualan_model extends CI_Model
{

    public $table = 'penjualan';
    public $id = 'id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // datatables
    function json() {
        $this->datatables->select('id,kode,user,total_penjualan,pelanggan,created_at');
        $this->datatables->from('penjualan');
        //add this line for join
        //$this->datatables->join('table2', 'penjualan.field = table2.field');
        ///$this->datatables->add_column('action', anchor(site_url('penjualan/read/$1'),'Read')." | ".anchor(site_url('penjualan/update/$1'),'Update')." | ".anchor(site_url('penjualan/delete/$1'),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'id');
        return $this->datatables->generate();
    }

    function transaksi_bymonth($month) {
        $this->datatables->select('id,kode,user,total_penjualan,pelanggan,created_at');
        $this->datatables->from('penjualan');
        $this->datatables->where('MONTH(created_at)',$month);
        //add this line for join
        //$this->datatables->join('table2', 'penjualan.field = table2.field');
        ///$this->datatables->add_column('action', anchor(site_url('penjualan/read/$1'),'Read')." | ".anchor(site_url('penjualan/update/$1'),'Update')." | ".anchor(site_url('penjualan/delete/$1'),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'id');
        return $this->datatables->generate();
    }

    function total_order_bymonth($month)
    {
        $this->db->where('MONTH(created_at)', $month);
        return $this->db->get($this->table)->result();
    }

    function penjualan_range($dateStart,$dateEnd)
    {

        $this->db->where('DATE(p.created_at) BETWEEN "' . date('Y-m-d', strtotime($dateStart)) . '" and "' . date('Y-m-d', strtotime($dateEnd)) . '"');
        $this->db->select('p.*,SUM(pd.jumlah) as jumlah_items');
        $this->db->join('penjualan_detail pd', 'p.id=pd.penjualan', 'left');
        $this->db->group_by('pd.penjualan');
        return $this->db->from('penjualan p');
        
    }

    function penjualan_ranges($dateStart,$dateEnd)
    {

        $this->db->where('DATE(p.created_at) BETWEEN "' . date('Y-m-d', strtotime($dateStart)) . '" and "' . date('Y-m-d', strtotime($dateEnd)) . '"');
        $this->db->select('p.*,SUM(pd.jumlah) as jumlah_items,pd.barang as barang');
        $this->db->join('penjualan_detail pd', 'p.id=pd.penjualan', 'left');
        $this->db->group_by('pd.penjualan');
        $results =  $this->db->from('penjualan p')->get()->result_array();

        foreach ($results as $key => $result) {
            $this->db->select('b.nama as nama_barang, b.harga_penjualan as harga_penjualan, b.harga_grosir as harga_grosir, b.min_grosir as min_grosir, pd.jumlah as jumlah_pembelian, pd.total as total_pembelian');
            $this->db->where('penjualan', $result["id"]);
            $this->db->join('barang b', 'b.id=pd.barang', 'left');
            $product_penjualan = $this->db->get('penjualan_detail as pd')->result_array();

            $results[$key]['products'] = $product_penjualan;
            //$results[$key]['prodcts'] = $products;
        }
        return $results;
        
    }
	
	function produk_total_penjualan($month)
    {
        $this->db->where('MONTH(p.created_at)', $month);
        $this->db->select('SUM(p.total_penjualan) as total_penjualan');
        return $this->db->from('penjualan p')->get()->row();
         
    }

    function produk_total_bymonth($month)
    {
        $this->db->where('MONTH(p.created_at)', $month);
        $this->db->select('SUM(pd.jumlah) as total_produk, SUM(p.total_penjualan) as total_penjualan');
        $this->db->from('penjualan p'); 
        return $this->db->join('penjualan_detail pd', 'p.id=pd.penjualan', 'left')->get()->row();
    }

    function top_product_bymonth($month)
    {
        $this->db->where('MONTH(p.created_at)', $month);
        $this->db->select('SUM(pd.jumlah) as total_produk, b.nama as nama_produk, b.image as image');
        $this->db->from('penjualan p'); 
        $this->db->join('penjualan_detail pd', 'p.id=pd.penjualan', 'left');
        $this->db->join('barang b', 'b.id=pd.barang', 'left');
        return $this->db->group_by('pd.barang')->order_by('total_produk','DESC')->limit('5')->get()->result();
    }

    function penjualan_terakhir()
    {
        $this->db->select('p.*');
        $this->db->from('penjualan p'); 
        $this->db->order_by("id", "desc");
       // $this->db->join('barang b', 'b.id=pd.barang', 'left');
        return $this->db->limit('10')->get()->result();
    }
    
    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    function get_data_product_byid($id){

        $this->db->select('p.*, pd.jumlah as pd_jumlah,pd.harga_jual as harga_jual, pd.total as pd_total,b.min_grosir as min_grosir,b.harga_grosir as harga_grosir, b.nama as nama_barang, b.kode as kode_barang, b.harga_penjualan as harga_barang');
        $this->db->from('penjualan p'); 
        $this->db->join('penjualan_detail pd', 'p.id=pd.penjualan', 'left');
        $this->db->join('barang b', 'b.id=pd.barang', 'left');
       
        return $this->db->where('p.id',$id);
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }
    
    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('id', $q);
	$this->db->or_like('kode', $q);
	$this->db->or_like('user', $q);
	$this->db->or_like('total_penjualan', $q);
	$this->db->or_like('pelanggan', $q);
	$this->db->or_like('created_at', $q);
	$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('id', $q);
	$this->db->or_like('kode', $q);
	$this->db->or_like('user', $q);
	$this->db->or_like('total_penjualan', $q);
	$this->db->or_like('pelanggan', $q);
	$this->db->or_like('created_at', $q);
	$this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->trans_start();
        $this->db->insert($this->table, $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return  $insert_id;
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}

/* End of file Penjualan_model.php */
/* Location: ./application/models/Penjualan_model.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-03-21 14:22:20 */
