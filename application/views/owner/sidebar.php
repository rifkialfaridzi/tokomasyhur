<aside id="sidebar-wrapper">
    <div class="sidebar-brand">
        <br><img src="<?php echo base_url('assets/LGO.png'); ?>" alt="logo" width="200"><br><br>
    </div>
    <div class="sidebar-brand sidebar-brand-sm">
        <img src="<?php echo base_url('assets/alt.png'); ?>" alt="logo" width="50">
    </div>
    <ul class="sidebar-menu">
        <!--
        <li class="menu-header">Starter</li>
        <li class="dropdown">
            <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-columns"></i> <span>Layout</span></a>
            <ul class="dropdown-menu">
                <li><a class="nav-link" href="layout-default.html">Default Layout</a></li>
                <li><a class="nav-link" href="layout-transparent.html">Transparent Sidebar</a></li>
                <li><a class="nav-link" href="layout-top-navigation.html">Top Navigation</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="nav-link has-dropdown"><i class="fas fa-ellipsis-h"></i> <span>Utilities</span></a>
            <ul class="dropdown-menu">
                <li><a href="utilities-contact.html">Contact</a></li>
                <li><a class="nav-link" href="utilities-invoice.html">Invoice</a></li>
                <li><a href="utilities-subscribe.html">Subscribe</a></li>
            </ul>
        </li>
-->
        <li <?php if ($this->uri->segment(1) == "owner") {
                echo 'class="active"';
            } ?>><a class="nav-link" href="<?php echo site_url("owner"); ?>"><i class="fas fa-fire"></i> <span>Dashboard</span></a></li>
        <li class="dropdown<?php if ($this->uri->segment(1) == "pembelian") {
                                echo ' active';
                            } ?>">
            <a href="#" class="nav-link has-dropdown"><i class="fas fa-calendar-plus"></i><span>Data Pembelian</span></a>
            <ul class="dropdown-menu">
                <li <?php if ($this->uri->segment(2) == "masuk") {
                        echo 'class="active"';
                    } ?>><a href="<?php echo site_url("pembelian/masuk"); ?>"><i class="fas fa-plus"></i>Produk Masuk</a></li>
                <li <?php if ($this->uri->segment(2) == "refund") {
                        echo 'class="active"';
                    } ?>><a class="nav-link" href="<?php echo site_url("pembelian/refund"); ?>"><i class="fas fa-paper-plane"></i>Produk Refund</a></li>
            </ul>
        </li>
        <li <?php if ($this->uri->segment(1) == "transaksi") {
                echo 'class="active"';
            } ?>><a class="nav-link" href="<?php echo base_url("transaksi"); ?>"><i class="fas fa-poll"></i> <span>Data Transaksi</span></a></li>
        <li <?php if ($this->uri->segment(1) == "stock") {
                echo 'class="active"';
            } ?>><a class="nav-link" href="<?php echo site_url("stock"); ?>"><i class="fas fa-cubes"></i> <span>Stock Gudang</span></a></li>
        <li <?php if ($this->uri->segment(1) == "kontrol") {
                echo 'class="active"';
            } ?>><a class="nav-link" href="<?php echo site_url("kontrol"); ?>"><i class="fas fa-rocket"></i> <span>Kontrol Stock</span></a></li>
        <li <?php if ($this->uri->segment(1) == "pengguna") {
                echo 'class="active"';
            } ?>><a class="nav-link" href="<?php echo site_url("pengguna"); ?>"><i class="fas fa-user"></i> <span>Pengguna</span></a></li>
    </ul>


</aside>