<div style="text-align:center">
    <h2 style="text-align:center"><strong>Laporan Penjualan</strong></h2>

    <h5 style="text-align:center">Tanggal <?php echo $date['dateStart']; ?> s/d&nbsp; <?php echo $date['dateEnd']; ?></h5>

    <p>&nbsp;</p>

    <table border="1" cellpadding="1" cellspacing="0" style="border-collapse:collapse; border:1px solid black; margin:auto; padding:10px; width:90%">

        <tbody>
            <tr>
                <td style="text-align:center;font-size: 13px;">Kode</td>
                <td style="text-align:center;font-size: 13px;">Tanggal</td>
                <td style="text-align:center;font-size: 13px;">Produk</td>
                <td style="text-align:center;font-size: 13px;">Jumlah</td>
                <td style="text-align:center;font-size: 13px;">Grosir</td>
                <td style="text-align:center;font-size: 13px;">Harga</td>
                <td style="text-align:center;font-size: 13px;">Total</td>
            </tr>

            <?php foreach ($data as $key) { ?>
                <tr>
                    <td style="text-align:center;font-size: 13px;"><b><?php echo $key['kode']; ?></b></td>
                    <td style="text-align:center;font-size: 13px;"><b><?php echo Date("d-m-Y",strtotime($key['created_at'])) ?></b></td>
                    <td style="text-align:center;font-size: 13px;"></td>
                    <td style="text-align:center;font-size: 13px;"></td>
                    <td style="text-align:center;font-size: 13px;"></td>
                    <td style="text-align:center;font-size: 13px;"></td>
                    <td style="text-align:center;font-size: 13px;"></td>
                </tr>
                <?php foreach ($key['products'] as $keys) { ?>
                    <tr>
                    <td style="text-align:center;font-size: 13px;"></td>
                    <td style="text-align:center;font-size: 13px;"></td>
                    <td style="text-align:center;font-size: 13px;"><?php echo $keys['nama_barang']; ?></td>
                    <td style="text-align:center;font-size: 13px;"><?php echo $keys['jumlah_pembelian']; ?></td>
                    <td style="text-align:center;font-size: 13px;"><?php echo intval($keys['jumlah_pembelian']) > intval($keys['min_grosir']) ? "Ya" : "Tidak"; ?></td>
                    <td style="text-align:center;font-size: 13px;"><?php echo "Rp " . number_format($keys['harga_penjualan'],0,',','.'); ?></td>
                    <td style="text-align:center;font-size: 13px;"><?php echo "Rp " . number_format($keys['total_pembelian'],0,',','.'); ?></td>
                    </tr>
                <?php } ?>
                
            <?php } ?>
        </tbody>
    </table>

    <p>&nbsp;</p>

    <p>&nbsp;</p>

    <table cellpadding="1" cellspacing="0" style="page-break-inside: avoid;text-align:center;border-collapse:collapse; border:none; margin:auto; padding:10px; width:90%">
        <tbody>
            <tr>
                <td>Yang Mengetahui,</td>
            </tr>
            <tr>
                <td><span style="font-size:16px"><strong><?php echo $data_user->name; ?></strong></span></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td><?php if ($data_user->level == 2) {
                        echo "Kasir Al Masyhur";
                    } elseif ($data_user->level == 3) {
                        echo "Pimpinan Al Masyhur";
                    } ?></td>
            </tr>
        </tbody>
    </table>

    <p>&nbsp;</p>
</div>

<p>&nbsp;</p>