<div style="text-align:center">
    <h2 style="text-align:center"><strong>Laporan Kartu Stock <?php echo $productName; ?></strong></h2>

    <h5 style="text-align:center">Tanggal <?php echo $date['dateStart']; ?> s/d&nbsp; <?php echo $date['dateEnd']; ?></h5>

    <p>&nbsp;</p>

    <table border="1" cellpadding="1" cellspacing="0" style="border-collapse:collapse; border:1px solid black; margin:auto; padding:10px; width:90%">

        <tbody>
            <tr>
            <td style="text-align:center">Tanggal</td>
                <td style="text-align:center">Kode</td>
                <td style="text-align:center">Nama</td>
                <td style="text-align:center">Stok Masuk</td>
                <td style="text-align:center">Stok Keluar</td>
                <td style="text-align:center">Stok Tersedia</td>
            </tr>

            <?php foreach ($data as $key) { ?>
                <tr>
                <td style="text-align:center"><?php echo $key->created_at; ?></td>
                    <td style="text-align:center"><?php echo $key->barang_kode; ?></td>
                    <td style="text-align:left"><?php echo $key->barang_name; ?></td>
                    <td style="text-align:center"><?php echo $key->masuk; ?></td>
                    <td style="text-align:center"><?php echo $key->keluar; ?></td>
                    <td style="text-align:center"><?php echo $key->sisa; ?></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>

    <p>&nbsp;</p>

    <p>&nbsp;</p>

    <table cellpadding="1" cellspacing="0" style="page-break-inside: avoid;text-align:center;border-collapse:collapse; border:none; margin:auto; padding:10px; width:90%">
        <tbody>
            <tr>
                <td>Yang Mengetahui,</td>
            </tr>
            <tr>
                <td><span style="font-size:16px"><strong><?php echo $data_user->name; ?></strong></span></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td><?php if ($data_user->level == 1) {
                    echo "Admin Gudang Al Masyhur";
                }elseif ($data_user->level == 3) {
                    echo "Pimpinan Al Masyhur";
                } ?></td>
            </tr>
        </tbody>
    </table>

    <p>&nbsp;</p>
</div>

<p>&nbsp;</p>