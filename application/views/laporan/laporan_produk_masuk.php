<div style="text-align:center">
    <h2 style="text-align:center"><strong>Laporan Produk Masuk</strong></h2>

    <h5 style="text-align:center">Tanggal <?php echo $date['dateStart']; ?> s/d&nbsp; <?php echo $date['dateEnd']; ?></h5>

    <p>&nbsp;</p>

    <table border="1" cellpadding="1" cellspacing="0" style="border-collapse:collapse; border:1px solid black; margin:auto; padding:10px; width:100%">

        <tbody>
            <tr>
            <td style="text-align:center">Tanggal</td>
                <td style="text-align:center">Kode Stock</td>
                <td style="text-align:center">Nama Produk</td>
                <td style="text-align:center">Kode Produk</td>
                <td style="text-align:center">Supplier</td>
                <td style="text-align:center">Jumlah</td>
                <td style="text-align:center">Harga Beli</td>
            </tr>

            <?php foreach ($data as $key) { ?>
                <tr>
                <td style="text-align:center"><?php echo $key->created_at; ?></td>
                    <td style="text-align:center"><?php echo $key->kode; ?></td>
                    <td style="text-align:center"><?php echo $key->nama_barang; ?></td>
                    <td style="text-align:center"><?php echo $key->kode_barang; ?></td>
                    <td style="text-align:center"><?php echo $key->nama_supplier; ?></td>
                    <td style="text-align:center"><?php echo $key->jumlah; ?></td>
                    <td style="text-align:center"><?php echo $key->harga_pembelian; ?></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>

    <p>&nbsp;</p>

    <p>&nbsp;</p>

    <table cellpadding="1" cellspacing="0" style="page-break-inside: avoid;text-align:center;border-collapse:collapse; border:none; margin:auto; padding:10px; width:100%">
        <tbody>
            <tr>
                <td>Yang Mengetahui,</td>
            </tr>
            <tr>
                <td><span style="font-size:16px"><strong><?php echo $data_user->name; ?></strong></span></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td><?php if ($data_user->level == 1) {
                    echo "Admin Gudang Al Masyhur";
                }elseif ($data_user->level == 3) {
                    echo "Pimpinan Al Masyhur";
                } ?></td>
            </tr>
        </tbody>
    </table>

    <p>&nbsp;</p>
</div>

<p>&nbsp;</p>