<div style="text-align:center">
    <h2 style="text-align:center"><strong>Laporan Produk</strong></h2>
    <p>Tanggal: <?php echo date("Y-m-d"); ?></p>
    
    <p>&nbsp;</p>

    <table border="1" cellpadding="1" cellspacing="0" style="border-collapse:collapse; border:1px solid black; margin:auto; padding:10px; width:100%">

        <tbody>
            <tr>
            <td style="text-align:center">Tanggal</td>
                <td style="text-align:center">Kode</td>
                <td style="text-align:center">Nama</td>
                <td style="text-align:center">Kategori</td>
                <td style="text-align:center">Rak</td>
                <td style="text-align:center">Stock</td>
            </tr>

            <?php foreach ($data as $key) { ?>
                <tr>
                <td style="text-align:center"><?php echo $key->created_at; ?></td>
                    <td style="text-align:center"><?php echo $key->kode; ?></td>
                    <td style="text-align:center"><?php echo $key->nama; ?></td>
                    <td style="text-align:center"><?php echo $key->category_name; ?></td>
                    <td style="text-align:center"><?php echo $key->rak_name; ?></td>
                    <td style="text-align:center"><?php echo $key->stock; ?></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>

    <p>&nbsp;</p>

    <p>&nbsp;</p>

    <table cellpadding="1" cellspacing="0" style="page-break-inside: avoid;text-align:center;border-collapse:collapse; border:none; margin:auto; padding:10px; width:100%">
        <tbody>
            <tr>
                <td>Yang Mengetahui,</td>
            </tr>
            <tr>
                <td><span style="font-size:16px"><strong>Ganis Kristikasari</strong></span></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>Pimpinan Al Masyhur</td>
            </tr>
        </tbody>
    </table>

    <p>&nbsp;</p>
</div>

<p>&nbsp;</p>