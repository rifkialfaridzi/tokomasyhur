<div style="text-align:center">
    <h2 style="text-align:center"><strong>Laporan Penjualan</strong></h2>

    <h5 style="text-align:center">Tanggal <?php echo $date['dateStart']; ?> s/d&nbsp; <?php echo $date['dateEnd']; ?></h5>

    <p>&nbsp;</p>

    <table border="1" cellpadding="1" cellspacing="0" style="border-collapse:collapse; border:1px solid black; margin:auto; padding:10px; width:90%">

        <tbody>
            <tr>
            <td style="text-align:center">Kode Transaksi</td>
                <td style="text-align:center">Jumlah Items</td>
                <td style="text-align:center">Total Penjualan</td>
                <td style="text-align:center">Tanggal</td>
            </tr>

            <?php foreach ($data as $key) { ?>
                <tr>
                <td style="text-align:center"><?php echo $key->kode; ?></td>
                    <td style="text-align:center"><?php echo $key->jumlah_items; ?></td>
                    <td style="text-align:center"><?php echo $key->total_penjualan; ?></td>
                    <td style="text-align:center"><?php echo $key->created_at; ?></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>

    <p>&nbsp;</p>

    <p>&nbsp;</p>

    <table cellpadding="1" cellspacing="0" style="page-break-inside: avoid;text-align:center;border-collapse:collapse; border:none; margin:auto; padding:10px; width:90%">
        <tbody>
            <tr>
                <td>Yang Mengetahui,</td>
            </tr>
            <tr>
                <td><span style="font-size:16px"><strong><?php echo $data_user->name; ?></strong></span></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td><?php if ($data_user->level == 2) {
                    echo "Kasir Al Masyhur";
                }elseif ($data_user->level == 3) {
                    echo "Pimpinan Al Masyhur";
                } ?></td>
            </tr>
        </tbody>
    </table>

    <p>&nbsp;</p>
</div>

<p>&nbsp;</p>