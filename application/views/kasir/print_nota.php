<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <link rel="stylesheet" href=<?php echo base_url("assets/modules/bootstrap/css/bootstrap.min.css"); ?>>
    <style>
        html {
            margin: 10px 0 10px 0
        }

        h3 {
            font-size: 18px;
        }

        .tg {
            border-collapse: collapse;
            border-spacing: 0;
        }

        .tg td {
            font-family: Arial, sans-serif;
            font-size: 14px;
            padding: 10px 5px;
            overflow: hidden;
            word-break: normal;
        }

        .tg th {
            font-family: Arial, sans-serif;
            font-size: 14px;
            font-weight: normal;
            padding: 10px 5px;
            overflow: hidden;
            word-break: normal;
        }

        .tg .tg-lboi {
            border-color: inherit;
            text-align: left;
            vertical-align: middle
        }

        .tg .tg-0pky {
            border-color: inherit;
            text-align: left;
            vertical-align: top
        }
    </style>
</head>

<body>
    <div class="container">
        <style type="text/css">
            .tg {
                border-collapse: collapse;
                border-spacing: 0;
            }

            .tg td {
                font-family: Arial, sans-serif;
                font-size: 14px;
                padding: 10px 5px;
               
                word-break: normal;
               
            }

            .tg th {
                font-family: Arial, sans-serif;
                font-size: 14px;
                font-weight: normal;
                padding: 10px 5px;
                overflow: hidden;
                word-break: normal;
            }

            .tg .tg-0pky {
                border-color: inherit;
                text-align: left;
                vertical-align: top
            }
        </style>
        <table class="tg" style="table-layout: fixed; width: 100%; border:solid 1px">
            <colgroup>
                <col style="width: 20%">
                <col style="width: 60%">
                <col style="width: 20%">
            </colgroup>
            <tr>
                <th class="tg-0pky">
                <img alt="image" style="width:80%; margin:5px; text-align:center" src="<?php echo site_url($image_logo); ?>">
                    <p style="margin:5px 0 5px 0;font-size:12px; vertical-align: middle;">JL Gendingan – Boloh Ruko No. 21, Kecamatan Toroh, Purwodadi</p>
                    <p style="margin:5px 0 5px 0;font-size:12px">Tlp. 082136033147</p>
                </th>
                <th class="tg-0pky" style="text-align:center;vertical-align: middle">
                    <h4> Nota Penjualan </h4>
                </th>
                <th class="tg-0pky" style="text-align: center">
                    <p style="margin: 2px 0 2px 0;font-size: 8"><?php echo $transaksi->kode; ?></p>
                </th>
            </tr>
            <tr>
                <td class="tg-0pky" colspan="3">
                    <table class="tg" style="width:100%">
                        <tr>
                            <th class="tg-cly1" colspan="5">
                                <table style="padding:0">
                                    <tr>
                                        <th style="padding:2px">
                                            <p style="margin:0;">Kepada</p>
                                        </th>
                                        <th style="padding:2px">
                                            <p style="margin:0;">: <?php echo $transaksi->pelanggan; ?></p> </p>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th style="padding:2px">
                                            <p style="margin:0;">Note</p>
                                        </th>
                                        <th style="padding:2px">
                                            <p style="margin:0;">: Barang Yang Sudah Di Beli Tidak Dapat Di Kembalikan/ Ditukar</p>
                                        </th>
                                    </tr>
                        </tr>
                    </table>
                    </th>
            </tr>
            <tr>
                <td style="border:solid 1px;padding:2px;width:5%">
                    <p style="font-weight:bold;margin:0">No.</p>
                </td>
                <td style="border:solid 1px;padding:2px;width:50%">
                    <p style="font-weight:bold;margin:0">Nama Produk</p>
                </td>
                <td style="border:solid 1px;padding:2px;width:5%">
                    <p style="font-weight:bold;margin:0">Qty</p>
                </td>
                <td style="border:solid 1px;padding:2px;width:20%">
                    <p style="font-weight:bold;margin:0">Harga</p>
                </td>
                <td style="border:solid 1px;padding:2px;width:20%">
                    <p style="font-weight:bold;margin:0">Total</p>
                </td>
            </tr>
            <?php $a=1; foreach ($detailTransaksi as $transaksis) {
                # code...
             ?>
            <tr>
                <td class="tg-cly1" style="border:solid 1px;padding:2px;text-align:center"><?php echo $a++; ?></td>
                <td class="tg-cly1" style="border:solid 1px;padding:2px"><?php echo $transaksis->nama_barang; ?>(<?php echo $transaksis->kode_barang; ?>)</td>
                <td class="tg-cly1" style="border:solid 1px;padding:2px;text-align:center"><?php echo $transaksis->pd_jumlah; ?></td>
                <td class="tg-0lax" style="border:solid 1px;padding:2px">Rp. <?php echo number_format($transaksis->harga_jual,2,',','.'); ?></td>
                <td class="tg-0lax" style="border:solid 1px;padding:2px">Rp. <?php echo number_format($transaksis->pd_total,2,',','.'); ?></td>
            </tr>
            <?php } ?>
            <tr>

                <td class="tg-0lax" colspan="2" style="border:solid 1px;padding:2px">
                <p style="text-align:right;font-size: 15px">Total Pembayaran </p>
                <p style="text-align:right;font-size: 12px">Cash </p>
                    <p style="text-align:right;font-size: 12px">Return </p>
                </td>
                <td class="tg-0lax" style="border:solid 1px;padding:2px"></td>
                <td class="tg-0lax" style="border:solid 1px;padding:2px"></td>
                <td class="tg-0lax" style="border:solid 1px;padding:2px">
                <p style="font-weight:bold;font-size: 15px">Rp. <?php echo number_format($transaksi->total_penjualan,2,',','.'); ?></p>
                <p style="font-weight:bold;font-size: 12px">Rp. <?php echo number_format($transaksi->bill,2,',','.'); ?></p>
                    <p style="font-weight:bold;font-size: 12px">Rp. <?php echo number_format(abs($transaksi->kembalian),2,',','.'); ?></p>
                </td>
            </tr>
            
            <tr>

                <td class="tg-0lax" colspan="2">
                <p style="text-align:left;font-weight: bold">Tanggal : <?php $time = strtotime($transaksi->created_at); echo date('d M Y H:i:s',$time); ?></p>
                </td>
                <td class="tg-0lax"></td>
                <td class="tg-0lax"></td>
                <td class="tg-0lax">
                    <p style="font-weight:bold"></p>
                </td>
            </tr>
        </table>
        </td>

        </td>
        </tr>
        </table>


    </div>
</body>

</html>