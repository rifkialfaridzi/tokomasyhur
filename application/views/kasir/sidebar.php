<aside id="sidebar-wrapper">
    <div class="sidebar-brand">
        <br><img src="<?php echo base_url('assets/LGO.png'); ?>" alt="logo" width="200"><br><br>
    </div>
    <div class="sidebar-brand sidebar-brand-sm">
        <img src="<?php echo base_url('assets/alt.png'); ?>" alt="logo" width="50">
    </div>
    <ul class="sidebar-menu">
        <!--
        <li class="menu-header">Starter</li>
        <li class="dropdown">
            <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-columns"></i> <span>Layout</span></a>
            <ul class="dropdown-menu">
                <li><a class="nav-link" href="layout-default.html">Default Layout</a></li>
                <li><a class="nav-link" href="layout-transparent.html">Transparent Sidebar</a></li>
                <li><a class="nav-link" href="layout-top-navigation.html">Top Navigation</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="nav-link has-dropdown"><i class="fas fa-ellipsis-h"></i> <span>Utilities</span></a>
            <ul class="dropdown-menu">
                <li><a href="utilities-contact.html">Contact</a></li>
                <li><a class="nav-link" href="utilities-invoice.html">Invoice</a></li>
                <li><a href="utilities-subscribe.html">Subscribe</a></li>
            </ul>
        </li>
-->     <li <?php if ($this->uri->segment(1) == "kasir") {
                echo 'class="active"';
            } ?>><a class="nav-link" href="<?php echo site_url("kasir"); ?>"><i class="fas fa-fire"></i> <span>Dashboard</span></a></li>
            
        <li <?php if ($this->uri->segment(2) == "produk") {
                echo 'class="active"';
            } ?>><a class="nav-link" href="<?php echo base_url("kasir/produk/cart"); ?>"><i class="fas fa-shopping-cart"></i> <span>Kasir Penjualan</span></a></li>
<li 
         <?php if ($this->uri->segment(1) == "transaksi") {
                echo 'class="active"';
            } ?>><a class="nav-link" href="<?php echo base_url("transaksi"); ?>"><i class="fas fa-poll"></i> <span>Data Transaksi</span></a></li>
             <li <?php if ($this->uri->segment(2) == "change_name") {
                echo 'class="active"';
            } ?>><a class="nav-link" href="<?php echo site_url("master/change_name"); ?>"><i class="fas fa-user"></i> <span>Profile</span></a></li>

    </ul>

    
</aside>