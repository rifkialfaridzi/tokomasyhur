<section class="section">
    <div class="section-header">
        <h1>Halaman Unit</h1>
    </div>

    <div class="section-body">
        <div class="row">
            <div class="col-lg-4 col-md-12 col-12 col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Edit Unit</h4>
                        <div class="card-header-action">
                            <a data-collapse="#mycard-collapse" class="btn btn-icon btn-info" href="#"><i class="fas fa-minus"></i></a>
                        </div>
                    </div>
                    <div class="collapse show" id="mycard-collapse">
                        <div class="card-body">
                            <form method="POST" action="<?php echo base_url("unit/update_action/" . $id); ?>" class="needs-validation" novalidate="">
                                <div class="form-group">
                                    <label for="nama">Nama Unit</label>
                                    <input id="nama" type="nama" value="<?php echo $nama ?>" class="form-control" name="nama" tabindex="1" required autofocus>
                                    <div class="invalid-feedback">
                                        Nama Unit Masih Kosong
                                    </div>
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-success btn-lg btn-block" tabindex="4">
                                        Ubah
                                    </button>
                                </div>
                            </form>

                            <?php if ($this->session->flashdata('pesan')) { ?>
                                <div class="alert alert-warning alert-dismissible show fade">
                                    <div class="alert-body">
                                        <button class="close" data-dismiss="alert">
                                            <span>&times;</span>
                                        </button>
                                        <?php echo $this->session->flashdata('pesan');
                                        ?>
                                    </div>
                                </div>
                            <?php } ?>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8 col-md-12 col-12 col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Data Unit</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="unit_tabel" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>
                                            #Kode
                                        </th>
                                        <th>Nama</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" tabindex="-1" role="dialog" id="exampleModalDelete">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Hapus Unit</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Apakah Anda Yakin ?</p>
            </div>
            <div class="modal-footer bg-whitesmoke br">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <a id="btn-delete" type="button" href="#" class="btn btn-danger">Hapus</a>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="exampleModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Unit</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="card-body">
                        <form id="form_edit" method="POST" action="<?php echo site_url('update_action/' . $id) ?>" class="needs-validation" novalidate="">

                            <div class="form-group">
                                <label for="nama">Nama Unit</label>
                                <input id="nama_kamu" type="nama" class="form-control" name="nama" tabindex="1" required autofocus>
                                <div class="invalid-feedback">
                                    Nama Unit Masih Kosong
                                </div>
                            </div>

                            <?php if ($this->session->flashdata('pesan_update')) { ?>
                                <div class="alert alert-warning alert-dismissible show fade">
                                    <div class="alert-body">
                                        <button class="close" data-dismiss="alert">
                                            <span>&times;</span>
                                        </button>
                                        <?php echo $this->session->flashdata('pesan_update');
                                        ?>
                                    </div>
                                </div>
                            <?php } ?>

                    </div>
                </div>
            </div>
            <div class="modal-footer bg-whitesmoke br">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-secondary cekancok">CEK</button>
                <button id="edit_submit" type="Submit" class="btn btn-primary">Simpan</button>
            </div>
            </form>
        </div>
    </div>
</div>




<script type="text/javascript">
    var save_method; //for save method string
    var table;

    $(document).ready(function() {
        //datatables
        table = $('#unit_tabel').DataTable({
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": '<?php echo site_url('unit/json'); ?>',
                "type": "POST"
            },
            //Set column definition initialisation properties.
            "columns": [{
                    "data": "id"
                },
                {
                    "data": "nama"
                },
                {
                    "data": null,
                    "render": function(data, type, row) {
                        return '<a href="<?php echo site_url("master/unit/edit/") ?>' + row.id + '" class="btn btn-icon btn-primary"><i class="far fa-edit"></i></a> <a onclick=deleteConfirm("' + row.id + '") href="#!" class="btn btn-icon btn-danger exampleModalDelete" data-toggle="modal" data-target="#exampleModalDelete"><i class="fas fa-times"></i></a>';
                    }
                }
            ],

        });



    });

    function deleteConfirm(url) {
        $('#btn-delete').attr('href', "<?php echo site_url("unit/delete"); ?>/" + url);
        $('#exampleModalDelete').modal();
    }
</script>