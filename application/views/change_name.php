<section class="section">
	<div class="section-header">
		<h1>Ubah User</h1>
	</div>

	<div class="section-body">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-12 col-sm-12">
				<div class="card">
					<div class="card-header">
						<h4>Ubah User</h4>
						<div class="card-header-action">
							<a data-collapse="#mycard-collapse" class="btn btn-icon btn-info" href="#"><i class="fas fa-minus"></i></a>
						</div>
					</div>
					<div class="collapse show" id="mycard-collapse">
						<div class="card-body">
							<form enctype="multipart/form-data" method="POST" action="<?php echo base_url("auth/change_name_action"); ?>" class="needs-validation" novalidate="">

								<div class="form-group">
									<label for="nama">Nama</label>
									<input id="nama" type="text" class="form-control" value="<?php echo $dataUser->name ?>" name="name" tabindex="1" required autofocus>
									<div class="invalid-feedback">
										Nama Masih Kosong
									</div>
								</div>

								<div class="form-group">
									<label for="nama">Username</label>
									<input disabled id="nama" type="text" class="form-control" value="<?php echo $dataUser->username ?>" name="username" tabindex="1" required autofocus>
									<div class="invalid-feedback">
										Nama Masih Kosong
									</div>
								</div>

								<div class="form-group">
									<button type="submit" class="btn btn-success btn-lg btn-block" tabindex="4">
										Ubah
									</button>
								</div>
							</form>

							<?php if ($this->session->flashdata('pesan')) { ?>
								<div class="alert alert-warning alert-dismissible show fade">
									<div class="alert-body">
										<button class="close" data-dismiss="alert">
											<span>&times;</span>
										</button>
										<?php echo $this->session->flashdata('pesan');
										?>
									</div>
								</div>
							<?php } ?>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>



<script type="text/javascript">
	var save_method; //for save method string
	var table;
</script>