<section class="section">
	<div class="section-header">
		<h1>Halaman Category</h1>
	</div>

	<div class="section-body">
		<div class="row">
			<div class="col-lg-4 col-md-12 col-12 col-sm-12">
				<div class="card">
					<div class="card-header">
						<h4>Ubah Category</h4>
						<div class="card-header-action">
							<a data-collapse="#mycard-collapse" class="btn btn-icon btn-info" href="#"><i class="fas fa-minus"></i></a>
						</div>
					</div>
					<div class="collapse show" id="mycard-collapse">
						<div class="card-body">
							<form method="POST" action="<?php echo base_url("pengguna/update_action/") . $id; ?>" class="needs-validation" novalidate="">
								<div class="form-group">
									<label for="nama">Nama</label>
									<input id="nama" type="text" class="form-control" value="<?php echo $data->name; ?>" name="name" tabindex="1" required autofocus>
									<div class="invalid-feedback">
										Nama pengguna Masih Kosong
									</div>
								</div>
								<div class="form-group">
									<label>Level</label>
									<select name="level" class="form-control select2">
										<option value="">Pilih Level</option>
										<option <?php echo $data->level == 1 ? "selected":""; ?> value="1">Admin Gudang</option>
										<option <?php echo $data->level == 2 ? "selected":""; ?> value="2">Admin Kasir</option>
									</select>
								</div>
								<div class="form-group">
									<label for="nama">Username</label>
									<input id="nama" type="text" value="<?php echo $data->username; ?>" class="form-control" name="username" tabindex="1" required autofocus>
									<div class="invalid-feedback">
										Username Masih Kosong
									</div>
								</div>
								<div class="form-group">
									<label for="nama">Password</label>
									<input id="nama" type="password" class="form-control" name="password" tabindex="1" autofocus>
									<div class="invalid-feedback">
										Password Masih Kosong
									</div>
								</div>

								<div class="form-group">
									<button type="submit" class="btn btn-success btn-lg btn-block" tabindex="4">
										Ubah
									</button>
								</div>
							</form>

							<?php if ($this->session->flashdata('pesan')) { ?>
								<div class="alert alert-warning alert-dismissible show fade">
									<div class="alert-body">
										<button class="close" data-dismiss="alert">
											<span>&times;</span>
										</button>
										<?php echo $this->session->flashdata('pesan');
										?>
									</div>
								</div>
							<?php } ?>

						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-8 col-md-12 col-12 col-sm-12">
				<div class="card">
					<div class="card-header">
						<h4>Data Pengguna</h4>
					</div>
					<div class="card-body">
						<div class="table-responsive">
							<table id="pengguna_tabel" class="table table-striped">
								<thead>
									<tr>
										<th>Nama</th>
										<th>Username</th>
										<th>Level</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="modal fade" tabindex="-1" role="dialog" id="exampleModalDelete">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Hapus Unit</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p>Apakah Anda Yakin ?</p>
			</div>
			<div class="modal-footer bg-whitesmoke br">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
				<a id="btn-delete" type="button" href="#" class="btn btn-danger">Hapus</a>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	var save_method; //for save method string
	var table;

	$(document).ready(function() {
		//datatables
		table = $('#pengguna_tabel').DataTable({
			// Load data for the table's content from an Ajax source
			"ajax": {
				"url": '<?php echo site_url('pengguna/json'); ?>',
				"type": "POST"
			},
			//Set column definition initialisation properties.
			"columns": [{
					"data": "name"
				},
				{
					"data": "username"
				},
				{
					"data": null,
					"render": function(data, type, row) {
						if (row.level == 1) {
							return "Admin Gudang";
						} else {
							return "Admin Kasir";
						}
					}
				},
				{
					"data": null,
					"render": function(data, type, row) {
						return '<a href="<?php echo site_url("pengguna/edit/") ?>' + row.id + '" class="btn btn-icon btn-primary"><i class="far fa-edit"></i></a> <a onclick=deleteConfirm("' + row.id + '") href="#!" class="btn btn-icon btn-danger exampleModalDelete" data-toggle="modal" data-target="#exampleModalDelete"><i class="fas fa-times"></i></a>';
					}
				}
			],

		});



	});

	function deleteConfirm(url) {
		$('#btn-delete').attr('href', "<?php echo site_url("category/delete"); ?>/" + url);
		$('#exampleModalDelete').modal();
	}
</script>