<section class="section">
	<div class="section-header">
		<h1>Halaman Produk</h1>
	</div>

	<div class="section-body">
		<div class="row">
			<div class="col-lg-4 col-md-12 col-12 col-sm-12">
				<div class="card">
					<div class="card-header">
						<h4>Tambah Produk</h4>
						<div class="card-header-action">
							<a data-collapse="#mycard-collapse" class="btn btn-icon btn-info" href="#"><i class="fas fa-minus"></i></a>
						</div>
					</div>
					<div class="collapse show" id="mycard-collapse">
						<div class="card-body">
							<form enctype="multipart/form-data" method="POST" action="<?php echo base_url("barang/create_action"); ?>" class="needs-validation" novalidate="">

								<div class="form-group row mb-4">
									<!--<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>-->
									<div class="col-sm-12 col-md-7">
										<div id="image-preview" class="image-preview">
											<label for="image-upload" id="image-label">Pilih Foto</label>
											<input type="file" name="image" id="image-upload" />
										</div>
									</div>
								</div>
								<div class="form-group">
									<label for="nama">Kode Produk</label>
									<input id="nama" type="text" class="form-control" name="kode" value="<?php echo $code_barang; ?>" tabindex="1" readonly autofocus>
									<div class="invalid-feedback">
										Nama Unit Masih Kosong
									</div>
								</div>
								<div class="form-group">
									<label for="nama">Nama Produk</label>
									<input id="nama" type="nama" class="form-control" name="nama" tabindex="1" required autofocus>
									<div class="invalid-feedback">
										Nama Unit Masih Kosong
									</div>
								</div>
								<div class="form-group">
									<label for="nama">Deskripsi</label>
									<textarea id="note" type="text" class="form-control" name="note" tabindex="1" required autofocus></textarea>
									<div class="invalid-feedback">
										Deskripsi Masih Kosong
									</div>
								</div>
								<div class="form-row">
									<div class="form-group col-6">
										<label for="nama">Minimal Stock</label>
										<input id="nama" type="number" class="form-control" name="min_stock" tabindex="1" required autofocus>
										<div class="invalid-feedback">
											Nama Unit Masih Kosong
										</div>
									</div>
									<div class="form-group col-6">
										<label for="nama">Minimal Grosir</label>
										<input id="nama" type="number" class="form-control" name="min_grosir" tabindex="1" required autofocus>
										<div class="invalid-feedback">
											Minimal Grosir Masih Kosong
										</div>
									</div>
								</div>

								<div class="form-group">
									<label>Pilih Kategori</label>
									<select name="category" class="form-control select2">
										<option value="">Pilih Kategori</option>
										<?php foreach ($kategori as $kategories) { ?>
											<option value=<?php echo $kategories->id; ?>><?php echo $kategories->nama; ?></option>
										<?php } ?>
									</select>
								</div>
								<div class="form-group">
									<label>Pilih Satuan</label>
									<select name="unit" class="form-control select2">
										<option value="">Pilih Satuan</option>
										<?php foreach ($unit as $units) { ?>
											<option value=<?php echo $units->id; ?>><?php echo $units->nama; ?></option>
										<?php } ?>
									</select>
								</div>

								<div class="form-group">
									<label>Pilih Rak</label>
									<select name="rak" class="form-control select2">
										<option value="">Pilih Rak</option>
										<?php foreach ($rak as $raks) { ?>
											<option value=<?php echo $raks->id; ?>><?php echo $raks->nama; ?></option>
										<?php } ?>
									</select>
								</div>

								<div class="form-group">
									<button type="submit" class="btn btn-success btn-lg btn-block" tabindex="4">
										Tambah
									</button>
								</div>
							</form>

							<?php if ($this->session->flashdata('pesan')) { ?>
								<div class="alert alert-warning alert-dismissible show fade">
									<div class="alert-body">
										<button class="close" data-dismiss="alert">
											<span>&times;</span>
										</button>
										<?php echo $this->session->flashdata('pesan');
										?>
									</div>
								</div>
							<?php } ?>

						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-8 col-md-12 col-12 col-sm-12">
				<div class="card">
					<div class="card-header">
						<h4>Data Produk</h4>
					</div>
					<div class="card-body">
						<div class="table-responsive">
							<table id="unit_tabel" class="table table-striped">
								<thead>
									<tr>
										<th>
											Gambar
										</th>
										<th>Kode</th>
										<th>Nama</th>
										<th>Kategori</th>
										<th>Satuan</th>
										<th>Rak</th>
										<th>Min Stock</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="modal fade" tabindex="-1" role="dialog" id="exampleModalDelete">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Hapus Produk</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p>Apakah Anda Yakin ?</p>
			</div>
			<div class="modal-footer bg-whitesmoke br">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
				<a id="btn-delete" type="button" href="#" class="btn btn-danger">Hapus</a>
			</div>
		</div>
	</div>
</div>



<script type="text/javascript">
	var save_method; //for save method string
	var table;

	$(document).ready(function() {
		//datatables
		table = $('#unit_tabel').DataTable({
			// Load data for the table's content from an Ajax source
			"ajax": {
				"url": '<?php echo site_url('barang/json'); ?>',
				"type": "POST"
			},
			//Set column definition initialisation properties.
			"columns": [{
					"data": null,
					"render": function(data, type, row) {
						return ' <img alt="image" src="<?php echo site_url('assets/uploads/'); ?>' + row.image + '" class="rounded-circle" width="35" data-toggle="tooltip" title="' + row.nama + '">';
					}
				},
				{
					"data": "kode"
				},
				{
					"data": "nama"
				},
				{
					"data": "category"
				},
				{
					"data": "unit"
				},
				{
					"data": null,
					"render": function(data, type, row) {
						return ' <div class="badge badge-info">' + row.rak + '</div>';
					}
				},
				{
					"data": null,
					"render": function(data, type, row) {
						return ' <div class="badge badge-success">' + row.stock + '</div>';
					}
				},
				{
					"data": null,
					"render": function(data, type, row) {
						return '<a href="<?php echo site_url("master/produk/edit/") ?>' + row.id + '" class="btn btn-icon btn-primary"><i class="far fa-edit"></i></a> <a onclick=deleteConfirm("' + row.id + '") href="#!" class="btn btn-icon btn-danger exampleModalDelete" data-toggle="modal" data-target="#exampleModalDelete"><i class="fas fa-times"></i></a>';
					}
				}
			],

		});



	});

	function deleteConfirm(url) {
		$('#btn-delete').attr('href', "<?php echo site_url("barang/delete"); ?>/" + url);
		$('#exampleModalDelete').modal();
	}

	$.uploadPreview({
		input_field: "#image-upload", // Default: .image-upload
		preview_box: "#image-preview", // Default: .image-preview
		label_field: "#image-label", // Default: .image-label
		label_default: "Choose File", // Default: Choose File
		label_selected: "Change File", // Default: Change File
		no_label: false, // Default: false
		success_callback: null // Default: null
	});
</script>