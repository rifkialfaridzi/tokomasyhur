<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <link rel="stylesheet" href=<?php echo base_url("assets/modules/bootstrap/css/bootstrap.min.css"); ?>>
    <style>
        .tg {
            border-collapse: collapse;
            border-spacing: 0;
        }

        .tg td {
            font-family: Arial, sans-serif;
            font-size: 14px;
            padding: 10px 5px;
            border-style: dashed;
            border-width: 1px;
            overflow: hidden;
            word-break: normal;
            border-color: black;
        }

        .tg th {
            font-family: Arial, sans-serif;
            font-size: 14px;
            font-weight: normal;
            padding: 10px 5px;
            border-style: dashed;
            border-width: 1px;
            overflow: hidden;
            word-break: normal;
            border-color: black;
        }

        .tg .tg-lboi {
            border-color: inherit;
            text-align: left;
            vertical-align: middle
        }

        .tg .tg-0pky {
            border-color: inherit;
            text-align: left;
            vertical-align: top
        }
    </style>
</head>

<body>
    <div class="container">
        <table class="tg">
            <?php $counter = 3;
            for ($i = 0; $i < $counter; $i++) {

            ?>
                <tr>
                    <th class="tg-lboi">
                        <div class="thumbnail">
                            <img alt="image" style="width:23.3%" src="<?php echo site_url($image); ?>">
                            <div class="caption">
                                <small><p style="text-align: center;font-size:9px"><?php echo $kode; ?></p></small>
                            </div>
                            <div class="caption">
                                <p style="text-align: center; font-weight:bold"><?php echo $produk_name; ?></p>
                            </div>
                            <div class="caption">
                                <p style="text-align: center; font-weight:bold;font-style: italic; color:white;background-color: #ff1e56"><?php echo $price; ?></p>
                            </div>
                            <p style="text-align: center; font-weight:bold;font-size:10px">Al Masyhur</p>
                        </div>
                    </th>
                    <th class="tg-lboi">
                        <div class="thumbnail">
                            <img alt="image" style="width:23.3%" src="<?php echo site_url($image); ?>">
                            <div class="caption">
                                <small><p style="text-align: center;font-size:9px"><?php echo $kode; ?></p></small>
                            </div>
                            <div class="caption">
                                <p style="text-align: center; font-weight:bold"><?php echo $produk_name; ?></p>
                            </div>
                            <div class="caption">
                                <p style="text-align: center; font-weight:bold;font-style: italic; color:white;background-color: #ff1e56"><?php echo $price; ?></p>
                            </div>
                            <p style="text-align: center; font-weight:bold;font-size:10px">Al Masyhur</p>
                        </div>
                    </th>
                    <th class="tg-lboi">
                        <div class="thumbnail">
                            <img alt="image" style="width:23.3%" src="<?php echo site_url($image); ?>">
                            <div class="caption">
                                <small><p style="text-align: center;font-size:9px"><?php echo $kode; ?></p></small>
                            </div>
                            <div class="caption">
                                <p style="text-align: center; font-weight:bold"><?php echo $produk_name; ?></p>
                            </div>
                            <div class="caption">
                                <p style="text-align: center; font-weight:bold;font-style: italic; color:white;background-color: #ff1e56"><?php echo $price; ?></p>
                            </div>
                            <p style="text-align: center; font-weight:bold;font-size:10px">Al Masyhur</p>
                        </div>
                    </th>
                    <th class="tg-lboi">
                        <div class="thumbnail">
                            <img alt="image" style="width:23.3%" src="<?php echo site_url($image); ?>">
                            <div class="caption">
                                <small><p style="text-align: center;font-size:9px"><?php echo $kode; ?></p></small>
                            </div>
                            <div class="caption">
                                <p style="text-align: center; font-weight:bold"><?php echo $produk_name; ?></p>
                            </div>
                            <div class="caption">
                                <p style="text-align: center; font-weight:bold;font-style: italic; color:white;background-color: #ff1e56"><?php echo $price; ?></p>
                            </div>
                            <p style="text-align: center; font-weight:bold;font-size:10px">Al Masyhur</p>
                            
                        </div>
                    </th>
                </tr>

            <?php } ?>
        </table>
        
        <p style="text-align: center;position: fixed;bottom: 0">Di Cetak Pada <?php echo date('Y-m-d H:i:s'); ?></p>
       
            
    </div>
</body>

</html>