<section class="section">
    <div class="section-header">
        <h1>Halaman Input Stock Produk</h1>
    </div>

    <div class="section-body">
        <div class="row">
            <div class="col-lg-4 col-md-12 col-12 col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Input Stock Produk</h4>
                        <div class="card-header-action">
                            <a data-collapse="#mycard-collapse" class="btn btn-icon btn-info" href="#"><i class="fas fa-minus"></i></a>
                        </div>
                    </div>
                    <div class="collapse show" id="mycard-collapse">
                        <div class="card-body">
                            <ul class="list-group">
                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                    Unlocated Stock
                                    <span class="badge badge-primary badge-pill"><?php echo $unlocated_stock; ?></span>
                                </li>
                            </ul>
                            <br>
                            <form enctype="multipart/form-data" method="POST" action="<?php echo base_url("barang_masuk/stock_action/") . $id; ?>" class="needs-validation" novalidate="">
                                <input id="unlocated_stock" type="text" class="form-control" value="<?php echo $unlocated_stock; ?>" name="unlocated_stock" tabindex="1" hidden>
                                <div class="form-row">
                                    <div class="form-group col-md-8">
                                        <label for="nama">Harga Beli</label>
                                        <input id="harga_pembelian" type="number" value="<?php echo $harga_pembelian; ?>" class="form-control" name="harga_pembelian" tabindex="1" currency readonly autofocus>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="nama">Barang Masuk</label>
                                        <input id="jumlah_masuk" type="number" value="<?php echo $jumlah; ?>" class="form-control" name="harga_pembelian" tabindex="1" currency readonly autofocus>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="nama">Kode</label>
                                    <input id="kode" type="text" class="form-control" value="<?php echo $kode; ?>" name="kode" tabindex="1" readonly autofocus>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-6">
                                        <label for="nama">Harga Jual</label>
                                        <input id="harga_jual" type="number" class="form-control" name="harga_jual" tabindex="1" currency required autofocus>
                                        <div class="invalid-feedback">
                                            Harga Jual Masih Kosong
                                        </div>
                                    </div>

                                    <div class="form-group col-6">
                                        <label for="nama">Harga Grosir</label>
                                        <input id="harga_grosir" type="number" class="form-control" name="harga_grosir" tabindex="1" currency required autofocus>
                                        <div class="invalid-feedback">
                                            Harga Grosir Masih Kosong
                                        </div>
                                    </div>
                                </div>


                                <!--
                                <div class="form-row">
                                    <div class="form-group  col-md-8">
                                        <label for="nama">Harga Grosir</label>
                                        <input id="nama" type="number" class="form-control" name="harga_grosir" tabindex="1" currency required autofocus>
                                        <div class="invalid-feedback">
                                            Harga Grosir Masih Kosong
                                        </div>
                                    </div>

                                    <div class="form-group  col-md-4">
                                        <label for="nama">Minimal Grosir</label>
                                        <input id="nama" type="number" class="form-control" name="min_grosir" tabindex="1" currency required autofocus>
                                        <div class="invalid-feedback">
                                            Minimal Grosir Masih Kosong
                                        </div>
                                    </div>
                                </div>
                                -->

                                <div class="form-group">
                                    <label for="nama">Tanggal Input Stock Produk</label>
                                    <input type="text" name="created_at" class="form-control datepicker" required autofocus>
                                    <div class="invalid-feedback">
                                        Tanggal Input Stock Produk Masih Kosong
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="nama">Jumlah</label>
                                    <input id="jumlah" type="number" class="form-control" name="jumlah" tabindex="1" required autofocus>
                                    <div class="invalid-feedback">
                                        Jumlah Masih Kosong
                                    </div>
                                </div>


                                <div class="form-group">
                                    <button id="tambah-faktur" type="submit" class="btn btn-success btn-lg btn-block" tabindex="4">
                                        Tambah
                                    </button>
                                </div>
                            </form>

                            <div id="notif_stock" class="alert alert-danger alert-dismissible show fade">
                                <div class="alert-body">
                                    <?php

                                    $jumlah_refund = $refund == null ? 0 : $refund;
                                    $available_stock_masuk = $jumlah - $jumlah_refund;
                                    ?>
                                    Stock Tidak Mencukupi, Jumlah Stock Masuk : <?php echo $available_stock_masuk; ?>
                                </div>
                            </div>

                            <div id="notif_harga" class="alert alert-danger alert-dismissible show fade">
                                <div class="alert-body">
                                    Harga harus lebih dari harga beli
                                </div>
                            </div>

                            <?php if ($this->session->flashdata('pesan')) { ?>
                                <div class="alert alert-warning alert-dismissible show fade">
                                    <div class="alert-body">
                                        <button class="close" data-dismiss="alert">
                                            <span>&times;</span>
                                        </button>
                                        <?php echo $this->session->flashdata('pesan');
                                        ?>
                                    </div>
                                </div>
                            <?php } ?>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8 col-md-12 col-12 col-sm-12">
                <div class="card profile-widget">
                    <div class="profile-widget-header">
                        <img alt="image" src="<?php echo base_url('assets/uploads/') . $product_data->image; ?>" class="rounded-circle profile-widget-picture">
                        <div class="profile-widget-items">
                            <div class="profile-widget-item">
                                <div class="profile-widget-item-label">Available Stock</div>
                                <div class="profile-widget-item-value"><?php echo $product_data->stock == null ? 0 : $product_data->stock; ?></div>
                            </div>
                            <div class="profile-widget-item">
                                <div class="profile-widget-item-label">Stock Masuk</div>
                                <div class="profile-widget-item-value"><?php echo $stcok_masuk_data->jumlah == null ? 0 : $stcok_masuk_data->jumlah ?></div>
                            </div>
                            <div class="profile-widget-item">
                                <div class="profile-widget-item-label">Refund</div>
                                <div class="profile-widget-item-value"><?php echo $refund; ?></div>
                            </div>
                        </div>
                    </div>
                    <div class="profile-widget-description pb-0">
                        <div class="profile-widget-name"><?php echo $barang; ?> <div class="text-muted d-inline font-weight-normal">
                                <div class="slash"></div> <?php echo $supplier_id; ?>
                            </div>
                        </div>
                        <p><?php echo $note; ?></p>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header">
                        <h4>Riwayat Stock Produk</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="unit_tabel" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Nama</th>
                                        <th>Supplier</th>
                                        <th>Harga Pembelian</th>
                                        <th>Harga Penjualan</th>
                                        <th>Harga Grosir</th>
                                        <th>Jumlah</th>
                                        <th>Tanggal</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>





            </div>
        </div>
</section>

<div class="modal fade" tabindex="-1" role="dialog" id="exampleModalDelete">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Hapus Produk Masuk</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Apakah Anda Yakin ?</p>
            </div>
            <div class="modal-footer bg-whitesmoke br">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <a id="btn-delete" type="button" href="#" class="btn btn-danger">Hapus</a>
            </div>
        </div>
    </div>
</div>



<script type="text/javascript">
    $("#notif_stock").hide();
    $("#notif_harga").hide();

    $('#tambah-faktur').click(function() {
        // setTimeout(function() {
        //     window.open('<?php echo site_url('pembelian/masuk/stock/print/') . $id; ?>', '_blank')
        // }, 1000);
        //Some code
    });
    var save_method; //for save method string
    var table;

    $(document).ready(function() {
        //datatables
        table = $('#unit_tabel').DataTable({
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": '<?php echo site_url('stock/stock_byid_product/') . $barang_id; ?>',
                "type": "POST"
            },
            //Set column definition initialisation properties.
            "columns": [{
                    "data": "nama_barang"
                },
                {
                    "data": "nama_supplier"
                },
                {
                    "data": "harga_pembelian"
                },
                {
                    "data": "harga_penjualan"
                },
                {
                    "data": "harga_grosir"
                },
                {
                    "data": null,
                    "render": function(data, type, row) {
                        return ' <div class="badge badge-info">' + row.amount + '</div>';
                    }
                },
                {
                    "data": null,
                    "render": function(data, type, row) {
                        return ' <div class="badge badge-success">' + row.created_at + '</div>';
                    }
                }
            ],

        });
    });

    function deleteConfirm(url) {
        $('#btn-delete').attr('href', "<?php echo site_url("pembelian/masuk/delete"); ?>/" + url);
        $('#exampleModalDelete').modal();
    }

    $.uploadPreview({
        input_field: "#image-upload", // Default: .image-upload
        preview_box: "#image-preview", // Default: .image-preview
        label_field: "#image-label", // Default: .image-label
        label_default: "Choose File", // Default: Choose File
        label_selected: "Change File", // Default: Change File
        no_label: false, // Default: false
        success_callback: null // Default: null
    });

    // CEK AVAILABLE STOCK MASUK
    // $('#jumlah').keypress(function() {
    //     console.log(this.value);
    // });
    $('#jumlah').keyup(function() {

        var jumlah_masuk = <?php echo $jumlah; ?>;
        var jumlah_refund = <?php echo $refund == null ? 0 : $refund; ?>;

        var available_stock = jumlah_masuk - jumlah_refund;

        console.log(available_stock);
        if (this.value > available_stock) {
            $("#notif_stock").show();
        } else {
            $("#notif_stock").hide();
        }

        //console.log(this.value);

    });

    $('#harga_jual').keyup(function() {

        if (parseInt(this.value) < parseInt($("#harga_pembelian").val())) {
            $("#notif_harga").show();
        } else {
            $("#notif_harga").hide();
        }

        //console.log(this.value);
    });

    $('#harga_grosir').keyup(function() {

        if (parseInt(this.value) < parseInt($("#harga_pembelian").val())) {
            $("#notif_harga").show();
        } else {
            $("#notif_harga").hide();
        }

        //console.log(this.value);
    });

    // Daterangepicker
    if (jQuery().daterangepicker) {
        if ($(".datepicker").length) {
            $('.datepicker').daterangepicker({
                locale: {
                    format: 'YYYY-MM-DD'
                },
                singleDatePicker: true,
            });
        }
        if ($(".datetimepicker").length) {
            $('.datetimepicker').daterangepicker({
                locale: {
                    format: 'YYYY-MM-DD hh:mm'
                },
                singleDatePicker: true,
                timePicker: true,
                timePicker24Hour: true,
            });
        }
        if ($(".daterange").length) {
            $('.daterange').daterangepicker({
                locale: {
                    format: 'YYYY-MM-DD'
                },
                drops: 'down',
                opens: 'right'
            });
        }
    }
</script>