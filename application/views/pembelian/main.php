<section class="section">
	<div class="section-header">
		<h1>Halaman Produk Masuk</h1>
	</div>

	<div class="section-body">
		<div class="row">
			<div class="col-lg-4 col-md-12 col-12 col-sm-12">
				<div class="card">
					<div class="card-header">
						<h4>Produk Masuk</h4>
						<div class="card-header-action">
							<a data-collapse="#mycard-collapse" class="btn btn-icon btn-info" href="#"><i class="fas fa-minus"></i></a>
						</div>
					</div>
					<div class="collapse show" id="mycard-collapse">
						<div class="card-body">
							<form enctype="multipart/form-data" method="POST" action="<?php echo base_url("barang_masuk/create_action"); ?>" class="needs-validation" novalidate="">

								<div class="form-group">
									<label>Pilih Produk Masuk</label>
									<select name="barang" class="form-control select2" required>
										<option value="">Pilih Produk</option>
										<?php foreach ($produk as $produks) { ?>
											<option value=<?php echo $produks->id; ?>><?php echo $produks->nama; ?> - <?php echo $produks->kode; ?></option>
										<?php } ?>
									</select>
									<div class="invalid-feedback">
										Produk Masih Kosong
									</div>
								</div>
								<div class="form-group">
									<label>Pilih Supplier</label>
									<select name="supplier" class="form-control select2" required>
										<option value="">Pilih Supplier</option>
										<?php foreach ($supplier as $suppliers) { ?>
											<option value=<?php echo $suppliers->id; ?>><?php echo $suppliers->nama; ?></option>
										<?php } ?>
									</select>
									<div class="invalid-feedback">
										Supplier Masih Kosong
									</div>
								</div>

								<div class="form-group">
									<label for="nama">Harga Beli</label>
									<input id="nama" type="number" class="form-control" name="harga_pembelian" tabindex="1" currency required autofocus>
									<div class="invalid-feedback">
										Harga Beli Masih Kosong
									</div>
								</div>

								<div class="form-group">
									<label for="nama">Tanggal Barang Datang</label>
									<input type="text" name="created_at" class="form-control datepicker" required autofocus>
									<div class="invalid-feedback">
									Tanggal Barang Datang Masih Kosong
									</div>
								</div>

								<div class="form-group">
									<label for="nama">Jumlah</label>
									<input id="nama" type="number" class="form-control" name="jumlah" tabindex="1" required autofocus>
									<div class="invalid-feedback">
										Jumlah Masih Kosong
									</div>
								</div>


								<div class="form-group">
									<button type="submit" class="btn btn-success btn-lg btn-block" tabindex="4">
										Tambah
									</button>
								</div>
							</form>

							<?php if ($this->session->flashdata('pesan')) { ?>
								<div class="alert alert-warning alert-dismissible show fade">
									<div class="alert-body">
										<button class="close" data-dismiss="alert">
											<span>&times;</span>
										</button>
										<?php echo $this->session->flashdata('pesan');
										?>
									</div>
								</div>
							<?php } ?>

						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-8 col-md-12 col-12 col-sm-12">
				<div class="card">
					<div class="card-header">
						<h4>Data Produk Masuk</h4>
					</div>
					<div class="card-body">
						<div class="form-group">
							<label>Pilih Tanggal</label>
							<div class="input-group">
								<div class="input-group-prepend">
									<div class="input-group-text">
										<i class="fas fa-calendar"></i>
									</div>
								</div>
								<input type="text" class="form-control daterange-cus">
								<div class="input-group-prepend">
									<div class="input-group-text">
										<a id="cetak" href="#" class="btn btn-icon icon-left btn-primary"><i class="fas fa-print"></i> Cetak</a>
									</div>
								</div>
							</div>
						</div>
						<div class="table-responsive">
							<table id="unit_tabel" class="table table-striped">
								<thead>
									<tr>
										<th>
											Status
										</th>
										<th>Gambar</th>
										<th>Nama</th>
										<th>Supplier</th>
										<th>Harga</th>
										<th>Jumlah</th>
										<th>Tanggal</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="modal fade" tabindex="-1" role="dialog" id="exampleModalDelete">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Hapus Produk Masuk</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p>Apakah Anda Yakin ?</p>
			</div>
			<div class="modal-footer bg-whitesmoke br">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
				<a id="btn-delete" type="button" href="#" class="btn btn-danger">Hapus</a>
			</div>
		</div>
	</div>
</div>



<script type="text/javascript">
	var save_method; //for save method string
	var table;

	var date = new Date();
    var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
    var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);

    var firstdatestring = firstDay.getFullYear() + "-" + (("0" + (firstDay.getMonth() + 1)).slice(-2)) + "-" + ("0" + firstDay.getDate()).slice(-2);
    var lastdatestring = lastDay.getFullYear() + "-" + (("0" + (lastDay.getMonth() + 1)).slice(-2)) + "-" + ("0" + lastDay.getDate()).slice(-2);



	$(document).ready(function() {
		
        var getFirstDate = firstdatestring;
        var getLastDate = lastdatestring;
		//datatables
		table = $('#unit_tabel').DataTable({
			// Load data for the table's content from an Ajax source
			"order": [
				[6, "desc"]
			],
			"ajax": {
				"url": '<?php echo site_url('barang_masuk/stockin_byrange_json/'); ?>' + getFirstDate + "/" + getLastDate,
				"type": "POST"
			},
			//Set column definition initialisation properties.
			"columns": [{
					"data": null,
					"render": function(data, type, row) {
						if (row.status == 0) {
							return '<a href="#" class="btn btn-icon btn-danger" title="Perlu Perhatian"><i class="fas fa-exclamation-triangle"></i></a>';
						} else if (row.status == 1) {
							return '<a href="#" class="btn btn-icon btn-success" title="Selesai"><i class="fas fa-check"></i></a>';
						} else {
							return '<a href="#" class="btn btn-icon btn-warning" title="Ada Refund"><i class="fas fa-exclamation-triangle"></i></a>';
						}

					}
				},
				{
					"data": null,
					"render": function(data, type, row) {
						return ' <img alt="image" src="<?php echo site_url('assets/uploads/'); ?>' + row.image + '" class="rounded-circle" width="35" data-toggle="tooltip" title="' + row.nama + '">';
					}
				},
				{
					"data": "nama_barang"
				},
				{
					"data": "nama_supplier"
				},
				{
					"data": "harga_pembelian"
				},
				{
					"data": null,
					"render": function(data, type, row) {
						return ' <div class="badge badge-info">' + row.jumlah + '</div>';
					}
				},
				{
					"data": null,
					"render": function(data, type, row) {
						return ' <div class="badge badge-success">' + row.created_at + '</div>';
					}
				},
				{
					"data": null,
					"render": function(data, type, row) {

						if (row.status == 2) {
							return '<div class="dropdown d-inline"><button class="btn btn-sm btn-primary dropdown-toggle" type="button" id="dropdownMenuButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Option</button><div class="dropdown-menu"><a class="dropdown-item has-icon" href="<?php echo site_url("pembelian/masuk/refund/") ?>' + row.id + '"><i class="far fa-heart"></i> Refund</a><div class="dropdown-divider"></div><a class="dropdown-item has-icon  text-primary" href="<?php echo site_url("pembelian/masuk/edit/") ?>' + row.id + '"><i class="far fa-edit"></i> Ubah</a><a onclick=deleteConfirm("' + row.id + '") class="dropdown-item has-icon  text-danger" href="#"><i class="fas fa-times"></i> Hapus</a></div></div>';
						} else {
							return '<div class="dropdown d-inline"><button class="btn btn-sm btn-primary dropdown-toggle" type="button" id="dropdownMenuButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Option</button><div class="dropdown-menu"><a class="dropdown-item has-icon" href="<?php echo site_url("pembelian/masuk/stock/") ?>' + row.id + '"><i class="far fa-file"></i> Stock</a><div class="dropdown-divider"></div><a class="dropdown-item has-icon  text-primary" href="<?php echo site_url("pembelian/masuk/edit/") ?>' + row.id + '"><i class="far fa-edit"></i> Ubah</a><a onclick=deleteConfirm("' + row.id + '") class="dropdown-item has-icon  text-danger" href="#"><i class="fas fa-times"></i> Hapus</a></div></div>';
						}
					}
				}
			],

		});

		

        $("#cetak").click(function() {
            window.open("<?php echo site_url('barang_masuk/print_stockin_byrange/'); ?>" + getFirstDate + "/" + getLastDate);
            // console.log(getFirstDate);
            // console.log(getLastDate);
        });

        $('.daterange-cus').daterangepicker({
                locale: {
                    format: 'YYYY-MM-DD'
                },
                drops: 'down',
                opens: 'right',
            },
            function(start, end) {
                console.log("Callback has been called!" + start.format('YYYY-MM-DD') + " to " + end.format('YYYY-MM-DD'));

                table.ajax.url("<?php echo site_url('barang_masuk/stockin_byrange_json/'); ?>"+ start.format('YYYY-MM-DD') + "/" + end.format('YYYY-MM-DD')).load();

                getFirstDate = start.format('YYYY-MM-DD');
                getLastDate = end.format('YYYY-MM-DD');

            }).val(firstdatestring + "-" + lastdatestring);

	});

	function deleteConfirm(url) {
		$('#btn-delete').attr('href', "<?php echo site_url("pembelian/masuk/delete"); ?>/" + url);
		$('#exampleModalDelete').modal();
	}

	$.uploadPreview({
		input_field: "#image-upload", // Default: .image-upload
		preview_box: "#image-preview", // Default: .image-preview
		label_field: "#image-label", // Default: .image-label
		label_default: "Choose File", // Default: Choose File
		label_selected: "Change File", // Default: Change File
		no_label: false, // Default: false
		success_callback: null // Default: null
	});

	// Daterangepicker
	if (jQuery().daterangepicker) {
		if ($(".datepicker").length) {
			$('.datepicker').daterangepicker({
				locale: {
					format: 'YYYY-MM-DD'
				},
				singleDatePicker: true,
			});
		}
		if ($(".datetimepicker").length) {
			$('.datetimepicker').daterangepicker({
				locale: {
					format: 'YYYY-MM-DD hh:mm'
				},
				singleDatePicker: true,
				timePicker: true,
				timePicker24Hour: true,
			});
		}
		if ($(".daterange").length) {
			$('.daterange').daterangepicker({
				locale: {
					format: 'YYYY-MM-DD'
				},
				drops: 'down',
				opens: 'right'
			});
		}
	}
</script>