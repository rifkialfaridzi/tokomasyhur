<!DOCTYPE html>
<html lang="en">
<body background="assets/img/bgcobo/2.jpg">
<head>
	<meta charset="UTF-8">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
	<link rel="icon"
	type="image/png"
	href="<?php echo base_url('assets/alt.png')?>"/>
	<title>Login</title>

	<!-- General CSS Files -->
	<link rel="stylesheet" href=<?php echo base_url("assets/modules/bootstrap/css/bootstrap.min.css"); ?>>
	<link rel="stylesheet" href=<?php echo base_url("assets/modules/fontawesome/css/all.min.css"); ?>>

	<!-- CSS Libraries -->
	<link rel="stylesheet" href=<?php echo base_url("assets/modules/bootstrap-social/bootstrap-social.css"); ?>>

	<!-- Template CSS -->
	<link rel="stylesheet" href=<?php echo base_url("assets/css/style.css"); ?>>
	<link rel="stylesheet" href=<?php echo base_url("assets/css/components.css"); ?>>
	<!-- Start GA -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-94034622-3"></script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());

		gtag('config', 'UA-94034622-3');
	</script>
	<!-- /END GA -->
</head>

<body style="background-image: url('<?php echo base_url('assets/bg1.jpg') ?>'); background-size: cover;">
	<div id="app">
		<section class="section">
			<div class="container mt-5">
				<div class="row">
					<div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
						<div class="login-brand">
							<img src="assets/LGO.png" alt="logo" width="200">
						</div>

						<div class="card card-primary">
							<div class="card-header">
								<h4>Login</h4>
							</div>

							<div class="card-body">
								<form method="POST" action="<?php echo base_url("index.php/auth/login"); ?>" class="needs-validation" novalidate="">
									<div class="form-group">
										<label for="username">Username</label>
										<input id="username" type="username" class="form-control" name="username" tabindex="1" required autofocus>
										<div class="invalid-feedback">
											Please fill in your email
										</div>
									</div>

									<div class="form-group">
										<div class="d-block">
											<label for="password" class="control-label">Password</label>
											<div class="float-right">
												
											</div>
										</div>
										<input id="password" type="password" class="form-control" name="password" tabindex="2" required>
										<div class="invalid-feedback">
											please fill in your password
										</div>
									</div>
									<br>
									<div class="form-group">
										<button type="submit" class="btn btn-primary btn-lg btn-block" tabindex="4">
											Login
										</button></div>
									
									<?php if($this->session->flashdata('pesan')){ ?>
									<div class="alert alert-danger alert-dismissible show fade">
										<div class="alert-body">
											<button class="close" data-dismiss="alert">
												<span>&times;</span>
											</button>
											<?php echo $this->session->flashdata('pesan');} ?>
										</div>
									</div>
								
									<div class="form-group">
										
									</div>
								</form>

							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>

	<!-- General JS Scripts -->
	<script src=<?php echo base_url("assets/modules/jquery.min.js"); ?>></script>
	<script src=<?php echo base_url("assets/modules/popper.js"); ?>></script>
	<script src=<?php echo base_url("assets/modules/tooltip.js"); ?>></script>
	<script src=<?php echo base_url("assets/modules/bootstrap/js/bootstrap.min.js"); ?>></script>
	<script src=<?php echo base_url("assets/modules/nicescroll/jquery.nicescroll.min.js"); ?>></script>
	<script src=<?php echo base_url("assets/modules/moment.min.js"); ?>></script>
	<script src=<?php echo base_url("assets/js/stisla.js"); ?>></script>

	<!-- JS Libraies -->

	<!-- Page Specific JS File -->

	<!-- Template JS File -->
	<script src=<?php echo base_url("assets/js/scripts.js"); ?>></script>
	<script src=<?php echo base_url("assets/js/custom.js"); ?>></script>
</body>

</html>
