<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Owner extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $data_session = $this->session->userdata;
       
		if ((!$this->session->userdata('logged_in')) || $data_session['level'] !=3) {
			redirect('auth'); // Cek udah login apa belum, kalo belum login dulu
        }
        
        $this->load->model('Category_model');
        $this->load->model('Barang_model');
        $this->load->model('Supplier_model');
        $this->load->model('Penjualan_model');
        $this->load->model('Penjualan_detail_model');
        $this->load->model('Stock_model');

        $this->load->model('auth_model');
        $this->load->library('form_validation');
        $this->load->library('datatables');
    }

    public function index()
    {
        $transaksi_terakhir = $this->Penjualan_model->penjualan_terakhir();
        $pembelian_terakhir = $this->Stock_model->pembelian_terakhir();
        $data['page_title'] = 'Halaman Owner';
        $data['main_content'] = 'owner/main';
        $data['transaksi_terakhir'] = $transaksi_terakhir == null ? [] : $transaksi_terakhir;
        $data['pembelian_terakhir'] = $pembelian_terakhir == null ? [] : $pembelian_terakhir;
        $this->load->view('template',$data);
    }

    public function json()
    {
        header('Content-Type: application/json');
        echo $this->Barang_model->json();
    }

    public function dashboard($month){
        $totalOrder =  count($this->Penjualan_model->total_order_bymonth($month));
        $totalProdukOrder = $this->Penjualan_model->produk_total_bymonth($month)->total_produk;
        $totalPendapatan = $this->Penjualan_model->produk_total_penjualan($month)->total_penjualan;
        $topProduk = $this->Penjualan_model->top_product_bymonth($month);
        $produk = $this->Barang_model->total_produk_bymonth($month);
        $supplier = $this->Supplier_model->total_supplier_bymonth($month);

        $data = [
            'totalOrder' => $totalOrder == null ? 0 : $totalOrder,
            'totalProdukOrder' => $totalProdukOrder == null ? 0 : $totalProdukOrder,
            'totalPendapatan' => $totalPendapatan == null ? 0 : $totalPendapatan,
            'topProduk' => $topProduk == null ? [] : $topProduk,
            'produk' => $produk ,
            'supplier' => $supplier == null ? 0 : count($supplier),
        ];

        echo json_encode($data);

    }

    public function print_nota($id)
    {

        $detailTransaksi = $this->Penjualan_model->get_data_product_byid($id)->get()->result();

        $transaksi = $this->Penjualan_model->get_by_id($id);

        if ($transaksi) {

            $data = [
                'transaksi' => $transaksi,
                'detailTransaksi' => $detailTransaksi,
                'company' => 'Maharany Skar Solo',
                'image_logo' => 'assets/Logo.png',
            ];
            // echo json_encode($data);
            // var_dump($data);
            $this->load->library('pdf');

            $this->pdf->setPaper('A4', 'potrait');
            $this->pdf->set_option('isRemoteEnabled', TRUE);
            $this->pdf->filename = "print_qrcode_" . $transaksi->kode . ".pdf";
            $this->pdf->load_view('kasir/print_nota', $data);
        } else {
            $this->session->set_flashdata('pesan', 'Data Tidak DItemukan');
            redirect(site_url('kasir/produk'));
        }
    }

    public function read($id)
    {
        $row = $this->Barang_model->get_by_id($id);
        if ($row) {
            $data = array(
                'id' => $row->id,
                'kode' => $row->kode,
                'nama' => $row->nama,
                'unit' => $row->unit,
                'category' => $row->category,
                'image' => $row->image,
                'stock' => $row->stock,
                'harga_penjualan' => $row->harga_penjualan,
                'min_stock' => $row->min_stock,
                'created_at' => $row->created_at,
            );
            $this->load->view('barang/barang_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('barang'));
        }
    }

    public function create()
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('barang/create_action'),
            'id' => set_value('id'),
            'kode' => set_value('kode'),
            'nama' => set_value('nama'),
            'unit' => set_value('unit'),
            'category' => set_value('category'),
            'image' => set_value('image'),
            'stock' => set_value('stock'),
            'harga_penjualan' => set_value('harga_penjualan'),
            'min_stock' => set_value('min_stock'),
            'created_at' => set_value('created_at'),
        );
        $this->load->view('barang/barang_form', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
                'kode' => $this->input->post('kode', TRUE),
                'nama' => $this->input->post('nama', TRUE),
                'unit' => $this->input->post('unit', TRUE),
                'category' => $this->input->post('category', TRUE),
                'image' => $this->input->post('image', TRUE),
                'stock' => $this->input->post('stock', TRUE),
                'harga_penjualan' => $this->input->post('harga_penjualan', TRUE),
                'min_stock' => $this->input->post('min_stock', TRUE),
                'created_at' => $this->input->post('created_at', TRUE),
            );

            $this->Barang_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('barang'));
        }
    }

    public function update($id)
    {
        $row = $this->Barang_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('barang/update_action'),
                'id' => set_value('id', $row->id),
                'kode' => set_value('kode', $row->kode),
                'nama' => set_value('nama', $row->nama),
                'unit' => set_value('unit', $row->unit),
                'category' => set_value('category', $row->category),
                'image' => set_value('image', $row->image),
                'stock' => set_value('stock', $row->stock),
                'harga_penjualan' => set_value('harga_penjualan', $row->harga_penjualan),
                'min_stock' => set_value('min_stock', $row->min_stock),
                'created_at' => set_value('created_at', $row->created_at),
            );
            $this->load->view('barang/barang_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('barang'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
                'kode' => $this->input->post('kode', TRUE),
                'nama' => $this->input->post('nama', TRUE),
                'unit' => $this->input->post('unit', TRUE),
                'category' => $this->input->post('category', TRUE),
                'image' => $this->input->post('image', TRUE),
                'stock' => $this->input->post('stock', TRUE),
                'harga_penjualan' => $this->input->post('harga_penjualan', TRUE),
                'min_stock' => $this->input->post('min_stock', TRUE),
                'created_at' => $this->input->post('created_at', TRUE),
            );

            $this->Barang_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('barang'));
        }
    }

    public function delete($id)
    {
        $row = $this->Barang_model->get_by_id($id);

        if ($row) {
            $this->Barang_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('barang'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('barang'));
        }
    }

    public function _rules()
    {
        $this->form_validation->set_rules('kode', 'kode', 'trim|required');
        $this->form_validation->set_rules('nama', 'nama', 'trim|required');
        $this->form_validation->set_rules('unit', 'unit', 'trim|required');
        $this->form_validation->set_rules('category', 'category', 'trim|required');
        $this->form_validation->set_rules('image', 'image', 'trim|required');
        $this->form_validation->set_rules('stock', 'stock', 'trim|required');
        $this->form_validation->set_rules('harga_penjualan', 'harga penjualan', 'trim|required');
        $this->form_validation->set_rules('min_stock', 'min stock', 'trim|required');
        $this->form_validation->set_rules('created_at', 'created at', 'trim|required');

        $this->form_validation->set_rules('id', 'id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "barang.xls";
        $judul = "barang";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
        xlsWriteLabel($tablehead, $kolomhead++, "Kode");
        xlsWriteLabel($tablehead, $kolomhead++, "Nama");
        xlsWriteLabel($tablehead, $kolomhead++, "Unit");
        xlsWriteLabel($tablehead, $kolomhead++, "Category");
        xlsWriteLabel($tablehead, $kolomhead++, "Image");
        xlsWriteLabel($tablehead, $kolomhead++, "Stock");
        xlsWriteLabel($tablehead, $kolomhead++, "Harga Penjualan");
        xlsWriteLabel($tablehead, $kolomhead++, "Min Stock");
        xlsWriteLabel($tablehead, $kolomhead++, "Created At");

        foreach ($this->Barang_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
            xlsWriteLabel($tablebody, $kolombody++, $data->kode);
            xlsWriteLabel($tablebody, $kolombody++, $data->nama);
            xlsWriteNumber($tablebody, $kolombody++, $data->unit);
            xlsWriteNumber($tablebody, $kolombody++, $data->category);
            xlsWriteLabel($tablebody, $kolombody++, $data->image);
            xlsWriteLabel($tablebody, $kolombody++, $data->stock);
            xlsWriteLabel($tablebody, $kolombody++, $data->harga_penjualan);
            xlsWriteLabel($tablebody, $kolombody++, $data->min_stock);
            xlsWriteLabel($tablebody, $kolombody++, $data->created_at);

            $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }
}
