<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Penjualan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Penjualan_model');
        $this->load->model('User_model');
        $this->load->library('form_validation');        
	$this->load->library('datatables');
    }

    public function index()
    {
        $this->load->view('penjualan/penjualan-detail');
    } 

    public function penjualan()
    {
        $this->load->view('penjualan/penjualan');
    }

    public function data_product(){

    }

    public function penjualan_byrange_json($dateStart, $dateEnd){
        
        $dataPenjualan = $this->Penjualan_model->penjualan_range($dateStart, $dateEnd)->get()->result();
        
        $data['draw'] = 0;
        $data['recordsTotal'] = $dataPenjualan == null ? [] : count($dataPenjualan);
        $data['recordsFiltered'] = $dataPenjualan == null ? [] : count($dataPenjualan);
        $data['data'] = $dataPenjualan == null ? [] : $dataPenjualan;
        // var_dump($stockmup);
        echo json_encode($data);
    }

    // public function print_kontrol_stock_byrange($dateStart, $dateEnd) // CETAK LAPORAN KONTROL STOCK BY RANGE
    // {

    //     $dataPenjualan = $this->Penjualan_model->penjualan_range($dateStart, $dateEnd)->get()->result();
    //     $data_session = $this->session->userdata;
    //     $data_user = $this->User_model->get_by_id($data_session['id']);

    //     $date = ['dateStart' => $dateStart,'dateEnd' => $dateEnd,];

    //     if ($dataPenjualan == null) {
    //         // $this->session->set_flashdata('message', 'Record Not Found');
    //         // redirect(site_url('kasir/transaksi'));
    //     } else {
    //         $this->load->library('pdf');

    //         $this->pdf->setPaper('A4', 'potrait');
    //         $this->pdf->set_option('isRemoteEnabled', TRUE);
    //         $this->pdf->filename = "print_penjualan-".$dateStart."-".$dateEnd.".pdf";
    //         $this->pdf->load_view('laporan/laporan_transaksi',['data'=>$dataPenjualan,'date'=>$date,'data_user'=>$data_user]);
    //     }
    // }

    public function print_kontrol_stock_byrange($dateStart, $dateEnd) // CETAK LAPORAN KONTROL STOCK BY RANGE
    {

        $dataPenjualan = $this->Penjualan_model->penjualan_ranges($dateStart, $dateEnd);
        $data_session = $this->session->userdata;
        $data_user = $this->User_model->get_by_id($data_session['id']);

        $date = ['dateStart' => $dateStart,'dateEnd' => $dateEnd,];

        if ($dataPenjualan == null) {
            // $this->session->set_flashdata('message', 'Record Not Found');
            // redirect(site_url('kasir/transaksi'));
        } else {
            $this->load->library('pdf');

            $this->pdf->setPaper('A4', 'potrait');
            $this->pdf->set_option('isRemoteEnabled', TRUE);
            $this->pdf->filename = "print_penjualan-".$dateStart."-".$dateEnd.".pdf";
            $this->pdf->load_view('laporan/laporan_transaksis',['data'=>$dataPenjualan,'date'=>$date,'data_user'=>$data_user]);
        }
    }

    
    public function data_penjualan(){
        $a = 55;
        $data_penjualan = [];

        for ($i=0; $i < $a; $i++) { 

            $data_penjualan[]= 
        
            [
                "id" => $i,
                "image" => "assets/uploads/FROYO1.jpg",
                "nama" => "Kebaya A ".$i,
                "harga" => "10000".$i,
                "note" => "Bahan: Broklat A".$i
            
            ];
            
        }

        $data = [
            'totalItems' => 55,
            'items' => $data_penjualan
           
        ];
        echo json_encode($data);
        
    }
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->Penjualan_model->json();
    }

    public function read($id) 
    {
        $row = $this->Penjualan_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'kode' => $row->kode,
		'user' => $row->user,
		'total_penjualan' => $row->total_penjualan,
		'pelanggan' => $row->pelanggan,
		'created_at' => $row->created_at,
	    );
            $this->load->view('penjualan/penjualan_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('penjualan'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('penjualan/create_action'),
	    'id' => set_value('id'),
	    'kode' => set_value('kode'),
	    'user' => set_value('user'),
	    'total_penjualan' => set_value('total_penjualan'),
	    'pelanggan' => set_value('pelanggan'),
	    'created_at' => set_value('created_at'),
	);
        $this->load->view('penjualan/penjualan_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'kode' => $this->input->post('kode',TRUE),
		'user' => $this->input->post('user',TRUE),
		'total_penjualan' => $this->input->post('total_penjualan',TRUE),
		'pelanggan' => $this->input->post('pelanggan',TRUE),
		'created_at' => $this->input->post('created_at',TRUE),
	    );

            $this->Penjualan_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('penjualan'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Penjualan_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('penjualan/update_action'),
		'id' => set_value('id', $row->id),
		'kode' => set_value('kode', $row->kode),
		'user' => set_value('user', $row->user),
		'total_penjualan' => set_value('total_penjualan', $row->total_penjualan),
		'pelanggan' => set_value('pelanggan', $row->pelanggan),
		'created_at' => set_value('created_at', $row->created_at),
	    );
            $this->load->view('penjualan/penjualan_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('penjualan'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'kode' => $this->input->post('kode',TRUE),
		'user' => $this->input->post('user',TRUE),
		'total_penjualan' => $this->input->post('total_penjualan',TRUE),
		'pelanggan' => $this->input->post('pelanggan',TRUE),
		'created_at' => $this->input->post('created_at',TRUE),
	    );

            $this->Penjualan_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('penjualan'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Penjualan_model->get_by_id($id);

        if ($row) {
            $this->Penjualan_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('penjualan'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('penjualan'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('kode', 'kode', 'trim|required');
	$this->form_validation->set_rules('user', 'user', 'trim|required');
	$this->form_validation->set_rules('total_penjualan', 'total penjualan', 'trim|required');
	$this->form_validation->set_rules('pelanggan', 'pelanggan', 'trim|required');
	$this->form_validation->set_rules('created_at', 'created at', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "penjualan.xls";
        $judul = "penjualan";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Kode");
	xlsWriteLabel($tablehead, $kolomhead++, "User");
	xlsWriteLabel($tablehead, $kolomhead++, "Total Penjualan");
	xlsWriteLabel($tablehead, $kolomhead++, "Pelanggan");
	xlsWriteLabel($tablehead, $kolomhead++, "Created At");

	foreach ($this->Penjualan_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->kode);
	    xlsWriteNumber($tablebody, $kolombody++, $data->user);
	    xlsWriteLabel($tablebody, $kolombody++, $data->total_penjualan);
	    xlsWriteLabel($tablebody, $kolombody++, $data->pelanggan);
	    xlsWriteLabel($tablebody, $kolombody++, $data->created_at);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

}

/* End of file Penjualan.php */
/* Location: ./application/controllers/Penjualan.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-03-21 14:22:20 */
/* http://harviacode.com */