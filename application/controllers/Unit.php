<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Unit extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Unit_model');
        $this->load->library('form_validation');
        $this->load->library('datatables');

        $data_session = $this->session->userdata;

        if ((!$this->session->userdata('logged_in')) || $data_session['level'] != 1 && $data_session['level'] != 3) {
            redirect('auth'); // Cek udah login apa belum, kalo belum login dulu
        }
    }

    public function index()
    {
        $data['main_content'] = 'unit/main';
        $data['page_title'] = 'Halaman Unit';
        $this->load->view('template', $data);
    }

    public function json()
	{
		header('Content-Type: application/json');
		$Unit =  $this->Unit_model->json();

		$data['draw'] = 0;
        $data['recordsTotal'] = $Unit == null ? [] : count($Unit);
        $data['recordsFiltered'] = $Unit == null ? [] : count($Unit);
        $data['data'] = $Unit == null ? [] : $Unit;
		
        echo json_encode($data);
	}

    public function read($id)
    {
        $row = $this->Unit_model->get_by_id($id);
        if ($row) {
            $data = array(
                'id' => $row->id,
                'nama' => $row->nama,
            );
            $this->load->view('unit/unit_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('unit'));
        }
    }

    public function create()
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('unit/create_action'),
            'id' => set_value('id'),
            'nama' => set_value('nama'),
        );
        $this->load->view('unit/unit_form', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {

            $this->session->set_flashdata('pesan', 'Gagal Di Tambahkan');
            redirect(site_url('unit'));

        } else {
            $data = array(
                'nama' => $this->input->post('nama', TRUE),
            );
            $this->Unit_model->insert($data);
            $this->session->set_flashdata('pesan', 'Data Sukses Di Tambahkan');
            redirect(site_url('master/unit'));
        }
    }

    public function edit($id)
    {
        $row = $this->Unit_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('unit/update_action/'.$row->id),
                'id' => set_value('id', $row->id),
                'nama' => set_value('nama', $row->nama),
                'main_content' => 'unit/update',
                'page_title' => 'Edit Unit'
            );
            $this->load->view('template', $data);
        } else {
            $this->session->set_flashdata('pesan', 'Data Tidak DItemukan');
            redirect(site_url('master/unit'));
        }
    }

    public function update_action($id)
    {
        $category = $this->Unit_model->get_by_id($id);
		$is_unique_name = $this->input->post('nama', TRUE) != $category->nama ? '|is_unique[unit.nama]' : '';

		$this->form_validation->set_rules('nama', 'Nama', 'required'.$is_unique_name);//|edit_unique[barang.nama.' . $id . ']


        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('pesan', 'Data Gagal Di Ubah');
            redirect(site_url('master/unit'));
        } else {
            $data = array(
                'nama' => $this->input->post('nama', TRUE),
            );

            $this->Unit_model->update($id, $data);
            $this->session->set_flashdata('pesan', 'Data Sukses Di Ubah');
            redirect(site_url('master/unit'));
        }
    }

    public function delete($id)
    {
        $row = $this->Unit_model->get_by_id($id);

        if ($row) {
            $this->Unit_model->delete($id);
            $this->session->set_flashdata('pesan', 'Data Berhasil Di Hapus');
            redirect(site_url('master/unit'));
        } else {
            $this->session->set_flashdata('pesan', 'Data Tidak Ditemukan');
            redirect(site_url('master/unit'));
        }
    }

    public function _rules()
    {
        $this->form_validation->set_rules('nama', 'nama', 'trim|required|is_unique[unit.nama]');

        $this->form_validation->set_rules('id', 'id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "unit.xls";
        $judul = "unit";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
        xlsWriteLabel($tablehead, $kolomhead++, "Nama");

        foreach ($this->Unit_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
            xlsWriteLabel($tablebody, $kolombody++, $data->nama);

            $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }
}

/* End of file Unit.php */
/* Location: ./application/controllers/Unit.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-03-21 14:22:23 */
/* http://harviacode.com */
