<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Barang_masuk extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $data_session = $this->session->userdata;

        if ((!$this->session->userdata('logged_in')) || $data_session['level'] != 1 && $data_session['level'] != 3) {
            redirect('auth'); // Cek udah login apa belum, kalo belum login dulu
        }

        $this->load->model('Barang_masuk_model');
        $this->load->model('Supplier_model');
        $this->load->model('User_model');
        $this->load->model('Stock_model');
        $this->load->model('Barang_model');
        $this->load->model('Refund_model');

        $this->load->library('form_validation');
        $this->load->library('datatables');
    }

    public function index()
    {
        $data['main_content'] = 'pembelian/main';
        $data['page_title'] = 'Halaman Produk Masuk';


        //$data['code_barang'] = "TR".mt_rand(100000,999999);
        $data['produk'] = $this->Barang_model->get_all();
        $data['supplier'] = $this->Supplier_model->get_all();
        $this->load->view('template', $data);
    }

    public function json()
    {
        header('Content-Type: application/json');
        echo $this->Barang_masuk_model->json();
    }
    public function stockin_byrange_json($dateStart,$dateEnd){

        $stockInData = $this->Barang_masuk_model->stockin_range($dateStart,$dateEnd)->get()->result();
        
        if ($stockInData == null) {
            $this->session->set_flashdata('message', 'Record Not Found');
            
            $data['draw'] = 0;
            $data['recordsTotal'] = 0;
            $data['recordsFiltered'] = 0;
            $data['data'] =[];
           
            echo json_encode($data);
        } else {

            $data['draw'] = 0;
            $data['recordsTotal'] = $stockInData == null ? [] : count($stockInData);
            $data['recordsFiltered'] = $stockInData == null ? [] : count($stockInData);
            $data['data'] = $stockInData == null ? [] : $stockInData;
           
            echo json_encode($data);
        }
    }

    public function print_stockin_byrange($dateStart,$dateEnd){
        $data_session = $this->session->userdata;
        $data_user = $this->User_model->get_by_id($data_session['id']);
        $stockInData = $this->Barang_masuk_model->stockin_range($dateStart,$dateEnd)->get()->result();

        $date = ['dateStart' => $dateStart,'dateEnd' => $dateEnd];
        if ($stockInData == null) {
            $this->session->set_flashdata('message', 'Record Not Found');
            
            redirect(site_url('pembelian/masuk')); 
        } else {
            $this->load->library('pdf');

            $this->pdf->setPaper('A4', 'potrait');
            $this->pdf->set_option('isRemoteEnabled', TRUE);
            $this->pdf->filename = "print_stock_masuk-".$dateStart."-".$dateEnd.".pdf";
            $this->pdf->load_view('laporan/laporan_produk_masuk',['data'=>$stockInData,'date'=>$date,'data_user'=>$data_user]);
        }

       
    }
    public function read($id)
    {
        $row = $this->Barang_masuk_model->get_by_id($id);
        if ($row) {
            $data = array(
                'id' => $row->id,
                'kode' => $row->kode,
                'barang' => $row->barang,
                'supplier' => $row->supplier,
                'harga_pembelian' => $row->harga_pembelian,
                'total_pembelian' => $row->total_pembelian,
                'status' => $row->status,
                'jumlah' => $row->jumlah,
                'created_at' => $row->created_at,
            );
            $this->load->view('barang_masuk/barang_masuk_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('barang_masuk'));
        }
    }

    public function create()
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('barang_masuk/create_action'),
            'id' => set_value('id'),
            'kode' => set_value('kode'),
            'barang' => set_value('barang'),
            'supplier' => set_value('supplier'),
            'harga_pembelian' => set_value('harga_pembelian'),
            'total_pembelian' => set_value('total_pembelian'),
            'status' => set_value('status'),
            'jumlah' => set_value('jumlah'),
            'created_at' => set_value('created_at'),
        );
        $this->load->view('barang_masuk/barang_masuk_form', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('pesan', 'Data Gagal Disimpan');
            redirect(site_url('pembelian/masuk'));
        } else {
            $data = array(
                'kode' => "TR" . mt_rand(100000, 999999),
                'barang' => $this->input->post('barang', TRUE),
                'supplier' => $this->input->post('supplier', TRUE),
                'harga_pembelian' => $this->input->post('harga_pembelian', TRUE),
                'total_pembelian' => 0,
                'status' => 0,
                'jumlah' => $this->input->post('jumlah', TRUE),
                'created_at' => $this->input->post('created_at', TRUE),
            );


            $this->Barang_masuk_model->insert($data);
            $this->session->set_flashdata('pesan', 'Create Record Success');
            redirect(site_url('pembelian/masuk'));
        }
    }

    public function edit($id)
    {
        $row = $this->Barang_masuk_model->get_by_id($id);

        if ($row->status == 0) {
            $data = array(
                'id' => set_value('id', $row->id),
                'barang' => set_value('barang', $row->barang),
                'supplier_id' => set_value('supplier', $row->supplier),
                'harga_pembelian' => set_value('harga_pembelian', $row->harga_pembelian),
                'jumlah' => set_value('jumlah', $row->jumlah),
                'produk' => $this->Barang_model->get_all(),
                'supplier' => $this->Supplier_model->get_all(),
                'created_at' => set_value('created_at', $row->created_at),
                'main_content' => 'pembelian/update',
                'page_title' => 'Edit Pembelian',
            );
            $this->load->view('template', $data);
        } else {
            $this->session->set_flashdata('pesan', 'Data Sudah Tidak Dapat Dimodifikasi');
            redirect(site_url('pembelian/masuk'));
        }
    }

    public function update_action($id)
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('pesan', 'Data Gagal Di Ubah');
            redirect(site_url('pembelian/masuk'));
        } else {
            $row = $this->Barang_masuk_model->get_data_relational_byid($id);

            if ($row) {

                if ($row->status == 0) {
                    $data = array(
                        'barang' => $this->input->post('barang', TRUE),
                        'supplier' => $this->input->post('supplier', TRUE),
                        'harga_pembelian' => $this->input->post('harga_pembelian', TRUE),
                        'jumlah' => $this->input->post('jumlah', TRUE),
                        'created_at' => $this->input->post('created_at', TRUE),
                    );

                    $this->Barang_masuk_model->update($id, $data);
                    $this->session->set_flashdata('pesan', 'Update Record Success');
                    redirect(site_url('pembelian/masuk'));
                } else {
                    $this->session->set_flashdata('pesan', 'Maaf Data Sudah Tidak Bisa Di Ubah');
                    redirect(site_url('pembelian/masuk'));
                }
            } else {
                $this->session->set_flashdata('pesan', 'Data Tidak Ditemukan');
                redirect(site_url('pembelian/masuk'));
            }
        }
    }

    public function generate_qr($kode)
    {

        $kode_barang = $kode;

        $this->load->library('ciqrcode'); //pemanggilan library QR CODE

        $config['cacheable']    = true; //boolean, the default is true
        $config['cachedir']     = './assets/'; //string, the default is application/cache/
        $config['errorlog']     = './assets/'; //string, the default is application/logs/
        $config['imagedir']     = './assets/uploads/transaction/'; //direktori penyimpanan qr code
        $config['quality']      = true; //boolean, the default is true
        $config['size']         = '1024'; //interger, the default is 1024
        $config['black']        = array(224, 255, 255); // array, default is array(255,255,255)
        $config['white']        = array(70, 130, 180); // array, default is array(0,0,0)
        $this->ciqrcode->initialize($config);

        $image_name = $kode_barang . '.png'; //buat name dari qr code sesuai dengan nim

        $params['data'] = $kode_barang; //data yang akan di jadikan QR CODE
        $params['level'] = 'H'; //H=High
        $params['size'] = 10;
        $params['savename'] = FCPATH . $config['imagedir'] . $image_name; //simpan image QR CODE ke folder assets/images/
        $this->ciqrcode->generate($params); // fungsi untuk generate QR CODE


    }

    public function print_retur($id)
    {


        $refund_data = $this->Refund_model->get_data_relational_byid($id)->get()->row();

        if ($refund_data) {
            $data = [
                'image' => 'assets/uploads/transaction/' . $refund_data->kode . '.png',
                'image_logo' => 'assets/Logo.png',
                'data_refund' => $refund_data
                // 'price'=>'Rp.'.number_format($produk->harga_penjualan,2),
                // 'produk_name'=>$produk->nama,
                // 'company'=>'Maharany Skar Solo'
            ];

            $this->load->library('pdf');

            $this->pdf->setPaper('A4', 'potrait');
            $this->pdf->set_option('isRemoteEnabled', TRUE);
            $this->pdf->filename = "print_retur.pdf";
            $this->pdf->load_view('produk/print_retur', $data);
        } else {
            $this->session->set_flashdata('pesan', 'Data Tidak DItemukan');
            redirect(site_url('pembelian/masuk'));
        }
    }

    public function print_faktur($id)
    {


        $stock_data = $this->Stock_model->get_data_relational_byid($id)->get()->row();

        if ($stock_data) {
            $data = [
                'image' => 'assets/uploads/transaction/' . $stock_data->kode . '.png',
                'image_logo' => 'assets/Logo.png',
                'data_stock' => $stock_data
                // 'price'=>'Rp.'.number_format($produk->harga_penjualan,2),
                // 'produk_name'=>$produk->nama,
                // 'company'=>'Maharany Skar Solo'
            ];

            $this->load->library('pdf');

            $this->pdf->setPaper('A4', 'potrait');
            $this->pdf->set_option('isRemoteEnabled', TRUE);
            $this->pdf->filename = "print_faktur.pdf";
            $this->pdf->load_view('produk/print_faktur', $data);
        } else {
            $this->session->set_flashdata('pesan', 'Data Tidak DItemukan');
            redirect(site_url('pembelian/masuk'));
        }
    }

    public function refund($id)
    {
        $row = $this->Barang_masuk_model->get_data_relational_byid($id);
        $refund_data = $this->Refund_model->get_data_relational_byid($id)->get()->row();
        $stock_data = $this->Stock_model->get_data_relational_byid($id)->get()->row();

        $product_data = $this->Barang_model->get_by_id($row->barang);

        if ($refund_data != null) {
            $this->session->set_flashdata('pesan', 'Data Tidak Dapat Di Modifikasi');
            redirect(site_url('pembelian/masuk'));
        }

      
        $jumlah_refund = $refund_data == null ? 0 : floatval($refund_data->jumlah);
        $jumlah_stock = $stock_data == null ? 0 : floatval($stock_data->amount);

        if ($jumlah_refund == 0 && $jumlah_stock == 0) {
            $unlocated_stock = $row->jumlah;
        } else {
            $unlocated_stock = floatval($row->jumlah) - (floatval($jumlah_stock) + floatval($jumlah_refund));
        }

        // var_dump($refund_data);
        $generate_code = "R" . $row->kode . "-" . date('ddmmyy');


        if ($row) {
            $data = array(
                'id' => set_value('id', $row->id),
                'kode' => set_value('id', $generate_code),
                'barang_id' => set_value('barang', $row->barang),
                'barang' => set_value('barang', $row->barang_name),
                'supplier_id' => set_value('supplier', $row->supplier_name),
                'harga_pembelian' => set_value('harga_pembelian', $row->harga_pembelian),
                'jumlah' => set_value('jumlah', $row->jumlah),
                'note' => set_value('note', $product_data->note),
                'refund' => $refund_data == null ? 0 : floatval($refund_data->jumlah),
                'stock' => $stock_data == null ? 0 : floatval($stock_data->amount),
                'unlocated_stock' => $unlocated_stock,
                'product_data' => $product_data,
                'stcok_masuk_data' => $row,
                'main_content' => 'pembelian/refund',
                'page_title' => 'Pencatatan Refund',
            );
            $this->load->view('template', $data);
        } else {
            $this->session->set_flashdata('pesan', 'Data Tidak DItemukan');
            redirect(site_url('pembelian/masuk'));
        }
    }

    public function refund_action($id)
    {
        $row = $this->Barang_masuk_model->get_data_relational_byid($id);
        $refund_data = $this->Refund_model->get_data_relational_byid($id)->get()->row();

        if ($row) { // CEK APA ADA DATA BARANG MASUK DI ID TERSEBUT

            $jumlah_stok_masuk = $this->input->post('jumlah', TRUE);

            $unlocated_stock = $this->input->post('unlocated_stock', TRUE);

            if ($jumlah_stok_masuk > $unlocated_stock) { // Validasi STOCK YANG BISA MASUUK
                $this->session->set_flashdata('pesan', 'Stock Barang Masuk Tidak Mencukupi');
                redirect(site_url('pembelian/masuk/refund/' . $id));
            } else {

                $this->generate_qr($this->input->post('kode', TRUE)); // Generate QR RETUR

                $data_refund = array(
                    'kode' => $this->input->post('kode', TRUE),
                    'barang_masuk' => $id,
                    'user' => 1, // ADMIN GUDANG
                    'supplier' => $row->supplier,
                    'harga_pembelian' => $row->harga_pembelian,
                    'jumlah' => $this->input->post('jumlah', TRUE),
                    'note' => $this->input->post('note', TRUE),
                    'created_at' => $this->input->post('created_at', TRUE),
                );

                //AMBIL DATA PRODUK 

                $stock_produk = $this->db->where('id', $row->barang)->get('barang')->row()->stock;



                if ($row->status == 1) {
                    $data_produk = array(

                        'stock' => floatval($stock_produk) - floatval($this->input->post('jumlah', TRUE)),
                        //'note' => $row->supplier,

                    );
                    $this->Barang_model->update($row->barang, $data_produk);
                }

                $produk_masuk = array(

                    'status' => 2

                );

                $this->Refund_model->insert($data_refund);
                $this->Barang_masuk_model->update($id, $produk_masuk);
                $this->session->set_flashdata('pesan', 'Produk Sukses Di Refund');
                sleep(3);
                redirect(site_url('pembelian/masuk'));
                //redirect(site_url('pembelian/masuk/refund/print/'.$id));
            }
        } else {
            $this->session->set_flashdata('pesan', 'Data Tidak DItemukan');
            redirect(site_url('pembelian/masuk'));
        }
    }

    public function stock($id)
    {
        $row = $this->Barang_masuk_model->get_data_relational_byid($id);
        $refund_data = $this->Refund_model->get_data_relational_byid($id)->get()->row();
        $stock_data = $this->Stock_model->get_data_relational_byid($id)->get()->row();
        $product_data = $this->Barang_model->get_by_id($row->barang);

        if ($stock_data != null) {
            $this->session->set_flashdata('pesan', 'Data Tidak Dapat Di Modifikasi');
            redirect(site_url('pembelian/masuk'));
        }

        $jumlah_refund = $refund_data == null ? 0 : floatval($refund_data->jumlah);
        $jumlah_stock = $stock_data == null ? 0 : floatval($stock_data->amount);

        if ($jumlah_refund == 0 && $jumlah_stock == 0) {
            $unlocated_stock = $row->jumlah;
        } else {
            $unlocated_stock = floatval($row->jumlah) - (floatval($jumlah_stock) + floatval($jumlah_refund));
        }

        // var_dump($refund_data);
        $generate_code = "S" . $row->kode . "-" . date('ddmmyy');
        if ($row) {
            $data = array(
                'id' => set_value('id', $row->id),
                'kode' => set_value('id', $generate_code),
                'barang_id' => set_value('barang', $row->barang),
                'barang' => set_value('barang', $row->barang_name),
                'supplier_id' => set_value('supplier', $row->supplier_name),
                'harga_pembelian' => set_value('harga_pembelian', $row->harga_pembelian),
                'jumlah' => set_value('jumlah', $row->jumlah),
                'note' => set_value('note', $product_data->note),
                'refund' => $refund_data == null ? 0 : floatval($refund_data->jumlah),
                'unlocated_stock' => $unlocated_stock,
                'product_data' => $product_data,
                'stcok_masuk_data' => $row,
                'main_content' => 'pembelian/stock',
                'page_title' => 'Pencatatan Stock Masuk',
            );
            $this->load->view('template', $data);
        } else {
            $this->session->set_flashdata('pesan', 'Data Tidak DItemukan');
            redirect(site_url('pembelian/masuk'));
        }
    }


    public function stock_action($id)
    {
        $row = $this->Barang_masuk_model->get_data_relational_byid($id);
        $refund_data = $this->Refund_model->get_data_relational_byid($id)->get()->row();

        if ($row) { // CEK APA ADA DATA BARANG MASUK DI ID TERSEBUT


            $jumlah_stok_masuk = $this->input->post('jumlah', TRUE);

            $unlocated_stock = $this->input->post('unlocated_stock', TRUE);

            if ($jumlah_stok_masuk > $unlocated_stock) { // Validasi STOCK YANG BISA MASUUK
                $this->session->set_flashdata('pesan', 'Stock Barang Masuk Tidak Mencukupi');
                redirect(site_url('pembelian/masuk/stock/' . $id));
            } else {
                $this->generate_qr($this->input->post('kode', TRUE)); // Generate QR FAKTUR
                $data_stock = array(
                    'kode' => $this->input->post('kode', TRUE),
                    'barang_masuk' => $id,
                    'barang' => $row->barang,
                    'user' => 1, // ADMIN GUDANG
                    'supplier' => $row->supplier,
                    'harga_pembelian' => $row->harga_pembelian,
                    'harga_penjualan' => $this->input->post('harga_jual', TRUE),
                    'amount' => $this->input->post('jumlah', TRUE),
                    'balance' => $this->input->post('jumlah', TRUE),
                    'note' => "Kondisi Ok",
                    'total_pembelian' => $row->harga_pembelian * $this->input->post('jumlah', TRUE),
                    //'note' => $row->supplier,
                    'created_at' => $this->input->post('created_at', TRUE),
                );

                // AMBIL DATA PRODUK 

                $stock_produk = $this->db->where('id', $row->barang)->get('barang')->row()->stock;

                $produk_masuk = array(

                    'status' => $refund_data == null ? 1 : 2 // Validate jika ada refund 2 jika tidak ada 1

                );

                $data_produk = array(

                    'harga_penjualan' => $this->input->post('harga_jual', TRUE),
                    'harga_grosir' => $this->input->post('harga_grosir', TRUE),
                    'stock' => floatval($stock_produk) + floatval($this->input->post('jumlah', TRUE)),
                    //'note' => $row->supplier,

                );

                $this->Stock_model->insert($data_stock);
                $this->Barang_masuk_model->update($id, $produk_masuk);
                $this->Barang_model->update($row->barang, $data_produk);

                if ($this->input->post('jumlah', TRUE) == $row->jumlah) {
                   
                    $this->Barang_masuk_model->update($id, ['status' => 1]);
                    $this->session->set_flashdata('pesan', 'Produk Sukses Masuk Ke Stock');
                    sleep(3);
                    redirect(site_url('pembelian/masuk'));

                }else{
                    $this->Barang_masuk_model->update($id, ['status' => 2]);
                    $this->session->set_flashdata('pesan', 'Refund Product');
                    sleep(3);
                    redirect(site_url('pembelian/masuk/refund/'.$id));
                }
               
            }
        } else {
            $this->session->set_flashdata('pesan', 'Data Tidak DItemukan');
            redirect(site_url('pembelian/masuk'));
        }
    }

    public function delete($id)
    {
        $row = $this->Barang_masuk_model->get_by_id($id);

        if ($row) {
            if ($row->status == 0) {
                $this->Barang_masuk_model->delete($id);
                $this->session->set_flashdata('pesan', 'Delete Record Success');
                redirect(site_url('pembelian/masuk'));
            } else {
                $this->session->set_flashdata('pesan', 'Sudah Dikonfirmasi, Data Tidak Dapat Di Hapus');
                redirect(site_url('pembelian/masuk'));

            }
        } else {
            $this->session->set_flashdata('pesan', 'Record Not Found');
            redirect(site_url('pembelian/masuk'));
        }
    }

    public function _rules()
    {
        $this->form_validation->set_rules('kode', 'kode', 'trim');
        $this->form_validation->set_rules('barang', 'barang', 'trim|required');
        $this->form_validation->set_rules('supplier', 'supplier', 'trim|required');
        $this->form_validation->set_rules('harga_pembelian', 'harga pembelian', 'trim|required');
        $this->form_validation->set_rules('total_pembelian', 'total pembelian', 'trim');
        $this->form_validation->set_rules('status', 'status', 'trim');
        $this->form_validation->set_rules('jumlah', 'jumlah', 'trim|required');
        $this->form_validation->set_rules('created_at', 'created at', 'trim');

        $this->form_validation->set_rules('id', 'id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "barang_masuk.xls";
        $judul = "barang_masuk";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
        xlsWriteLabel($tablehead, $kolomhead++, "Kode");
        xlsWriteLabel($tablehead, $kolomhead++, "Barang");
        xlsWriteLabel($tablehead, $kolomhead++, "Supplier");
        xlsWriteLabel($tablehead, $kolomhead++, "Harga Pembelian");
        xlsWriteLabel($tablehead, $kolomhead++, "Total Pembelian");
        xlsWriteLabel($tablehead, $kolomhead++, "Status");
        xlsWriteLabel($tablehead, $kolomhead++, "Jumlah");
        xlsWriteLabel($tablehead, $kolomhead++, "Created At");

        foreach ($this->Barang_masuk_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
            xlsWriteLabel($tablebody, $kolombody++, $data->kode);
            xlsWriteNumber($tablebody, $kolombody++, $data->barang);
            xlsWriteNumber($tablebody, $kolombody++, $data->supplier);
            xlsWriteLabel($tablebody, $kolombody++, $data->harga_pembelian);
            xlsWriteLabel($tablebody, $kolombody++, $data->total_pembelian);
            xlsWriteNumber($tablebody, $kolombody++, $data->status);
            xlsWriteLabel($tablebody, $kolombody++, $data->jumlah);
            xlsWriteLabel($tablebody, $kolombody++, $data->created_at);

            $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }
}

/* End of file Barang_masuk.php */
/* Location: ./application/controllers/Barang_masuk.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-03-21 14:22:20 */
/* http://harviacode.com */
