<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Refund extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $data_session = $this->session->userdata;

        if ((!$this->session->userdata('logged_in')) || $data_session['level'] != 1 && $data_session['level'] != 3) {
            redirect('auth'); // Cek udah login apa belum, kalo belum login dulu
        }

        $this->load->model('User_model');
        $this->load->model('Barang_masuk_model');
        $this->load->model('Supplier_model');
        $this->load->model('Barang_model');
        $this->load->model('Refund_model');

        $this->load->model('Stock_model');
        $this->load->library('form_validation');
        $this->load->library('datatables');
    }

    public function index()
    {
        $data['main_content'] = 'refund/main';
        $data['page_title'] = 'Halaman Stock Produk';


        //$data['code_barang'] = "TR".mt_rand(100000,999999);
        $data['produk'] = $this->Barang_model->get_data_relational_all();
        $data['supplier'] = $this->Supplier_model->get_all();
        $this->load->view('template', $data);
    }

    public function refund_range_json($id,$dateStart, $dateEnd)
    {

        $refundData = $this->Refund_model->stockin_range($id,$dateStart,$dateEnd)->get()->result();

        $data['draw'] = 0;
        $data['recordsTotal'] = $refundData == null ? [] : count($refundData);
        $data['recordsFiltered'] = $refundData == null ? [] : count($refundData);
        $data['data'] = $refundData == null ? [] : $refundData;
        // var_dump($stockmup);
        echo json_encode($data);
    }

    public function detail($id)
    {
      
        $product_data = $this->Barang_model->get_data_product_byid($id)->get()->row();
      
        
       // var_dump($refund_data);
        if ($product_data) {
            $data = array(
        
                'id' => $id,
                'product_data' => $product_data,
               
                'main_content' => 'refund/refund',
                'page_title' => 'Daftar Refund Stock',
            );
            $this->load->view('template', $data);
        } else {
            $this->session->set_flashdata('pesan', 'Data Tidak DItemukan');
            redirect(site_url('pembelian/masuk'));
        }
    }


    public function json()
    {
        header('Content-Type: application/json');
        echo $this->Stock_model->json();
    }

    public function refund_byid_product($id)
    {

        header('Content-Type: application/json');
		$Refund =  $this->Refund_model->refund_byid_product($id);

		$data['draw'] = 0;
        $data['recordsTotal'] = $Refund == null ? [] : count($Refund);
        $data['recordsFiltered'] = $Refund == null ? [] : count($Refund);
        $data['data'] = $Refund == null ? [] : $Refund;
		
        echo json_encode($data);
    }

    public function print_refund_id_byrange($id,$dateStart,$dateEnd){

        $refundData = $this->Refund_model->stockin_range($id,$dateStart,$dateEnd)->get()->result();
        $data_session = $this->session->userdata;
        $data_user = $this->User_model->get_by_id($data_session['id']);
       //var_dump($refundData);
        $date = ['dateStart' => $dateStart,'dateEnd' => $dateEnd];
        if ($refundData == null) {
            $this->session->set_flashdata('message', 'Record Not Found');
            
            redirect(site_url('pembelian/refund')); 
        } else {
            $productName = $this->Barang_model->get_by_id($id)->nama;
            $this->load->library('pdf');

            $this->pdf->setPaper('A4', 'potrait');
            $this->pdf->set_option('isRemoteEnabled', TRUE);
            $this->pdf->filename = "print_kartustock-".$productName."-".$dateStart."-".$dateEnd.".pdf";
            $this->pdf->load_view('laporan/laporan_refund_stock',['data'=>$refundData,'date'=>$date,'productName'=>$productName,'data_user'=>$data_user]);
        }

       
    }


    public function read($id)
    {
        $row = $this->Stock_model->get_by_id($id);
        if ($row) {
            $data = array(
                'id' => $row->id,
                'kode' => $row->kode,
                'barang' => $row->barang,
                'user' => $row->user,
                'supplier' => $row->supplier,
                'harga_pembelian' => $row->harga_pembelian,
                'harga_penjualan' => $row->harga_penjualan,
                'amount' => $row->amount,
                'balance' => $row->balance,
                'note' => $row->note,
                'created_at' => $row->created_at,
            );
            $this->load->view('stock/stock_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('stock'));
        }
    }

    public function create()
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('stock/create_action'),
            'id' => set_value('id'),
            'kode' => set_value('kode'),
            'barang' => set_value('barang'),
            'user' => set_value('user'),
            'supplier' => set_value('supplier'),
            'harga_pembelian' => set_value('harga_pembelian'),
            'harga_penjualan' => set_value('harga_penjualan'),
            'amount' => set_value('amount'),
            'balance' => set_value('balance'),
            'note' => set_value('note'),
            'created_at' => set_value('created_at'),
        );
        $this->load->view('stock/stock_form', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
                'kode' => $this->input->post('kode', TRUE),
                'barang' => $this->input->post('barang', TRUE),
                'user' => $this->input->post('user', TRUE),
                'supplier' => $this->input->post('supplier', TRUE),
                'harga_pembelian' => $this->input->post('harga_pembelian', TRUE),
                'harga_penjualan' => $this->input->post('harga_penjualan', TRUE),
                'amount' => $this->input->post('amount', TRUE),
                'balance' => $this->input->post('balance', TRUE),
                'note' => $this->input->post('note', TRUE),
                'created_at' => $this->input->post('created_at', TRUE),
            );

            $this->Stock_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('stock'));
        }
    }

    public function update($id)
    {
        $row = $this->Stock_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('stock/update_action'),
                'id' => set_value('id', $row->id),
                'kode' => set_value('kode', $row->kode),
                'barang' => set_value('barang', $row->barang),
                'user' => set_value('user', $row->user),
                'supplier' => set_value('supplier', $row->supplier),
                'harga_pembelian' => set_value('harga_pembelian', $row->harga_pembelian),
                'harga_penjualan' => set_value('harga_penjualan', $row->harga_penjualan),
                'amount' => set_value('amount', $row->amount),
                'balance' => set_value('balance', $row->balance),
                'note' => set_value('note', $row->note),
                'created_at' => set_value('created_at', $row->created_at),
            );
            $this->load->view('stock/stock_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('stock'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
                'kode' => $this->input->post('kode', TRUE),
                'barang' => $this->input->post('barang', TRUE),
                'user' => $this->input->post('user', TRUE),
                'supplier' => $this->input->post('supplier', TRUE),
                'harga_pembelian' => $this->input->post('harga_pembelian', TRUE),
                'harga_penjualan' => $this->input->post('harga_penjualan', TRUE),
                'amount' => $this->input->post('amount', TRUE),
                'balance' => $this->input->post('balance', TRUE),
                'note' => $this->input->post('note', TRUE),
                'created_at' => $this->input->post('created_at', TRUE),
            );

            $this->Stock_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('stock'));
        }
    }

    public function delete($id)
    {
        $row = $this->Stock_model->get_by_id($id);

        if ($row) {
            $this->Stock_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('stock'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('stock'));
        }
    }

    public function _rules()
    {
        $this->form_validation->set_rules('kode', 'kode', 'trim|required');
        $this->form_validation->set_rules('barang', 'barang', 'trim|required');
        $this->form_validation->set_rules('user', 'user', 'trim|required');
        $this->form_validation->set_rules('supplier', 'supplier', 'trim|required');
        $this->form_validation->set_rules('harga_pembelian', 'harga pembelian', 'trim|required');
        $this->form_validation->set_rules('harga_penjualan', 'harga penjualan', 'trim|required');
        $this->form_validation->set_rules('amount', 'amount', 'trim|required');
        $this->form_validation->set_rules('balance', 'balance', 'trim|required');
        $this->form_validation->set_rules('note', 'note', 'trim|required');
        $this->form_validation->set_rules('created_at', 'created at', 'trim|required');

        $this->form_validation->set_rules('id', 'id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "stock.xls";
        $judul = "stock";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
        xlsWriteLabel($tablehead, $kolomhead++, "Kode");
        xlsWriteLabel($tablehead, $kolomhead++, "Barang");
        xlsWriteLabel($tablehead, $kolomhead++, "User");
        xlsWriteLabel($tablehead, $kolomhead++, "Supplier");
        xlsWriteLabel($tablehead, $kolomhead++, "Harga Pembelian");
        xlsWriteLabel($tablehead, $kolomhead++, "Harga Penjualan");
        xlsWriteLabel($tablehead, $kolomhead++, "Amount");
        xlsWriteLabel($tablehead, $kolomhead++, "Balance");
        xlsWriteLabel($tablehead, $kolomhead++, "Note");
        xlsWriteLabel($tablehead, $kolomhead++, "Created At");

        foreach ($this->Stock_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
            xlsWriteLabel($tablebody, $kolombody++, $data->kode);
            xlsWriteNumber($tablebody, $kolombody++, $data->barang);
            xlsWriteNumber($tablebody, $kolombody++, $data->user);
            xlsWriteNumber($tablebody, $kolombody++, $data->supplier);
            xlsWriteLabel($tablebody, $kolombody++, $data->harga_pembelian);
            xlsWriteLabel($tablebody, $kolombody++, $data->harga_penjualan);
            xlsWriteNumber($tablebody, $kolombody++, $data->amount);
            xlsWriteNumber($tablebody, $kolombody++, $data->balance);
            xlsWriteLabel($tablebody, $kolombody++, $data->note);
            xlsWriteLabel($tablebody, $kolombody++, $data->created_at);

            $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }
}

/* End of file Stock.php */
/* Location: ./application/controllers/Stock.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-03-21 14:22:22 */
/* http://harviacode.com */
