<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $data_session = $this->session->userdata;

        if ((!$this->session->userdata('logged_in')) || $data_session['level'] != 1 && $data_session['level'] != 3) {
            redirect('auth'); // Cek udah login apa belum, kalo belum login dulu
        }

        $this->load->model('User_model');
        $this->load->model('Category_model');
        $this->load->model('Barang_model');
        $this->load->model('Supplier_model');
        $this->load->model('Penjualan_model');
        $this->load->model('Penjualan_detail_model');
        $this->load->model('Stock_model');


        $this->load->model('auth_model');
        $this->load->library('form_validation');
        $this->load->library('datatables');
    }

    public function stock_sisa($month, $idProduk)
    {
        $dataMutasiAll = $this->Stock_model->stock_sisa($month, $idProduk)->get()->result();

        $total = 0;
        $sisa_stock = 0;
        foreach ($dataMutasiAll as $keys) {

            if ($keys->status == 0) {

                $total += $keys->amount;
            } else {

                $total += $keys->balance;
            }

            $sisa_stock = $total;
        }

        return $sisa_stock;
    }
    public function print_kontrol_stock_byrange($dateStart, $dateEnd) // CETAK LAPORAN KONTROL STOCK BY RANGE
    {

        $allProduk = $this->Barang_model->get_data_product_all();
        $stockmup = [];
        foreach ($allProduk as $produk) {


            if ($this->Stock_model->sum_stockin_byrange($produk->id, $dateStart, $dateEnd) == null) {
                $produk->totalMasuk = 0 + $this->stock_sisa($dateStart, $produk->id);
            } else {
                $produk->totalMasuk = $this->Stock_model->sum_stockin_byrange($produk->id, $dateStart, $dateEnd)->total_masuk + $this->stock_sisa($dateStart, $produk->id);
            }

            if ($this->Stock_model->sum_stockout_byrange($produk->id, $dateStart, $dateEnd) == null) {
                $produk->totalKeluar = "0";
            } else {
                $produk->totalKeluar = $this->Stock_model->sum_stockout_byrange($produk->id, $dateStart, $dateEnd)->total_keluar;
            }

            if ($this->Stock_model->sum_stockavailable_byrange($produk->id, $dateStart, $dateEnd) == null) {
                $produk->totalAvailable = $produk->totalMasuk + $produk->totalKeluar;
            } else {
                $produk->totalAvailable = $produk->totalMasuk + $produk->totalKeluar;
                //$produk->totalAvailable = $this->Stock_model->sum_stockavailable_byrange($produk->id,$dateStart,$dateEnd)->total_available;
            }

            $stockmup[] = $produk;
        }

        $date = ['dateStart' => $dateStart, 'dateEnd' => $dateEnd,];
        $data_session = $this->session->userdata;
        $data_user = $this->User_model->get_by_id($data_session['id']);

        if ($stockmup == null) {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('barang'));
        } else {
            $this->load->library('pdf');

            $this->pdf->setPaper('A4', 'potrait');
            $this->pdf->set_option('isRemoteEnabled', TRUE);
            $this->pdf->filename = "print_kontrolstock-" . $dateStart . "-" . $dateEnd . ".pdf";
            $this->pdf->load_view('laporan/laporan_kontrol_stock', ['data' => $stockmup, 'date' => $date, 'data_user' => $data_user]);
        }
    }

    public function print_kartu_stock_byrange($idProduk, $dateStart, $dateEnd)
    {
        $data_session = $this->session->userdata;
        $data_user = $this->User_model->get_by_id($data_session['id']);

        $dataMutasiAll = $this->Stock_model->stock_mutatio_byproduk_all_range($dateStart, $dateEnd, $idProduk)->get()->result();
        
        $total = $this->stock_sisa($dateStart, $idProduk);
        $masuk = 0;
        $keluar = 0;
        $datamu = [];
        foreach ($dataMutasiAll as $keys) {

            if ($keys->status == 0) {
                $masuk = $keys->amount;
                $keluar = 0;
                $total += $keys->amount;
            } else {
                $masuk = 0;
                $keluar = $keys->amount;
                $total += $keys->balance;
            }

            $keys->masuk = $masuk;
            $keys->keluar = $keluar;
            $keys->sisa = $total;
            $datamu[] = $keys;
        }

        $date = ['dateStart' => $dateStart, 'dateEnd' => $dateEnd,];

        if ($datamu == null) {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('barang'));
        } else {
            $productName = $this->Barang_model->get_by_id($idProduk)->nama;
            $this->load->library('pdf');

            $this->pdf->setPaper('A4', 'potrait');
            $this->pdf->set_option('isRemoteEnabled', TRUE);
            $this->pdf->filename = "print_kartustock-" . $productName . "-" . $dateStart . "-" . $dateEnd . ".pdf";
            $this->pdf->load_view('laporan/laporan_kartu_stock', ['data' => $datamu, 'date' => $date, 'productName' => $productName, 'data_user' => $data_user]);
        }
    }


    public function index()
    {
        $pembelian_terakhir = $this->Stock_model->pembelian_terakhir();
        // var_dump($pembelian_terakhir);

        $data['main_content'] = 'admin/main';
        $data['page_title'] = 'Halaman Admin';
        $data['pembelian_terakhir'] = $pembelian_terakhir == null ? [] : $pembelian_terakhir;
        $this->load->view('template', $data);
    }

    public function kontrol_stock()
    {
        $data['page_title'] = 'Halaman Kontrol Stock';
        $data['main_content'] = 'stock/kontrolstock';

        $this->load->view('template', $data);
        //var_dump($transaksi_terakhir);
    }


    public function kontrol_stock_json($month)
    {

        $allProduk = $this->Barang_model->get_data_product_all();
        $stockmup = [];
        foreach ($allProduk as $produk) {


            if ($this->Stock_model->sum_stockin_bymonth($produk->id, $month) == null) {
                $produk->totalMasuk = "0";
            } else {
                $produk->totalMasuk = $this->Stock_model->sum_stockin_bymonth($produk->id, $month)->total_masuk;
            }

            if ($this->Stock_model->sum_stockout_bymonth($produk->id, $month) == null) {
                $produk->totalKeluar = "0";
            } else {
                $produk->totalKeluar = $this->Stock_model->sum_stockout_bymonth($produk->id, $month)->total_keluar;
            }

            if ($this->Stock_model->sum_stockavailable_bymonth($produk->id, $month) == null) {
                $produk->totalAvailable = "0";
            } else {
                $produk->totalAvailable = $this->Stock_model->sum_stockavailable_bymonth($produk->id, $month)->total_available;
            }

            $stockmup[] = $produk;
        }

        $data['draw'] = 0;
        $data['recordsTotal'] = $stockmup == null ? [] : count($stockmup);
        $data['recordsFiltered'] = $stockmup == null ? [] : count($stockmup);
        $data['data'] = $stockmup == null ? [] : $stockmup;
        // var_dump($stockmup);
        echo json_encode($data);
    }

    public function kontrol_stock_range_json($dateStart, $dateEnd)
    {
        $stockmup = [];
        $allProduk = $this->Barang_model->get_data_product_all();


        foreach ($allProduk as $produk) {


            if ($this->Stock_model->sum_stockin_byrange($produk->id, $dateStart, $dateEnd) == null) {
                $produk->totalMasuk = 0 + $this->stock_sisa($dateStart, $produk->id);
            } else {
                $produk->totalMasuk = $this->Stock_model->sum_stockin_byrange($produk->id, $dateStart, $dateEnd)->total_masuk + $this->stock_sisa($dateStart, $produk->id);
            }

            if ($this->Stock_model->sum_stockout_byrange($produk->id, $dateStart, $dateEnd) == null) {
                $produk->totalKeluar = "0";
            } else {
                $produk->totalKeluar = $this->Stock_model->sum_stockout_byrange($produk->id, $dateStart, $dateEnd)->total_keluar;
            }

            if ($this->Stock_model->sum_stockavailable_byrange($produk->id, $dateStart, $dateEnd) == null) {
                $produk->totalAvailable = $produk->totalMasuk + $produk->totalKeluar;
            } else {
                $produk->totalAvailable = $produk->totalMasuk + $produk->totalKeluar;
                //$produk->totalAvailable = $this->Stock_model->sum_stockavailable_byrange($produk->id,$dateStart,$dateEnd)->total_available;
            }

            $stockmup[] = $produk;
        }

        $data['draw'] = 0;
        $data['recordsTotal'] = $stockmup == null ? [] : count($stockmup);
        $data['recordsFiltered'] = $stockmup == null ? [] : count($stockmup);
        $data['data'] = $stockmup == null ? [] : $stockmup;
        // var_dump($stockmup);
        echo json_encode($data);
    }

    public function kartu_stock($idProduk)
    {
        $data['product_data'] = $this->Barang_model->get_data_product_byid($idProduk)->get()->row();
        $data['page_title'] = 'Halaman Kartu Stock';
        $data['main_content'] = 'stock/kartustock';
        $data['produknya'] = $idProduk;

        $this->load->view('template', $data);
        //var_dump($transaksi_terakhir);
    }

    public function kartu_stock_json($idProduk)
    {

        $dataMutasiAll = $this->Stock_model->stock_mutatio_byproduk_all(date("m"), $idProduk)->get()->result();
        $total = 0;
        $masuk = 0;
        $keluar = 0;
        foreach ($dataMutasiAll as $keys) {

            if ($keys->status == 0) {
                $masuk = $keys->amount;
                $keluar = 0;
                $total += $keys->amount;
            } else {
                $masuk = 0;
                $keluar = $keys->amount;
                $total += $keys->balance;
            }

            $keys->masuk = $masuk;
            $keys->keluar = $keluar;
            $keys->sisa = $total;
            $datamu[] = $keys;
        }

        $data['draw'] = 0;
        $data['recordsTotal'] = $datamu == null ? [] : count($datamu);
        $data['recordsFiltered'] = $datamu == null ? [] : count($datamu);
        $data['data'] = $datamu == null ? [] : $datamu;

        echo json_encode($data);
    }

    public function kartu_stock_range_json($idProduk, $dateStart, $dateEnd)
    {

        $dataMutasiAll = $this->Stock_model->stock_mutatio_byproduk_all_range($dateStart, $dateEnd, $idProduk)->get()->result();
        // $sisa_stock = $this->stock_sisa($dateStart,$idProduk);

        $total = $this->stock_sisa($dateStart, $idProduk);
        $masuk = 0;
        $keluar = 0;
        $datamu = [];
        foreach ($dataMutasiAll as $keys) {

            if ($keys->status == 0) {
                $masuk = $keys->amount;
                $keluar = 0;
                $total += $keys->amount;
            } else {
                $masuk = 0;
                $keluar = $keys->amount;
                $total += $keys->balance;
            }

            $keys->masuk = $masuk;
            $keys->keluar = $keluar;
            $keys->sisa = $total;
            $datamu[] = $keys;
        }

        $data['draw'] = 0;
        $data['recordsTotal'] = $datamu == null ? [] : count($datamu);
        $data['recordsFiltered'] = $datamu == null ? [] : count($datamu);
        $data['data'] = $datamu == null ? [] : $datamu;


        echo json_encode($data);
    }



    public function dashboard($month)
    {
        $totalOrder =  count($this->Penjualan_model->total_order_bymonth($month));
        $totalProdukOrder = $this->Penjualan_model->produk_total_bymonth($month)->total_produk;
        $totalPendapatan = $this->Penjualan_model->produk_total_penjualan($month)->total_penjualan;
        $topProduk = $this->Penjualan_model->top_product_bymonth($month);
        $produk = $this->Barang_model->total_produk_bymonth($month);
        $supplier = $this->Supplier_model->total_supplier_bymonth($month);

        $data = [
            'totalOrder' => $totalOrder == null ? 0 : $totalOrder,
            'totalProdukOrder' => $totalProdukOrder == null ? 0 : $totalProdukOrder,
            'totalPendapatan' => $totalPendapatan == null ? 0 : $totalPendapatan,
            'topProduk' => $topProduk == null ? [] : $topProduk,
            'produk' => $produk,
            'supplier' => $supplier == null ? 0 : count($supplier),
        ];

        echo json_encode($data);
    }


    public function json()
    {
        header('Content-Type: application/json');
        echo $this->Barang_model->json();
    }

    public function read($id)
    {
        $row = $this->Barang_model->get_by_id($id);
        if ($row) {
            $data = array(
                'id' => $row->id,
                'kode' => $row->kode,
                'nama' => $row->nama,
                'unit' => $row->unit,
                'category' => $row->category,
                'image' => $row->image,
                'stock' => $row->stock,
                'harga_penjualan' => $row->harga_penjualan,
                'min_stock' => $row->min_stock,
                'created_at' => $row->created_at,
            );
            $this->load->view('barang/barang_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('barang'));
        }
    }

    public function create()
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('barang/create_action'),
            'id' => set_value('id'),
            'kode' => set_value('kode'),
            'nama' => set_value('nama'),
            'unit' => set_value('unit'),
            'category' => set_value('category'),
            'image' => set_value('image'),
            'stock' => set_value('stock'),
            'harga_penjualan' => set_value('harga_penjualan'),
            'min_stock' => set_value('min_stock'),
            'created_at' => set_value('created_at'),
        );
        $this->load->view('barang/barang_form', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
                'kode' => $this->input->post('kode', TRUE),
                'nama' => $this->input->post('nama', TRUE),
                'unit' => $this->input->post('unit', TRUE),
                'category' => $this->input->post('category', TRUE),
                'image' => $this->input->post('image', TRUE),
                'stock' => $this->input->post('stock', TRUE),
                'harga_penjualan' => $this->input->post('harga_penjualan', TRUE),
                'min_stock' => $this->input->post('min_stock', TRUE),
                'created_at' => $this->input->post('created_at', TRUE),
            );

            $this->Barang_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('barang'));
        }
    }

    public function update($id)
    {
        $row = $this->Barang_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('barang/update_action'),
                'id' => set_value('id', $row->id),
                'kode' => set_value('kode', $row->kode),
                'nama' => set_value('nama', $row->nama),
                'unit' => set_value('unit', $row->unit),
                'category' => set_value('category', $row->category),
                'image' => set_value('image', $row->image),
                'stock' => set_value('stock', $row->stock),
                'harga_penjualan' => set_value('harga_penjualan', $row->harga_penjualan),
                'min_stock' => set_value('min_stock', $row->min_stock),
                'created_at' => set_value('created_at', $row->created_at),
            );
            $this->load->view('barang/barang_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('barang'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
                'kode' => $this->input->post('kode', TRUE),
                'nama' => $this->input->post('nama', TRUE),
                'unit' => $this->input->post('unit', TRUE),
                'category' => $this->input->post('category', TRUE),
                'image' => $this->input->post('image', TRUE),
                'stock' => $this->input->post('stock', TRUE),
                'harga_penjualan' => $this->input->post('harga_penjualan', TRUE),
                'min_stock' => $this->input->post('min_stock', TRUE),
                'created_at' => $this->input->post('created_at', TRUE),
            );

            $this->Barang_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('barang'));
        }
    }

    public function delete($id)
    {
        $row = $this->Barang_model->get_by_id($id);

        if ($row) {
            $this->Barang_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('barang'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('barang'));
        }
    }

    public function _rules()
    {
        $this->form_validation->set_rules('kode', 'kode', 'trim|required');
        $this->form_validation->set_rules('nama', 'nama', 'trim|required');
        $this->form_validation->set_rules('unit', 'unit', 'trim|required');
        $this->form_validation->set_rules('category', 'category', 'trim|required');
        $this->form_validation->set_rules('image', 'image', 'trim|required');
        $this->form_validation->set_rules('stock', 'stock', 'trim|required');
        $this->form_validation->set_rules('harga_penjualan', 'harga penjualan', 'trim|required');
        $this->form_validation->set_rules('min_stock', 'min stock', 'trim|required');
        $this->form_validation->set_rules('created_at', 'created at', 'trim|required');

        $this->form_validation->set_rules('id', 'id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }
}
