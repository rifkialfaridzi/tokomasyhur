<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Pengguna extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$data_session = $this->session->userdata;

		if ((!$this->session->userdata('logged_in')) || $data_session['level'] != 1 && $data_session['level'] != 3) {
			redirect('auth'); // Cek udah login apa belum, kalo belum login dulu
		}

		$this->load->model('Auth_model');
		$this->load->model('User_model');
		$this->load->library('form_validation');
		$this->load->library('datatables');
	}

	public function index()
	{
		$data['main_content'] = 'pengguna/main';
		$data['page_title'] = 'Halaman pengguna';
        $this->load->view('template',$data);
	}

	public function json()
	{
		header('Content-Type: application/json');
		$pengguna =  $this->Auth_model->get_by_level();
        //var_dump($pengguna);
		$data['draw'] = 0;
        $data['recordsTotal'] = $pengguna == null ? [] : count($pengguna);
        $data['recordsFiltered'] = $pengguna == null ? [] : count($pengguna);
        $data['data'] = $pengguna == null ? [] : $pengguna;
		
        echo json_encode($data);
	}


	public function create_action()
	{
		$data_session = $this->session->userdata;

		$this->form_validation->set_rules('username', 'username', 'required|is_unique[user.username]');
		$this->form_validation->set_rules('name', 'name', 'required');
		$this->form_validation->set_rules('password', 'password', 'required');
		$this->form_validation->set_rules('level', 'level', 'required');

		if ($data_session['level'] == 3) {

			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('pesan', 'Data Gagal Disimpan');
				redirect('pengguna');
			} else {
				$data = array(
					'name' => $this->input->post('name', TRUE),
					'username' => $this->input->post('username', TRUE),
					'password' => md5($this->input->post('password', TRUE)),
					'level' => $this->input->post('level', TRUE),
				);

				$this->User_model->insert($data);
				$this->session->set_flashdata('pesan', 'Data Sukses Disimpan');
				redirect('pengguna');
			}
		} else {
			redirect('pengguna');
		}
	}

	public function edit($id)
	{
		$row = $this->Auth_model->get_by_id($id);

		if ($row) {
			$data = array(
				'id' => $id,
				'data' => $row,
				'main_content' => 'pengguna/update',
                'page_title' => 'Edit pengguna'
			);
			$this->load->view('template', $data);
		} else {
			$this->session->set_flashdata('pesan', 'Data Tidak DItemukan');
			redirect(site_url('master/pengguna'));
		}
	}

	public function update_action($id)
	{
        if ($this->input->post("password") == "") {
            $data = [
                "name" =>  $this->input->post('name', TRUE),
                "username" =>  $this->input->post('username', TRUE),
                "level" =>  $this->input->post('level', TRUE),
                "name" =>  $this->input->post('name', TRUE),
            ];
        }else{
            $data = [
                "name" =>  $this->input->post('name', TRUE),
                "username" =>  $this->input->post('username', TRUE),
                "level" =>  $this->input->post('level', TRUE),
                "password" =>  $this->input->post('password', TRUE),
            ];
        }
		
		$pengguna = $this->Auth_model->get_by_id($id);
		$is_unique_name = $this->input->post('username', TRUE) != $pengguna->username ? '|is_unique[user.username]' : '';

		$this->form_validation->set_rules('username', 'Username', 'required'.$is_unique_name);//|edit_unique[barang.nama.' . $id . ']

		if ($this->form_validation->run() == FALSE) {
			 $this->session->set_flashdata('pesan', 'Data Gagal Di Ubah');
            redirect(site_url('pengguna'));
		} else {

			$this->Auth_model->update($id, $data);
			$this->session->set_flashdata('pesan', 'Data Sukses Di Ubah');
			redirect(site_url('pengguna'));
		}
	}

	public function delete($id)
	{
		$row = $this->Auth_model->get_by_id($id);

		if ($row) {
			$this->Auth_model->delete($id);
			$this->session->set_flashdata('pesan', 'Data Berhasil Di Hapus');
			redirect(site_url('pengguna'));
		} else {
			$this->session->set_flashdata('pesan', 'Data Tidak Ditemukan');
			redirect(site_url('pengguna'));
		}
	}

	public function _rules()
	{
		$this->form_validation->set_rules('nama', 'nama', 'required|is_unique[pengguna.nama]');

		$this->form_validation->set_rules('id', 'id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
	}

	public function excel()
	{
		$this->load->helper('exportexcel');
		$namaFile = "pengguna.xls";
		$judul = "pengguna";
		$tablehead = 0;
		$tablebody = 1;
		$nourut = 1;
		//penulisan header
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");
		header("Content-Disposition: attachment;filename=" . $namaFile . "");
		header("Content-Transfer-Encoding: binary ");

		xlsBOF();

		$kolomhead = 0;
		xlsWriteLabel($tablehead, $kolomhead++, "No");
		xlsWriteLabel($tablehead, $kolomhead++, "Nama");

		foreach ($this->pengguna_model->get_all() as $data) {
			$kolombody = 0;

			//ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
			xlsWriteNumber($tablebody, $kolombody++, $nourut);
			xlsWriteLabel($tablebody, $kolombody++, $data->nama);

			$tablebody++;
			$nourut++;
		}

		xlsEOF();
		exit();
	}
}

/* End of file pengguna.php */
/* Location: ./application/controllers/pengguna.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-03-21 14:22:20 */
/* http://harviacode.com */
