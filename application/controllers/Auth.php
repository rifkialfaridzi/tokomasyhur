<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Auth extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('auth_model');
		$this->load->model('User_model');
		$this->load->library('form_validation');
		$this->load->library('datatables');
	}

	public function index()
	{
		$data_session = $this->session->userdata;

		if (!empty($data_session['level'])) {

			switch ($data_session['level']) { // Login Sebagai APA ? 
				case "1":
					redirect('admin');
					break;
				case "2":
					redirect('kasir');
					break;
				case "3":
					redirect('owner');
					break;
			}
		}
		$this->load->view('auth/login');
	}


	public function user()
	{

		$data_session = $this->session->userdata;


		if ($data_session['level'] == 3) {

			$data['main_content'] = 'add_user';
			$data['page_title'] = 'Halaman User';


			$this->load->view('template', $data);
		} else {
			redirect('auth');
		}
	}

	public function create_user()
	{

		$data_session = $this->session->userdata;

		$this->form_validation->set_rules('username', 'username', 'required|is_unique[user.username]');
		$this->form_validation->set_rules('name', 'name', 'required');
		$this->form_validation->set_rules('password', 'password', 'required');
		$this->form_validation->set_rules('level', 'level', 'required');

		if ($data_session['level'] == 3) {

			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('pesan', 'Data Gagal Disimpan');
				redirect(site_url('master/user'));
			} else {
				$data = array(
					'name' => $this->input->post('name', TRUE),
					'username' => $this->input->post('username', TRUE),
					'password' => md5($this->input->post('password', TRUE)),
					'level' => $this->input->post('level', TRUE),
				);

				$this->User_model->insert($data);
				$this->session->set_flashdata('pesan', 'Data Sukses Disimpan');
				redirect(site_url('master/user'));
			}
		} else {
			redirect('auth');
		}
	}

	public function edit_user($id)
	{
		$row = $this->User_model->get_by_id($id);

		if ($row) {
			$data = array(
				'id' => set_value('id', $row->id),
				'name' => set_value('nama', $row->name),
				'username' => set_value('username', $row->username),
				'level' => set_value('level', $row->level),

				'main_content' => 'change_user',
				'page_title' => 'Edit User'
			);
			$this->load->view('template', $data);
		} else {
			$this->session->set_flashdata('pesan', 'Data Tidak DItemukan');
			redirect(site_url('master/category'));
		}
	}

	public function update_user($id)
	{

		$data_session = $this->session->userdata;

		$user = $this->User_model->get_by_id($id);


		$this->form_validation->set_rules('name', 'name', 'required');
		$this->form_validation->set_rules('password', 'password', 'trim');
		$this->form_validation->set_rules('level', 'level', 'required');

		if ($data_session['level'] == 3) {

			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('pesan', 'Data Gagal Disimpan');
				redirect(site_url('master/user'));
			} else {

				if ($this->input->post('password', TRUE) == null) {
					$data = array(
						'name' => $this->input->post('name', TRUE),
						'level' => $this->input->post('level', TRUE),
					);
				} else {
					$data = array(
						'name' => $this->input->post('name', TRUE),
						'password' => md5($this->input->post('password', TRUE)),
						'level' => $this->input->post('level', TRUE),
					);
				}


				$this->User_model->update($id,$data);
				$this->session->set_flashdata('pesan', 'Data Sukses Disimpan');
				redirect(site_url('master/user'));
			}
		} else {
			redirect('auth');
		}
	}

	public function user_non_owner_json()
	{

		$data_session = $this->session->userdata;


		if ($data_session['level'] == 3) {

			$dataUser = $this->User_model->get_non_owner();

			$data['draw'] = 0;
			$data['recordsTotal'] = $dataUser == null ? [] : count($dataUser);
			$data['recordsFiltered'] = $dataUser == null ? [] : count($dataUser);
			$data['data'] = $dataUser == null ? [] : $dataUser;

			echo json_encode($data);
		} else {
			redirect('auth');
		}
	}

	public function user_delete($id)
	{
		$row = $this->User_model->get_by_id($id);

		if ($row) {
			$this->User_model->delete($id);
			$this->session->set_flashdata('pesan', 'Data Berhasil Di Hapus');
			redirect(site_url('master/user'));
		} else {
			$this->session->set_flashdata('pesan', 'Data Tidak Ditemukan');
			redirect(site_url('master/user'));
		}
	}

	public function change_name()
	{
		$data_session = $this->session->userdata;
		$dataUser = $this->User_model->get_by_id($data_session['id']);
		// var_dump($pembelian_terakhir);

		$data['main_content'] = 'change_name';
		$data['page_title'] = 'Halaman User';
		$data['dataUser'] = $dataUser;

		$this->load->view('template', $data);
	}

	public function change_name_action()
	{

		$data_session = $this->session->userdata;


		$this->form_validation->set_rules('name', 'name', 'trim|required');
		$data_session = $this->session->userdata;
		$dataUser = $this->User_model->get_by_id($data_session['id']);

		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('message', 'Gagal Di Simpan');
			redirect(site_url('/'));
		} else {
			$data = array(

				'name' => $this->input->post('name', TRUE)
			);

			$this->User_model->update($data_session['id'], $data);
			$this->session->set_flashdata('message', 'Sukses Update Data');
			redirect(site_url('/'));
		}
	}

	function login()
	{ // Cek Login

		$username = htmlspecialchars($this->input->post('username', TRUE), ENT_QUOTES); // cek special caracter
		$password = htmlspecialchars($this->input->post('password', TRUE), ENT_QUOTES);

		$cek = $this->auth_model->auth_login($username, $password); // ambil data user login

		if ($cek->num_rows() > 0) { //jika login sebagai dosen
			$data = $cek->row_array();

			$this->session->set_userdata('level', $data['level']);
			$this->session->set_userdata('id', $data['id']);
			$this->session->set_userdata('username', $data['username']);
			$this->session->set_userdata('logged_in', true);

			if ($data['level'] == '1') { // Jika Akses admin gudang
				redirect('admin');
			} elseif ($data['level'] == '2') { // Jika Akses Kasir
				redirect('kasir');
			} elseif ($data['level'] == '3') { // Jika Akses Owner
				redirect('owner');
			}
		} else { //jika login gagal
			$url = base_url();
			$this->session->set_flashdata('pesan', 'Username Atau Password Salah');
			redirect($url);
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();

		redirect('auth', 'refresh');
	}



	public function json()
	{
		header('Content-Type: application/json');
		echo $this->Barang_model->json();
	}

	public function read($id)
	{
		$row = $this->Barang_model->get_by_id($id);
		if ($row) {
			$data = array(
				'id' => $row->id,
				'kode' => $row->kode,
				'nama' => $row->nama,
				'unit' => $row->unit,
				'category' => $row->category,
				'image' => $row->image,
				'stock' => $row->stock,
				'harga_penjualan' => $row->harga_penjualan,
				'min_stock' => $row->min_stock,
				'created_at' => $row->created_at,
			);
			$this->load->view('barang/barang_read', $data);
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('barang'));
		}
	}

	public function create()
	{
		$data = array(
			'button' => 'Create',
			'action' => site_url('barang/create_action'),
			'id' => set_value('id'),
			'kode' => set_value('kode'),
			'nama' => set_value('nama'),
			'unit' => set_value('unit'),
			'category' => set_value('category'),
			'image' => set_value('image'),
			'stock' => set_value('stock'),
			'harga_penjualan' => set_value('harga_penjualan'),
			'min_stock' => set_value('min_stock'),
			'created_at' => set_value('created_at'),
		);
		$this->load->view('barang/barang_form', $data);
	}

	public function create_action()
	{
		$this->_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->create();
		} else {
			$data = array(
				'kode' => $this->input->post('kode', TRUE),
				'nama' => $this->input->post('nama', TRUE),
				'unit' => $this->input->post('unit', TRUE),
				'category' => $this->input->post('category', TRUE),
				'image' => $this->input->post('image', TRUE),
				'stock' => $this->input->post('stock', TRUE),
				'harga_penjualan' => $this->input->post('harga_penjualan', TRUE),
				'min_stock' => $this->input->post('min_stock', TRUE),
				'created_at' => $this->input->post('created_at', TRUE),
			);

			$this->Barang_model->insert($data);
			$this->session->set_flashdata('message', 'Create Record Success');
			redirect(site_url('barang'));
		}
	}

	public function update($id)
	{
		$row = $this->Barang_model->get_by_id($id);

		if ($row) {
			$data = array(
				'button' => 'Update',
				'action' => site_url('barang/update_action'),
				'id' => set_value('id', $row->id),
				'kode' => set_value('kode', $row->kode),
				'nama' => set_value('nama', $row->nama),
				'unit' => set_value('unit', $row->unit),
				'category' => set_value('category', $row->category),
				'image' => set_value('image', $row->image),
				'stock' => set_value('stock', $row->stock),
				'harga_penjualan' => set_value('harga_penjualan', $row->harga_penjualan),
				'min_stock' => set_value('min_stock', $row->min_stock),
				'created_at' => set_value('created_at', $row->created_at),
			);
			$this->load->view('barang/barang_form', $data);
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('barang'));
		}
	}

	public function update_action()
	{
		$this->_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->update($this->input->post('id', TRUE));
		} else {
			$data = array(
				'kode' => $this->input->post('kode', TRUE),
				'nama' => $this->input->post('nama', TRUE),
				'unit' => $this->input->post('unit', TRUE),
				'category' => $this->input->post('category', TRUE),
				'image' => $this->input->post('image', TRUE),
				'stock' => $this->input->post('stock', TRUE),
				'harga_penjualan' => $this->input->post('harga_penjualan', TRUE),
				'min_stock' => $this->input->post('min_stock', TRUE),
				'created_at' => $this->input->post('created_at', TRUE),
			);

			$this->Barang_model->update($this->input->post('id', TRUE), $data);
			$this->session->set_flashdata('message', 'Update Record Success');
			redirect(site_url('barang'));
		}
	}

	public function delete($id)
	{
		$row = $this->Barang_model->get_by_id($id);

		if ($row) {
			$this->Barang_model->delete($id);
			$this->session->set_flashdata('message', 'Delete Record Success');
			redirect(site_url('barang'));
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('barang'));
		}
	}

	public function _rules()
	{
		$this->form_validation->set_rules('kode', 'kode', 'trim|required');
		$this->form_validation->set_rules('nama', 'nama', 'trim|required');
		$this->form_validation->set_rules('unit', 'unit', 'trim|required');
		$this->form_validation->set_rules('category', 'category', 'trim|required');
		$this->form_validation->set_rules('image', 'image', 'trim|required');
		$this->form_validation->set_rules('stock', 'stock', 'trim|required');
		$this->form_validation->set_rules('harga_penjualan', 'harga penjualan', 'trim|required');
		$this->form_validation->set_rules('min_stock', 'min stock', 'trim|required');
		$this->form_validation->set_rules('created_at', 'created at', 'trim|required');

		$this->form_validation->set_rules('id', 'id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
	}
}
