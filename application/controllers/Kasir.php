<?php

use SebastianBergmann\Environment\Console;

date_default_timezone_set('Asia/Jakarta');
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Kasir extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $data_session = $this->session->userdata;

        if ((!$this->session->userdata('logged_in'))  || $data_session['level'] != 2 && $data_session['level'] != 3) {
            redirect('auth'); // Cek udah login apa belum, kalo belum login dulu
        }

        $this->load->model('Category_model');
        $this->load->model('Barang_model');
        $this->load->model('Supplier_model');
        $this->load->model('Penjualan_model');
        $this->load->model('Penjualan_detail_model');

        $this->load->model('Stock_model');
        $this->load->model('auth_model');
        $this->load->library('form_validation');
        $this->load->library('datatables');
    }

    public function index()
    {
        $transaksi_terakhir = $this->Penjualan_model->penjualan_terakhir();
        $data['page_title'] = 'Halaman Dashboard';
        $data['main_content'] = 'kasir/main';
        $data['transaksi_terakhir'] = $transaksi_terakhir == null ? [] : $transaksi_terakhir;

        $this->load->view('template', $data);
        //var_dump($transaksi_terakhir);
    }

    public function transaksi()
    {
        $transaksi_terakhir = $this->Penjualan_model->penjualan_terakhir();
        $data['page_title'] = 'Halaman Dashboard';
        $data['main_content'] = 'kasir/transaksi';
        $data['transaksi_terakhir'] = $transaksi_terakhir == null ? [] : $transaksi_terakhir;

        $this->load->view('template', $data);
        //var_dump($transaksi_terakhir);
    }

    public function transaksi_bymonth_json($month)
    {
        //     $data['page_title'] = 'Halaman Data Transaksi';
        //     $data['main_content'] = 'kasir/transaksi';
        //     $this->load->view('template', $data);
        echo  $this->Penjualan_model->transaksi_bymonth($month);
    }

    public function dashboard($month)
    {
        $totalOrder =  count($this->Penjualan_model->total_order_bymonth($month));
        $totalProdukOrder = $this->Penjualan_model->produk_total_bymonth($month)->total_produk;
        $totalPendapatan = $this->Penjualan_model->produk_total_penjualan($month)->total_penjualan;
        $topProduk = $this->Penjualan_model->top_product_bymonth($month);
        $produk = $this->Barang_model->total_produk_bymonth($month);
        $supplier = $this->Supplier_model->total_supplier_bymonth($month);

        $data = [
            'totalOrder' => $totalOrder == null ? 0 : $totalOrder,
            'totalProdukOrder' => $totalProdukOrder == null ? 0 : $totalProdukOrder,
            'totalPendapatan' => $totalPendapatan == null ? 0 : $totalPendapatan,
            'topProduk' => $topProduk == null ? [] : $topProduk,
            'produk' => $produk,
            'supplier' => $supplier == null ? 0 : count($supplier),
        ];

        echo json_encode($data);
    }
    public function data_dashboard($month)
    {
        $totalOrder =  count($this->Penjualan_model->total_order_bymonth($month));
        $totalProdukOrder = $this->Penjualan_model->produk_total_bymonth($month)->total_produk;
        $totalPendapatan = $this->Penjualan_model->produk_total_penjualan($month)->total_penjualan;
        $topProduk = $this->Penjualan_model->top_product_bymonth($month);
        $produk = $this->Barang_model->total_produk_bymonth($month);
        $supplier = $this->Supplier_model->total_supplier_bymonth($month);


        $dataMutasiOut = $this->Stock_model->stock_mutatio_byproduk_out($month, 6)->get()->result();
        $dataMutasiIn = $this->Stock_model->stock_mutatio_byproduk_in($month, 6)->get()->result();
        $dataMutasiAll = $this->Stock_model->stock_mutatio_byproduk_all($month, 6)->get()->result();
        $dataMutasiGroupByDate = $this->Stock_model->stock_mutatio_byproduk_groupbydate($month, 6)->get()->result();

        // $tanggal = [];
        // $dataMutation = [];
        // foreach ($dataMutasiGroupByDate as $key) {
        //     $tanggal[]= $key->tanggal;
        //     foreach ($dataMutasiAll as $keys) {

        //         if ($keys->status == 0) {
        //             $dataMutasiAll['mutation_in'][]= $keys->status;
        //         }


        //     }
        // }


        $tanggalTmp = [];
        $DataTmp = [];
        $dateCounter = 0;
        // foreach ($dataMutasiGroupByDate as $key) {

        //     $DataTmp[$dateCounter++]['tanggal'][] = $key->tanggal;

        //     foreach ($dataMutasiAll as $keys) {
        //         if ($key->tanggal == $keys->created_at) {

        //             if ($keys->status == 0) {
        //                 $a['mutasi']['pembelian']['balance'][] = $keys->balance;
        //             }else{
        //                 $a['mutasi']['pembelian']['balance'][] = "-";
        //             } 

        //             if ($keys->status == 1) {
        //                 $a['mutasi']['penjualan'][] = $keys->balance;
        //             }else{
        //                 $a['mutasi']['penjualan'][] = "-";
        //             } 

        //         }
        //     }

        // }


        // $key = [];
        $total = 0;

        // foreach ($dataMutasiAll as $keys) {

        //     if ($keys->status == 0) {
        //         $masuk = $keys->amount;
        //         $keluar = 0;
        //         $total += $keys->amount;
        //     } else {
        //         $masuk = 0;
        //         $keluar = $keys->amount;
        //         $total += $keys->balance;
        //     }

        //     $keys->masuk = $masuk;
        //     $keys->keluar = $keluar;
        //     $keys->sisa = $total;
        //     $datamu[] = $keys;
        // }

        $allProduk = $this->Barang_model->get_all();

        $masuk = 0;
        $c = 0;
        foreach ($allProduk as $produk) {

            $produk->totalMasuk = $this->Stock_model->sum_stockin_bymonth($produk->id, 5)->total_masuk;
            $produk->totalKeluar = $this->Stock_model->sum_stockout_bymonth($produk->id, 5)->total_keluar;
            $produk->totalAvailable = $this->Stock_model->sum_stockavailable_bymonth($produk->id, 5)->total_available;
            $stockmup[] = $produk;
        }
        var_dump($stockmup);
        // if ($keys->status == 0) {
        //     $s['mutation_in'][]= $keys->status;
        // }

        // $datamutasi[] = $keys->status;


        //echo json_encode($data1);
    }

    public function control_stock()
    {

        $allProduk = $this->Barang_model->get_all();

        foreach ($allProduk as $produk) {

            $produk->totalMasuk = $this->Stock_model->sum_stockin_bymonth($produk->id, 5)->total_masuk;
            $produk->totalKeluar = $this->Stock_model->sum_stockout_bymonth($produk->id, 5)->total_keluar;
            $produk->totalAvailable = $this->Stock_model->sum_stockavailable_bymonth($produk->id, 5)->total_available;
            $stockmup[] = $produk;
        }

        $data['draw'] = 0;
        $data['recordsTotal'] = $stockmup == null ? [] : count($stockmup);
        $data['recordsFiltered'] = $stockmup == null ? [] : count($stockmup);
        $data['data'] = $stockmup == null ? [] : $stockmup;

        echo json_encode($data);
    }

    public function print_nota($id)
    {

        $detailTransaksi = $this->Penjualan_model->get_data_product_byid($id)->get()->result();

       
       
        $transaksi = $this->Penjualan_model->get_by_id($id);

        if ($transaksi) {

            $data = [
                'transaksi' => $transaksi,
                'detailTransaksi' => $detailTransaksi,
                'company' => 'Maharany Skar Solo',
                'image_logo' => 'assets/Logo.png',
            ];
            // echo json_encode($data);
            // var_dump($data);
            $this->load->library('pdf');

            $this->pdf->setPaper('A4', 'potrait');
            $this->pdf->set_option('isRemoteEnabled', TRUE);
            $this->pdf->filename = "print_qrcode_" . $transaksi->kode . ".pdf";
            $this->pdf->load_view('kasir/print_nota', $data);
        } else {
            $this->session->set_flashdata('pesan', 'Data Tidak DItemukan');
            redirect(site_url('kasir/produk'));
        }
    }

    public function bayar_tagihan()
    {

        // $data_cart = [
        //     ['id' => 6, 'nama' => "Produk ID 6","qty" => 10],
        //     ['id' => 8, 'nama' => "Produk ID 8","qty" => 25]
        // ];
        $params = json_decode(file_get_contents('php://input'), true);


        $data_cart = $this->cart->contents();

        $sisaStock = $this->Barang_model->get_by_id(6)->stock;

        $stockTidakCukup = 0;
        $dataTransaksi = "";
        $dataDetailTransaksi = "";
        $idLastTransaksi = "";
        $data_barang_keluar = [];

        if ($params['bill'] < $this->total_cart()) {
            $this->session->set_flashdata('pesan', 'Uang Tidak Mencukupi');
            // redirect(site_url('kasir/produk'));
        } else {


            foreach ($data_cart as $keys) {  // CEK STOCK APA AJA YG NGGAK CUKUP
                $sisaStock = $this->Barang_model->get_by_id($keys['id'])->stock;
                $min_stock = $this->Barang_model->get_by_id($keys['id'])->min_stock;


                if ($keys['qty'] > ($sisaStock - $min_stock)) {
                    //echo "Stcok Tidak Cukup";
                    $stockTidakCukup++; // HITUNG JUMLAH STOCK YANG TIDAK CUKUP
                }
            }

            if ($stockTidakCukup > 0) { // JIKA STOCktidak cukup lebih dari 0
                $this->session->set_flashdata('pesan', 'Stock Produk Ada Yang Tidak Mencukupi, Mohon Cek Kembali');
                redirect(site_url('kasir/produk'));
            } else {

                $dataTransaksi = [
                    'kode' => "TRX" . mt_rand(100000, 999999) . "-" . date('Ymd'),
                    'user' => 1,
                    'total_penjualan' =>  $this->total_cart(),
                    'pelanggan' => "Pelanggan Terhormat",
                    'bill' => $params['bill'],
                    'kembalian' => $params['kembalian'],
                    'created_at' => date("Y-m-d H:i:s"),
                ];
                $idLastTransaksi = $this->Penjualan_model->insert($dataTransaksi); // INSERT DATA BARU SBG PENGELUARAN
                $rows =  $this->db->affected_rows();
                //var_dump($rows);
                //    var_dump($idLastTransaksi);


                // OPERASI UPDATE DAN PENGURANGAN STOCK
                $harga_jual = 0;
                $subtotal = 0;
                foreach ($data_cart as $key) { // CEK STOK

                    $sisaStock = $this->Barang_model->get_by_id($key['id'])->stock;
                    $min_stock = $this->Barang_model->get_by_id($key['id'])->min_stock;


                    $data_produk = $this->Barang_model->get_data_product_byid($key['id'])->get()->row();

                    if ($key['qty'] >= $data_produk->min_grosir) {

                        $harga_jual = $data_produk->harga_grosir;
                        $subtotal = $key['qty'] * $data_produk->harga_grosir;
                    } else {
                        $harga_jual = $data_produk->harga_penjualan;
                        $subtotal = $key['subtotal'];
                    }



                    if ($key['qty'] > ($sisaStock - $min_stock)) {
                        //echo "Stcok Tidak Cukup";
                        $stockTidakCukup++;
                    }
                    $qty = $key['qty'];

                    if ($stockTidakCukup > 0) {
                        $this->session->set_flashdata('pesan', 'Stock Produk Ada Yang Tidak Mencukupi, Mohon Cek Kembali');
                        redirect(site_url('kasir/produk'));
                    } else {

                        $dataDetailTransaksi = [
                            'barang' => $key['id'],
                            'jumlah' => $key['qty'],
                            'harga_jual' => $harga_jual,
                            'total' =>  $subtotal,
                            'penjualan' => $idLastTransaksi,
                        ];
                        $this->Penjualan_detail_model->insert($dataDetailTransaksi);  // INSERT DETAIL TRANSAKSI


                        $stocks = $this->Stock_model->get_stock_byidproduct_asc($key['id']);
                        foreach ($stocks as $stock) {
                            $brangMasuk = $stock->kode;
                            $stockProduct = $stock->balance;

                            if ($qty > 0) {

                                $qtyTemp = $qty;
                                $qty = $qty - $stockProduct;

                                if ($qty > 0) {
                                    $stok_update = 0;
                                    $stock_keluar = $stockProduct;
                                } else {
                                    $stok_update = $qtyTemp - $stockProduct;
                                    $stock_keluar = $stockProduct + ($qtyTemp - $stockProduct);
                                }

                                $data_barang_keluar[] = array(
                                    'kode' => "OTR" . mt_rand(100000, 999999),
                                    'barang_masuk' => $stock->barang_masuk,
                                    'barang' => $stock->barang,
                                    'supplier' => $stock->supplier,
                                    'harga_pembelian' => $stock->harga_pembelian,
                                    'harga_penjualan' => $stock->harga_penjualan,
                                    'amount' => "-" . $stock_keluar,
                                    'balance' => "-" . $stock_keluar,
                                    'note' => "Barang Ok",
                                    'total_pembelian' => $key['subtotal'],
                                    'status' => 1,
                                    'created_at' => date('Y-m-d'),
                                );

                                $update_stock = array(
                                    'balance' => $stockProduct - $stock_keluar,
                                );

                                $this->Stock_model->update($stock->id, $update_stock); // UPDATE ORIGIN STOCK 

                            }
                        }

                        $update_stock_product = array(
                            'stock' => $sisaStock - $key['qty'],
                        );

                        $this->Barang_model->update($key['id'], $update_stock_product); // UPDATE ORIGIN STOCK 
                    }
                }
                foreach ($data_barang_keluar as $barang_keluar) {
                    $this->Stock_model->insert($barang_keluar); // INSERT DATA BARU SBG PENGELUARAN
                }
                $this->cart->destroy();
                $this->session->set_flashdata('pesan', '<strong>Transaksi Sukses</strong> Cetak Nota <a target="_blank" href="' . base_url("kasir/print_nota/").$idLastTransaksi . '">Disini</a>');
                echo $idLastTransaksi;
                // redirect(site_url('kasir/print_nota/'+$idLastTransaksi));
            }
        }


        // var_dump($data_barang_keluar);

        //$this->Barang_model->get_all();

    }

    public function data_kategori()
    {
        echo json_encode($this->Category_model->get_all());
    }

    public function data_product()
    {
        $data = [
            "items" => $this->Barang_model->get_all(),
            "totalItems" => count($this->Barang_model->get_all())
        ];
        echo json_encode($data);
    }

    public function data_product_bykategori($id_kategori)
    {

        if ($id_kategori == "all") {
            $data =  $this->Barang_model->get_all();
        } else {
            $data =  $this->Barang_model->get_bykategori($id_kategori);
        }

        if ($data) {
            $dataProduct = [
                "items" => $data,
                "totalItems" => count($data),
                "msg" => "Data Ditemukan"
            ];
        } else {
            $dataProduct = [
                "items" => [],
                "totalItems" => 0,
                "msg" => "Data Tidak Ditemukan"
            ];
        }

        echo json_encode($dataProduct);
    }

    public function data_detail_product($id)
    {

        $data = $this->Barang_model->get_data_product_byid($id)->get()->result();

        if ($data) {
            $dataProduct = [
                "items" => $data,
                "msg" => "Data Ditemukan"
            ];
        } else {
            $dataProduct = [
                "items" => [],
                "msg" => "Data Tidak Ditemukan"
            ];
        }

        echo json_encode($dataProduct);
    }

    public function produk()
    {
        $this->load->view('kasir/produk');
    }

    public function produk_detail($id)
    {
        $produk = $this->Barang_model->get_data_product_byid($id)->get()->row();

        if ($produk) {
            $data['data'] = $produk;
            $this->load->view('kasir/produk-detail', $data);
        } else {
            echo "<script>
alert('Data Tidak Ditemukan');
window.location.href='" . site_url('kasir/produk') . "';
</script>";
            // $this->session->set_flashdata('message', 'Record Not Found');
            // redirect(site_url('kasir/produk'));
        }
    }

    public function total_cart()
    {

        $data_cart = $this->cart->contents();
        $load_data_cart = [];
        $final_data_cart = [];
        $total_cart = 0;
        if ($data_cart) {


            foreach ($data_cart as $key) {

                $data_produk = $this->Barang_model->get_data_product_byid($key['id'])->get()->row();

                if ($key['qty'] >= $data_produk->min_grosir) {

                    $load_data_cart['grosir'] = 1;
                    $load_data_cart['subtotal'] = $key['qty'] * $data_produk->harga_grosir;
                    $load_data_cart['price'] = $data_produk->harga_grosir;

                    $total_cart += $key['qty'] * $data_produk->harga_grosir;
                } else {
                    $load_data_cart['grosir'] = 0;
                    $load_data_cart['subtotal'] = $key['subtotal'];
                    $load_data_cart['price'] = $key['price'];

                    $total_cart += $key['subtotal'];
                }
            }
        }


        return $total_cart;
    }

    public function produk_cart()
    {
        $data_cart = $this->cart->contents();
        $load_data_cart = [];
        $final_data_cart = [];
        $total_cart = 0;
        if ($data_cart) {


            foreach ($data_cart as $key) {

                $data_produk = $this->Barang_model->get_data_product_byid($key['id'])->get()->row();

                if ($key['qty'] >= $data_produk->min_grosir) {

                    $load_data_cart['grosir'] = 1;
                    $load_data_cart['subtotal'] = $key['qty'] * $data_produk->harga_grosir;
                    $load_data_cart['price'] = $data_produk->harga_grosir;

                    $total_cart += $key['qty'] * $data_produk->harga_grosir;
                } else {
                    $load_data_cart['grosir'] = 0;
                    $load_data_cart['subtotal'] = $key['subtotal'];
                    $load_data_cart['price'] = $key['price'];

                    $total_cart += $key['subtotal'];
                }

                $load_data_cart['rowid'] = $key['rowid'];
                $load_data_cart['name'] = $key['name'];
                $load_data_cart['stock'] = $data_produk->stock;
                $load_data_cart['min_grosir'] = $data_produk->min_grosir;
                $load_data_cart['harga_grosir'] = $data_produk->harga_grosir;
                $load_data_cart['qty'] = $key['qty'];
                $load_data_cart['image'] = $data_produk->image;
                $load_data_cart['note'] = $data_produk->note;
                $load_data_cart['kode'] = $data_produk->kode;
                $load_data_cart['category_name'] = $data_produk->category_name;
                $load_data_cart['id_product'] = $key['id'];

                $final_data_cart[] = $load_data_cart;
            }
        }

        $data['data_cart'] = $final_data_cart;
        $data['total_cart'] = $total_cart;

        $this->load->view('kasir/produk-cart', $data);
    }

    public function json()
    {
        header('Content-Type: application/json');
        echo $this->Barang_model->json();
    }

    public function read($id)
    {
        $row = $this->Barang_model->get_by_id($id);
        if ($row) {
            $data = array(
                'id' => $row->id,
                'kode' => $row->kode,
                'nama' => $row->nama,
                'unit' => $row->unit,
                'category' => $row->category,
                'image' => $row->image,
                'stock' => $row->stock,
                'harga_penjualan' => $row->harga_penjualan,
                'min_stock' => $row->min_stock,
                'created_at' => $row->created_at,
            );
            $this->load->view('barang/barang_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('barang'));
        }
    }

    public function create()
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('barang/create_action'),
            'id' => set_value('id'),
            'kode' => set_value('kode'),
            'nama' => set_value('nama'),
            'unit' => set_value('unit'),
            'category' => set_value('category'),
            'image' => set_value('image'),
            'stock' => set_value('stock'),
            'harga_penjualan' => set_value('harga_penjualan'),
            'min_stock' => set_value('min_stock'),
            'created_at' => set_value('created_at'),
        );
        $this->load->view('barang/barang_form', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
                'kode' => $this->input->post('kode', TRUE),
                'nama' => $this->input->post('nama', TRUE),
                'unit' => $this->input->post('unit', TRUE),
                'category' => $this->input->post('category', TRUE),
                'image' => $this->input->post('image', TRUE),
                'stock' => $this->input->post('stock', TRUE),
                'harga_penjualan' => $this->input->post('harga_penjualan', TRUE),
                'min_stock' => $this->input->post('min_stock', TRUE),
                'created_at' => $this->input->post('created_at', TRUE),
            );

            $this->Barang_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('barang'));
        }
    }

    public function update($id)
    {
        $row = $this->Barang_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('barang/update_action'),
                'id' => set_value('id', $row->id),
                'kode' => set_value('kode', $row->kode),
                'nama' => set_value('nama', $row->nama),
                'unit' => set_value('unit', $row->unit),
                'category' => set_value('category', $row->category),
                'image' => set_value('image', $row->image),
                'stock' => set_value('stock', $row->stock),
                'harga_penjualan' => set_value('harga_penjualan', $row->harga_penjualan),
                'min_stock' => set_value('min_stock', $row->min_stock),
                'created_at' => set_value('created_at', $row->created_at),
            );
            $this->load->view('barang/barang_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('barang'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
                'kode' => $this->input->post('kode', TRUE),
                'nama' => $this->input->post('nama', TRUE),
                'unit' => $this->input->post('unit', TRUE),
                'category' => $this->input->post('category', TRUE),
                'image' => $this->input->post('image', TRUE),
                'stock' => $this->input->post('stock', TRUE),
                'harga_penjualan' => $this->input->post('harga_penjualan', TRUE),
                'min_stock' => $this->input->post('min_stock', TRUE),
                'created_at' => $this->input->post('created_at', TRUE),
            );

            $this->Barang_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('barang'));
        }
    }

    public function delete($id)
    {
        $row = $this->Barang_model->get_by_id($id);

        if ($row) {
            $this->Barang_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('barang'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('barang'));
        }
    }

    public function _rules()
    {
        $this->form_validation->set_rules('kode', 'kode', 'trim|required');
        $this->form_validation->set_rules('nama', 'nama', 'trim|required');
        $this->form_validation->set_rules('unit', 'unit', 'trim|required');
        $this->form_validation->set_rules('category', 'category', 'trim|required');
        $this->form_validation->set_rules('image', 'image', 'trim|required');
        $this->form_validation->set_rules('stock', 'stock', 'trim|required');
        $this->form_validation->set_rules('harga_penjualan', 'harga penjualan', 'trim|required');
        $this->form_validation->set_rules('min_stock', 'min stock', 'trim|required');
        $this->form_validation->set_rules('created_at', 'created at', 'trim|required');

        $this->form_validation->set_rules('id', 'id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "barang.xls";
        $judul = "barang";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
        xlsWriteLabel($tablehead, $kolomhead++, "Kode");
        xlsWriteLabel($tablehead, $kolomhead++, "Nama");
        xlsWriteLabel($tablehead, $kolomhead++, "Unit");
        xlsWriteLabel($tablehead, $kolomhead++, "Category");
        xlsWriteLabel($tablehead, $kolomhead++, "Image");
        xlsWriteLabel($tablehead, $kolomhead++, "Stock");
        xlsWriteLabel($tablehead, $kolomhead++, "Harga Penjualan");
        xlsWriteLabel($tablehead, $kolomhead++, "Min Stock");
        xlsWriteLabel($tablehead, $kolomhead++, "Created At");

        foreach ($this->Barang_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
            xlsWriteLabel($tablebody, $kolombody++, $data->kode);
            xlsWriteLabel($tablebody, $kolombody++, $data->nama);
            xlsWriteNumber($tablebody, $kolombody++, $data->unit);
            xlsWriteNumber($tablebody, $kolombody++, $data->category);
            xlsWriteLabel($tablebody, $kolombody++, $data->image);
            xlsWriteLabel($tablebody, $kolombody++, $data->stock);
            xlsWriteLabel($tablebody, $kolombody++, $data->harga_penjualan);
            xlsWriteLabel($tablebody, $kolombody++, $data->min_stock);
            xlsWriteLabel($tablebody, $kolombody++, $data->created_at);

            $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }
}
