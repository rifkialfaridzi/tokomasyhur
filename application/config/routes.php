<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['master/unit'] = 'unit/index';
$route['master/unit/edit/(:any)'] = 'unit/edit/$1';

$route['master/category'] = 'category/index';
$route['master/category/edit/(:any)'] = 'category/edit/$1';

$route['master/produk'] = 'barang/index';

$route['master/supplier'] = 'supplier/index';
$route['master/supplier/edit/(:any)'] = 'supplier/edit/$1';

$route['master/rak'] = 'rak/index';
$route['master/rak/edit/(:any)'] = 'rak/edit/$1';

$route['master/change_name'] = 'auth/change_name';
$route['master/user'] = 'auth/user';
$route['master/user/edit/(:any)'] = 'auth/edit_user/$1';

$route['master/produk'] = 'barang/index';
$route['master/produk/edit/(:any)'] = 'barang/edit/$1';

$route['pembelian/masuk'] = 'barang_masuk/index';
$route['pembelian/masuk/edit/(:any)'] = 'barang_masuk/edit/$1';
$route['pembelian/masuk/delete/(:any)'] = 'barang_masuk/delete/$1';

$route['pembelian/masuk/stock/(:any)'] = 'barang_masuk/stock/$1';
$route['pembelian/masuk/stock/print/(:any)'] = 'barang_masuk/print_faktur/$1';
$route['pembelian/masuk/refund/(:any)'] = 'barang_masuk/refund/$1';
$route['pembelian/masuk/refund/print/(:any)'] = 'barang_masuk/print_retur/$1';

$route['pembelian/refund'] = 'refund/index';
$route['pembelian/refund/detail/(:any)'] = 'refund/detail/$1';

$route['kasir/produk'] = 'kasir/produk';
$route['kasir/produk/detail/(:any)'] = 'kasir/produk_detail/$1';
$route['kasir/produk/cart'] = 'kasir/produk_cart';
$route['kasir/bayar'] = 'kasir/bayar_tagihan';

$route['transaksi'] = 'kasir/transaksi';

$route['kontrol'] = 'admin/kontrol_stock';
$route['kontrol/stock/(:any)'] = 'admin/kartu_stock/$1';

$route['default_controller'] = 'auth';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
