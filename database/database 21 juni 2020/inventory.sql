-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 21, 2020 at 05:21 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.3.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `inventory`
--

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `id` int(11) NOT NULL,
  `kode` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `unit` int(11) NOT NULL,
  `category` int(11) NOT NULL,
  `rak` varchar(255) DEFAULT NULL,
  `image` varchar(255) NOT NULL,
  `stock` varchar(25) NOT NULL,
  `harga_penjualan` varchar(25) NOT NULL,
  `min_stock` varchar(25) NOT NULL,
  `note` text DEFAULT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`id`, `kode`, `nama`, `unit`, `category`, `rak`, `image`, `stock`, `harga_penjualan`, `min_stock`, `note`, `created_at`) VALUES
(30, 'SKU0001', 'Daster B.Rainbow', 23, 9, '7', '20200509_1517081.jpg', '69', '12000', '0', 'Allsize', '2020-06-15'),
(31, 'SKU0002', 'Daster Pink Aksara', 23, 9, '7', 'grosirmaharanysekarsolo_B6vCvoJgBZC1.jpg', '35', '15000', '0', 'Allsize', '2020-06-15');

-- --------------------------------------------------------

--
-- Table structure for table `barang_masuk`
--

CREATE TABLE `barang_masuk` (
  `id` int(11) NOT NULL,
  `kode` varchar(25) NOT NULL,
  `barang` int(11) NOT NULL,
  `supplier` int(11) NOT NULL,
  `harga_pembelian` varchar(25) NOT NULL,
  `total_pembelian` varchar(25) NOT NULL,
  `status` int(11) NOT NULL,
  `jumlah` varchar(25) NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang_masuk`
--

INSERT INTO `barang_masuk` (`id`, `kode`, `barang`, `supplier`, `harga_pembelian`, `total_pembelian`, `status`, `jumlah`, `created_at`) VALUES
(78, 'TR547719', 30, 11, '9000', '0', 1, '50', '2020-06-15'),
(79, 'TR205582', 30, 11, '9000', '0', 2, '30', '2020-06-15'),
(80, 'TR477917', 30, 11, '9000', '0', 1, '20', '2020-06-15'),
(81, 'TR945370', 30, 11, '9000', '0', 2, '5', '2020-06-15'),
(82, 'TR759978', 30, 11, '9000', '0', 2, '10', '2020-06-15'),
(83, 'TR382718', 30, 11, '9000', '0', 2, '10', '2020-06-15'),
(84, 'TR741428', 30, 11, '9000', '0', 1, '3', '2020-06-15'),
(85, 'TR362964', 30, 11, '9000', '0', 2, '12', '2020-06-15'),
(86, 'TR239000', 30, 11, '9000', '0', 1, '5', '2020-06-15'),
(87, 'TR350170', 31, 11, '10000', '0', 1, '50', '2020-06-15'),
(88, 'TR602008', 30, 11, '9000', '0', 1, '20', '2020-06-15'),
(89, 'TR366677', 31, 11, '10000', '0', 2, '20', '2020-06-17');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `nama`) VALUES
(9, 'Daster'),
(10, 'Kain'),
(11, 'Kebaya'),
(12, 'Gamis'),
(13, 'Outer'),
(14, 'Kemeja');

-- --------------------------------------------------------

--
-- Table structure for table `penjualan`
--

CREATE TABLE `penjualan` (
  `id` int(11) NOT NULL,
  `kode` varchar(25) NOT NULL,
  `user` int(11) NOT NULL,
  `total_penjualan` varchar(25) NOT NULL,
  `bill` varchar(100) NOT NULL,
  `kembalian` varchar(100) NOT NULL,
  `pelanggan` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penjualan`
--

INSERT INTO `penjualan` (`id`, `kode`, `user`, `total_penjualan`, `bill`, `kembalian`, `pelanggan`, `created_at`) VALUES
(90, 'TRX186294-20200615', 1, '720000', '720000', '0', 'Pelanggan Terhormat', '2020-06-15 15:26:03'),
(91, 'TRX478972-20200615', 1, '252000', '252000', '0', 'Pelanggan Terhormat', '2020-06-15 15:29:32'),
(92, 'TRX252023-20200615', 1, '450000', '450000', '0', 'Pelanggan Terhormat', '2020-06-15 15:31:35');

-- --------------------------------------------------------

--
-- Table structure for table `penjualan_detail`
--

CREATE TABLE `penjualan_detail` (
  `id` int(11) NOT NULL,
  `barang` int(11) NOT NULL,
  `jumlah` varchar(25) NOT NULL,
  `total` varchar(25) NOT NULL,
  `penjualan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penjualan_detail`
--

INSERT INTO `penjualan_detail` (`id`, `barang`, `jumlah`, `total`, `penjualan`) VALUES
(128, 30, '60', '720000', 90),
(129, 30, '21', '252000', 91),
(130, 31, '30', '450000', 92);

-- --------------------------------------------------------

--
-- Table structure for table `rak`
--

CREATE TABLE `rak` (
  `id` int(11) NOT NULL,
  `kode` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rak`
--

INSERT INTO `rak` (`id`, `kode`, `nama`) VALUES
(7, 'RAK0001', 'Rak 1'),
(8, 'RAK0002', 'Rak 2'),
(9, 'RAK0003', 'Rak 3'),
(10, 'RAK0004', 'Rak 4'),
(11, 'RAK0005', 'Rak 5'),
(12, 'RAK0006', 'Rak 6'),
(13, 'RAK0007', 'Rak 7'),
(14, 'RAK0008', 'Rak 8'),
(15, 'RAK0009', 'Rak 9'),
(16, 'RAK0010', 'Rak 10');

-- --------------------------------------------------------

--
-- Table structure for table `refund`
--

CREATE TABLE `refund` (
  `id` int(11) NOT NULL,
  `kode` varchar(25) NOT NULL,
  `barang_masuk` int(11) NOT NULL,
  `jumlah` varchar(25) NOT NULL,
  `supplier` int(11) NOT NULL,
  `harga_pembelian` varchar(225) NOT NULL,
  `note` text NOT NULL,
  `user` int(11) NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `refund`
--

INSERT INTO `refund` (`id`, `kode`, `barang_masuk`, `jumlah`, `supplier`, `harga_pembelian`, `note`, `user`, `created_at`) VALUES
(29, 'RTR205582-151506062020', 79, '3', 11, '9000', 'xxx', 1, '2020-06-15'),
(30, 'RTR945370-151506062020', 81, '2', 11, '9000', 'xxx', 1, '2020-06-15'),
(31, 'RTR759978-151506062020', 82, '5', 11, '9000', 'xxx', 1, '2020-06-15'),
(32, 'RTR382718-151506062020', 83, '3', 11, '9000', 'xxx', 1, '2020-06-15'),
(33, 'RTR362964-151506062020', 85, '2', 11, '9000', 'xxx', 1, '2020-06-15'),
(34, 'RTR366677-171706062020', 89, '5', 11, '10000', 'xxx', 1, '2020-06-17');

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `pemilik` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stock`
--

CREATE TABLE `stock` (
  `id` int(11) NOT NULL,
  `kode` varchar(255) NOT NULL,
  `barang_masuk` int(11) NOT NULL,
  `barang` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `supplier` int(11) NOT NULL,
  `harga_pembelian` varchar(25) NOT NULL,
  `harga_penjualan` varchar(25) NOT NULL,
  `amount` int(11) NOT NULL,
  `balance` int(11) NOT NULL,
  `note` text NOT NULL,
  `total_pembelian` varchar(225) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock`
--

INSERT INTO `stock` (`id`, `kode`, `barang_masuk`, `barang`, `user`, `supplier`, `harga_pembelian`, `harga_penjualan`, `amount`, `balance`, `note`, `total_pembelian`, `status`, `created_at`) VALUES
(214, 'STR547719-151506062020', 78, 30, 1, 11, '9000', '12000', 50, 0, 'Kondisi Ok', '450000', 0, '2020-06-15'),
(215, 'STR205582-151506062020', 79, 30, 1, 11, '9000', '12000', 27, 0, 'Kondisi Ok', '243000', 0, '2020-06-15'),
(216, 'STR477917-151506062020', 80, 30, 1, 11, '9000', '12000', 20, 16, 'Kondisi Ok', '180000', 0, '2020-06-15'),
(217, 'STR945370-151506062020', 81, 30, 1, 11, '9000', '12000', 3, 3, 'Kondisi Ok', '27000', 0, '2020-06-15'),
(218, 'STR759978-151506062020', 82, 30, 1, 11, '9000', '12000', 5, 5, 'Kondisi Ok', '45000', 0, '2020-06-15'),
(219, 'STR382718-151506062020', 83, 30, 1, 11, '9000', '12000', 7, 7, 'Kondisi Ok', '63000', 0, '2020-06-15'),
(220, 'STR741428-151506062020', 84, 30, 1, 11, '9000', '12000', 3, 3, 'Kondisi Ok', '27000', 0, '2020-06-15'),
(221, 'STR362964-151506062020', 85, 30, 1, 11, '9000', '12000', 10, 10, 'Kondisi Ok', '90000', 0, '2020-06-15'),
(222, 'STR239000-151506062020', 86, 30, 1, 11, '9000', '12000', 5, 5, 'Kondisi Ok', '45000', 0, '2020-06-15'),
(223, 'STR350170-151506062020', 87, 31, 1, 11, '10000', '15000', 50, 20, 'Kondisi Ok', '500000', 0, '2020-06-15'),
(224, 'OTR583951', 78, 30, 0, 11, '9000', '12000', -50, -50, 'Barang Ok', '720000', 1, '2020-06-15'),
(225, 'OTR759729', 79, 30, 0, 11, '9000', '12000', -10, -10, 'Barang Ok', '720000', 1, '2020-06-15'),
(226, 'OTR292808', 79, 30, 0, 11, '9000', '12000', -17, -17, 'Barang Ok', '252000', 1, '2020-06-15'),
(227, 'OTR112441', 80, 30, 0, 11, '9000', '12000', -4, -4, 'Barang Ok', '252000', 1, '2020-06-15'),
(228, 'OTR384841', 87, 31, 0, 11, '10000', '15000', -30, -30, 'Barang Ok', '450000', 1, '2020-06-15'),
(229, 'STR602008-151506062020', 88, 30, 1, 11, '9000', '12000', 20, 20, 'Kondisi Ok', '180000', 0, '2020-06-15'),
(230, 'STR366677-171706062020', 89, 31, 1, 11, '10000', '15000', 15, 15, 'Kondisi Ok', '150000', 0, '2020-06-17');

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `id` int(11) NOT NULL,
  `kode` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `no_tlp` varchar(15) NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`id`, `kode`, `nama`, `alamat`, `no_tlp`, `created_at`) VALUES
(10, 'SP931467', 'Pusat Batik Surakarta', 'Jl. Slamet Riyadi', '085432234112', '2020-06-01'),
(11, 'SP905567', 'Industri Supra Txt', 'Jl. Kemangi Laweyan', '087766545611', '2020-06-01');

-- --------------------------------------------------------

--
-- Table structure for table `unit`
--

CREATE TABLE `unit` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `unit`
--

INSERT INTO `unit` (`id`, `nama`) VALUES
(23, 'PCS'),
(24, 'Pack'),
(25, 'Lusin'),
(26, 'Seri');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `level` int(11) NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `name`, `email`, `password`, `image`, `level`, `created_at`) VALUES
(1, 'admingudang', 'Ibu Siti GMH', 'admingudang@mail.com', '21232f297a57a5a743894a0e4a801fc3', '', 1, '2020-03-21'),
(2, 'adminkasir', 'Kasir Maharany', 'adminkasir@mail.com', '21232f297a57a5a743894a0e4a801fc3', '', 2, '2020-03-21'),
(3, 'adminowner', 'Ibu Maliyana', 'adminowner@mail.com', '21232f297a57a5a743894a0e4a801fc3', '', 3, '2020-03-21');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `unit` (`unit`),
  ADD KEY `category` (`category`),
  ADD KEY `rak` (`rak`);

--
-- Indexes for table `barang_masuk`
--
ALTER TABLE `barang_masuk`
  ADD PRIMARY KEY (`id`),
  ADD KEY `barang` (`barang`),
  ADD KEY `supplier` (`supplier`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `penjualan`
--
ALTER TABLE `penjualan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user` (`user`);

--
-- Indexes for table `penjualan_detail`
--
ALTER TABLE `penjualan_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `penjualan` (`penjualan`);

--
-- Indexes for table `rak`
--
ALTER TABLE `rak`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `refund`
--
ALTER TABLE `refund`
  ADD PRIMARY KEY (`id`),
  ADD KEY `barang_masuk` (`barang_masuk`),
  ADD KEY `supplier` (`supplier`),
  ADD KEY `user` (`user`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stock`
--
ALTER TABLE `stock`
  ADD PRIMARY KEY (`id`),
  ADD KEY `barang` (`barang`),
  ADD KEY `user` (`user`),
  ADD KEY `barang_masuk``` (`barang_masuk`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `unit`
--
ALTER TABLE `unit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `barang`
--
ALTER TABLE `barang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `barang_masuk`
--
ALTER TABLE `barang_masuk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `penjualan`
--
ALTER TABLE `penjualan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=93;

--
-- AUTO_INCREMENT for table `penjualan_detail`
--
ALTER TABLE `penjualan_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=131;

--
-- AUTO_INCREMENT for table `rak`
--
ALTER TABLE `rak`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `refund`
--
ALTER TABLE `refund`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stock`
--
ALTER TABLE `stock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=231;

--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `unit`
--
ALTER TABLE `unit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
