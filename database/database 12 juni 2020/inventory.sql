-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 12, 2020 at 08:50 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.3.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `inventory`
--

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `id` int(11) NOT NULL,
  `kode` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `unit` int(11) NOT NULL,
  `category` int(11) NOT NULL,
  `rak` varchar(255) DEFAULT NULL,
  `image` varchar(255) NOT NULL,
  `stock` varchar(25) NOT NULL,
  `harga_penjualan` varchar(25) NOT NULL,
  `harga_grosir` varchar(225) NOT NULL,
  `min_grosir` varchar(225) NOT NULL,
  `min_stock` varchar(25) NOT NULL,
  `note` text DEFAULT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`id`, `kode`, `nama`, `unit`, `category`, `rak`, `image`, `stock`, `harga_penjualan`, `harga_grosir`, `min_grosir`, `min_stock`, `note`, `created_at`) VALUES
(13, 'SKU0001', 'Daster H.Rainbow', 23, 9, '7', '20200509_151708.jpg', '17', '20000', '0', '0', '10', 'Allsize', '2020-06-01'),
(14, 'SKU0002', 'Daster Pink Aksara', 23, 9, '7', 'grosirmaharanysekarsolo_B6vCvoJgBZC.jpg', '69', '15000', '0', '0', '10', 'Allsize', '2020-06-01'),
(16, 'SKU0003', 'Outer Green Motif K', 23, 13, '8', 'grosirmaharanysekarsolo_B3Wg_RDAjaZ.jpg', '22', '70000', '0', '0', '10', 'Allsize', '2020-06-01'),
(17, 'SKU0004', 'Kain Motif Ungu Horizon', 24, 10, '9', 'grosirmaharanysekarsolo_B_XcM5Pg4Yv.jpg', '34', '25000', '0', '0', '0', 'Meter', '2020-06-01'),
(18, 'SKU0005', 'Kain Motif Dayak S', 24, 10, '9', 'grosirmaharanysekarsolo_B-vaLO-AdVX.jpg', '29', '20000', '0', '0', '0', 'Meter', '2020-06-01'),
(19, 'SKU0006', 'Kebaya Hitam H.Dada', 23, 11, '10', 'grosirmaharanysekarsolo_B6LH7uZAear.jpg', '42', '50000', '0', '0', '0', 'Allsize', '2020-06-01'),
(20, 'SKU0007', 'Kebaya Jawa M.K', 23, 14, '10', 'grosirmaharanysekarsolo_B6LJZ6AgvdT.jpg', '39', '50000', '0', '0', '0', 'Allsize', '2020-06-01'),
(21, 'SKU0008', 'Kemeja Batik Motif Hijau B', 23, 14, '11', 'grosirmaharanysekarsolo_B4PxCr7HuBt.jpg', '85', '20000', '0', '0', '10', 'Allsize', '2020-06-01'),
(22, 'SKU0009', 'Kemeja Hitam Putih Guru', 23, 14, '11', '20200429_154331.jpg', '24', '13000', '0', '0', '0', 'Allsize', '2020-06-01'),
(23, 'SKU0010', 'Outer Noah Hitam', 23, 13, '8', '20200509_1517261.jpg', '26', '75000', '0', '0', '10', 'Allsize', '2020-06-01'),
(25, 'SKU0011', 'Gamis Biru Polos PJN', 23, 12, '12', 'grosirmaharanysekarsolo_B9dxTrZgeWe.jpg', '50', '23000', '0', '0', '0', 'Allsize', '2020-06-03');

-- --------------------------------------------------------

--
-- Table structure for table `barang_masuk`
--

CREATE TABLE `barang_masuk` (
  `id` int(11) NOT NULL,
  `kode` varchar(25) NOT NULL,
  `barang` int(11) NOT NULL,
  `supplier` int(11) NOT NULL,
  `harga_pembelian` varchar(25) NOT NULL,
  `total_pembelian` varchar(25) NOT NULL,
  `status` int(11) NOT NULL,
  `jumlah` varchar(25) NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang_masuk`
--

INSERT INTO `barang_masuk` (`id`, `kode`, `barang`, `supplier`, `harga_pembelian`, `total_pembelian`, `status`, `jumlah`, `created_at`) VALUES
(50, 'TR473902', 13, 11, '12000', '0', 1, '50', '2020-06-01'),
(51, 'TR632938', 14, 11, '9000', '0', 1, '50', '2020-06-01'),
(52, 'TR344072', 16, 10, '50000', '0', 1, '50', '2020-06-01'),
(53, 'TR431181', 23, 10, '50000', '0', 1, '50', '2020-06-01'),
(54, 'TR959836', 17, 10, '20000', '0', 1, '50', '2020-06-01'),
(55, 'TR255537', 18, 10, '15000', '0', 1, '50', '2020-06-01'),
(56, 'TR705758', 19, 11, '30000', '0', 1, '50', '2020-06-01'),
(58, 'TR137510', 21, 10, '15000', '0', 1, '50', '2020-06-01'),
(59, 'TR662877', 22, 10, '10000', '0', 1, '50', '2020-06-01'),
(60, 'TR979566', 20, 11, '45000', '0', 1, '50', '2020-06-01'),
(61, 'TR313822', 13, 11, '12000', '0', 2, '30', '2020-06-01'),
(62, 'TR430754', 22, 10, '10000', '0', 2, '30', '2020-06-01'),
(63, 'TR870066', 13, 11, '12000', '0', 1, '20', '2020-06-02'),
(64, 'TR542432', 25, 11, '15000', '0', 1, '50', '2020-06-03'),
(65, 'TR569867', 14, 11, '9000', '0', 1, '30', '2020-06-08'),
(66, 'TR789209', 21, 10, '15000', '0', 2, '30', '2020-06-12'),
(67, 'TR545022', 21, 10, '15000', '0', 1, '20', '2020-06-12');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `nama`) VALUES
(9, 'Daster'),
(10, 'Kain'),
(11, 'Kebaya'),
(12, 'Gamis'),
(13, 'Outer'),
(14, 'Kemeja');

-- --------------------------------------------------------

--
-- Table structure for table `penjualan`
--

CREATE TABLE `penjualan` (
  `id` int(11) NOT NULL,
  `kode` varchar(25) NOT NULL,
  `user` int(11) NOT NULL,
  `total_penjualan` varchar(25) NOT NULL,
  `bill` varchar(100) NOT NULL,
  `kembalian` varchar(100) NOT NULL,
  `pelanggan` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penjualan`
--

INSERT INTO `penjualan` (`id`, `kode`, `user`, `total_penjualan`, `bill`, `kembalian`, `pelanggan`, `created_at`) VALUES
(81, 'TRX659873-20200601', 1, '358000', '358000', '0', 'Pelanggan Terhormat', '2020-06-01 13:39:23'),
(82, 'TRX476206-20200601', 1, '3215000', '3215000', '0', 'Pelanggan Terhormat', '2020-06-01 13:42:33'),
(83, 'TRX653469-20200601', 1, '800000', '800000', '0', 'Pelanggan Terhormat', '2020-06-01 13:48:34'),
(84, 'TRX612205-20200601', 1, '195000', '195000', '0', 'Pelanggan Terhormat', '2020-06-01 13:52:19'),
(85, 'TRX685334-20200601', 1, '247000', '247000', '0', 'Pelanggan Terhormat', '2020-06-01 13:53:47'),
(86, 'TRX757341-20200601', 1, '1540000', '1540000', '0', 'Pelanggan Terhormat', '2020-06-01 13:55:12'),
(87, 'TRX843248-20200602', 1, '400000', '400000', '0', 'Pelanggan Terhormat', '2020-06-02 15:57:57'),
(88, 'TRX173300-20200602', 1, '1350000', '3000000000', '-2998650000', 'Pelanggan Terhormat', '2020-06-02 16:00:45');

-- --------------------------------------------------------

--
-- Table structure for table `penjualan_detail`
--

CREATE TABLE `penjualan_detail` (
  `id` int(11) NOT NULL,
  `barang` int(11) NOT NULL,
  `jumlah` varchar(25) NOT NULL,
  `total` varchar(25) NOT NULL,
  `penjualan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penjualan_detail`
--

INSERT INTO `penjualan_detail` (`id`, `barang`, `jumlah`, `total`, `penjualan`) VALUES
(99, 23, '1', '75000', 81),
(100, 22, '1', '13000', 81),
(101, 21, '1', '20000', 81),
(102, 20, '1', '50000', 81),
(103, 19, '1', '50000', 81),
(104, 18, '1', '20000', 81),
(105, 17, '1', '25000', 81),
(106, 16, '1', '70000', 81),
(107, 14, '1', '15000', 81),
(108, 13, '1', '20000', 81),
(109, 23, '5', '375000', 82),
(110, 22, '15', '195000', 82),
(111, 21, '9', '180000', 82),
(112, 20, '10', '500000', 82),
(113, 19, '7', '350000', 82),
(114, 18, '20', '400000', 82),
(115, 17, '15', '375000', 82),
(116, 16, '5', '350000', 82),
(117, 14, '10', '150000', 82),
(118, 13, '17', '340000', 82),
(119, 13, '40', '800000', 83),
(120, 22, '15', '195000', 84),
(121, 22, '19', '247000', 85),
(122, 16, '22', '1540000', 86),
(123, 13, '20', '400000', 87),
(124, 23, '18', '1350000', 88);

-- --------------------------------------------------------

--
-- Table structure for table `rak`
--

CREATE TABLE `rak` (
  `id` int(11) NOT NULL,
  `kode` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rak`
--

INSERT INTO `rak` (`id`, `kode`, `nama`) VALUES
(7, 'RAK0001', 'Rak 1'),
(8, 'RAK0002', 'Rak 2'),
(9, 'RAK0003', 'Rak 3'),
(10, 'RAK0004', 'Rak 4'),
(11, 'RAK0005', 'Rak 5'),
(12, 'RAK0006', 'Rak 6'),
(13, 'RAK0007', 'Rak 7'),
(14, 'RAK0008', 'Rak 8'),
(15, 'RAK0009', 'Rak 9'),
(16, 'RAK0010', 'Rak 10');

-- --------------------------------------------------------

--
-- Table structure for table `refund`
--

CREATE TABLE `refund` (
  `id` int(11) NOT NULL,
  `kode` varchar(25) NOT NULL,
  `barang_masuk` int(11) NOT NULL,
  `jumlah` varchar(25) NOT NULL,
  `supplier` int(11) NOT NULL,
  `harga_pembelian` varchar(225) NOT NULL,
  `note` text NOT NULL,
  `user` int(11) NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `refund`
--

INSERT INTO `refund` (`id`, `kode`, `barang_masuk`, `jumlah`, `supplier`, `harga_pembelian`, `note`, `user`, `created_at`) VALUES
(25, 'RTR313822-010106062020', 61, '5', 11, '12000', 'Dilengan Sobek ', 1, '2020-06-01'),
(26, 'RTR430754-010106062020', 62, '6', 10, '10000', 'Tidak Sesuai', 1, '2020-06-01'),
(27, 'RTR789209-121206062020', 66, '5', 10, '15000', 'xxxx', 1, '2020-06-12');

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `pemilik` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stock`
--

CREATE TABLE `stock` (
  `id` int(11) NOT NULL,
  `kode` varchar(255) NOT NULL,
  `barang_masuk` int(11) NOT NULL,
  `barang` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `supplier` int(11) NOT NULL,
  `harga_pembelian` varchar(25) NOT NULL,
  `harga_penjualan` varchar(25) NOT NULL,
  `amount` int(11) NOT NULL,
  `balance` int(11) NOT NULL,
  `note` text NOT NULL,
  `total_pembelian` varchar(225) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock`
--

INSERT INTO `stock` (`id`, `kode`, `barang_masuk`, `barang`, `user`, `supplier`, `harga_pembelian`, `harga_penjualan`, `amount`, `balance`, `note`, `total_pembelian`, `status`, `created_at`) VALUES
(146, 'OTR648576', 45, 8, 0, 8, '65000', '100000', -1, -1, 'Barang Ok', '10000', 1, '2020-05-09'),
(147, 'OTR722911', 45, 8, 0, 8, '65000', '100000', -30, -30, 'Barang Ok', '300000', 1, '2020-05-09'),
(148, 'OTR793440', 47, 7, 0, 2, '60000', '80000', -30, -30, 'Barang Ok', '1800000', 1, '2020-05-09'),
(149, 'OTR465481', 44, 6, 0, 8, '35000', '50000', -1, -1, 'Barang Ok', '50000', 1, '2020-05-09'),
(150, 'OTR987987', 47, 7, 0, 2, '60000', '80000', -4, -4, 'Barang Ok', '1800000', 1, '2020-05-09'),
(151, 'OTR483864', 48, 7, 0, 8, '50000', '60000', -26, -26, 'Barang Ok', '1800000', 1, '2020-05-09'),
(152, 'OTR402958', 45, 8, 0, 8, '65000', '100000', -8, -8, 'Barang Ok', '100000', 1, '2020-05-09'),
(153, 'OTR233296', 49, 8, 0, 8, '60000', '10000', -2, -2, 'Barang Ok', '100000', 1, '2020-05-09'),
(154, 'OTR720525', 48, 7, 0, 8, '50000', '60000', -9, -9, 'Barang Ok', '540000', 1, '2020-05-09'),
(155, 'OTR637886', 44, 6, 0, 8, '35000', '50000', -2, -2, 'Barang Ok', '500000', 1, '2020-05-09'),
(156, 'OTR785058', 46, 6, 0, 7, '35000', '50000', -8, -8, 'Barang Ok', '500000', 1, '2020-05-09'),
(157, 'STR473902-010106062020', 50, 13, 1, 11, '12000', '20000', 50, 0, 'Kondisi Ok', '1000000', 0, '2020-06-01'),
(158, 'STR632938-010106062020', 51, 14, 1, 11, '9000', '15000', 50, 39, 'Kondisi Ok', '750000', 0, '2020-06-01'),
(159, 'STR344072-010106062020', 52, 16, 1, 10, '50000', '70000', 50, 22, 'Kondisi Ok', '3500000', 0, '2020-06-01'),
(160, 'STR431181-010106062020', 53, 23, 1, 10, '50000', '75000', 50, 26, 'Kondisi Ok', '3750000', 0, '2020-06-01'),
(161, 'STR959836-010106062020', 54, 17, 1, 10, '20000', '25000', 50, 34, 'Kondisi Ok', '1250000', 0, '2020-06-01'),
(162, 'STR255537-010106062020', 55, 18, 1, 10, '15000', '20000', 50, 29, 'Kondisi Ok', '1000000', 0, '2020-06-01'),
(163, 'STR705758-010106062020', 56, 19, 1, 11, '30000', '50000', 50, 42, 'Kondisi Ok', '2500000', 0, '2020-06-01'),
(164, 'STR137510-010106062020', 58, 21, 1, 10, '15000', '20000', 50, 40, 'Kondisi Ok', '1000000', 0, '2020-06-01'),
(165, 'STR979566-010106062020', 60, 20, 1, 11, '45000', '50000', 50, 39, 'Kondisi Ok', '2500000', 0, '2020-06-01'),
(166, 'STR662877-010106062020', 59, 22, 1, 10, '10000', '13000', 50, 0, 'Kondisi Ok', '650000', 0, '2020-06-01'),
(167, 'OTR903875', 53, 23, 0, 10, '50000', '75000', -1, -1, 'Barang Ok', '75000', 1, '2020-06-01'),
(168, 'OTR731592', 59, 22, 0, 10, '10000', '13000', -1, -1, 'Barang Ok', '13000', 1, '2020-06-01'),
(169, 'OTR615404', 58, 21, 0, 10, '15000', '20000', -1, -1, 'Barang Ok', '20000', 1, '2020-06-01'),
(170, 'OTR848391', 60, 20, 0, 11, '45000', '50000', -1, -1, 'Barang Ok', '50000', 1, '2020-06-01'),
(171, 'OTR318807', 56, 19, 0, 11, '30000', '50000', -1, -1, 'Barang Ok', '50000', 1, '2020-06-01'),
(172, 'OTR199953', 55, 18, 0, 10, '15000', '20000', -1, -1, 'Barang Ok', '20000', 1, '2020-06-01'),
(173, 'OTR857388', 54, 17, 0, 10, '20000', '25000', -1, -1, 'Barang Ok', '25000', 1, '2020-06-01'),
(174, 'OTR633963', 52, 16, 0, 10, '50000', '70000', -1, -1, 'Barang Ok', '70000', 1, '2020-06-01'),
(175, 'OTR965362', 51, 14, 0, 11, '9000', '15000', -1, -1, 'Barang Ok', '15000', 1, '2020-06-01'),
(176, 'OTR845354', 50, 13, 0, 11, '12000', '20000', -1, -1, 'Barang Ok', '20000', 1, '2020-06-01'),
(177, 'OTR691895', 53, 23, 0, 10, '50000', '75000', -5, -5, 'Barang Ok', '375000', 1, '2020-06-01'),
(178, 'OTR600381', 59, 22, 0, 10, '10000', '13000', -15, -15, 'Barang Ok', '195000', 1, '2020-06-01'),
(179, 'OTR725468', 58, 21, 0, 10, '15000', '20000', -9, -9, 'Barang Ok', '180000', 1, '2020-06-01'),
(180, 'OTR431114', 60, 20, 0, 11, '45000', '50000', -10, -10, 'Barang Ok', '500000', 1, '2020-06-01'),
(181, 'OTR706186', 56, 19, 0, 11, '30000', '50000', -7, -7, 'Barang Ok', '350000', 1, '2020-06-01'),
(182, 'OTR171307', 55, 18, 0, 10, '15000', '20000', -20, -20, 'Barang Ok', '400000', 1, '2020-06-01'),
(183, 'OTR358335', 54, 17, 0, 10, '20000', '25000', -15, -15, 'Barang Ok', '375000', 1, '2020-06-01'),
(184, 'OTR700255', 52, 16, 0, 10, '50000', '70000', -5, -5, 'Barang Ok', '350000', 1, '2020-06-01'),
(185, 'OTR977155', 51, 14, 0, 11, '9000', '15000', -10, -10, 'Barang Ok', '150000', 1, '2020-06-01'),
(186, 'OTR844543', 50, 13, 0, 11, '12000', '20000', -17, -17, 'Barang Ok', '340000', 1, '2020-06-01'),
(187, 'STR313822-010106062020', 61, 13, 1, 11, '12000', '20000', 25, 0, 'Kondisi Ok', '500000', 0, '2020-06-01'),
(188, 'OTR402631', 50, 13, 0, 11, '12000', '20000', -32, -32, 'Barang Ok', '800000', 1, '2020-06-01'),
(189, 'OTR783937', 61, 13, 0, 11, '12000', '20000', -8, -8, 'Barang Ok', '800000', 1, '2020-06-01'),
(190, 'OTR129853', 59, 22, 0, 10, '10000', '13000', -15, -15, 'Barang Ok', '195000', 1, '2020-06-01'),
(191, 'OTR324271', 59, 22, 0, 10, '10000', '13000', -19, -19, 'Barang Ok', '247000', 1, '2020-06-01'),
(192, 'OTR988241', 52, 16, 0, 10, '50000', '70000', -22, -22, 'Barang Ok', '1540000', 1, '2020-06-01'),
(193, 'STR430754-010106062020', 62, 22, 1, 10, '10000', '13000', 24, 24, 'Kondisi Ok', '312000', 0, '2020-06-01'),
(194, 'STR870066-020206062020', 63, 13, 1, 11, '12000', '20000', 20, 17, 'Kondisi Ok', '400000', 0, '2020-06-02'),
(195, 'OTR703149', 61, 13, 0, 11, '12000', '20000', -17, -17, 'Barang Ok', '400000', 1, '2020-06-02'),
(196, 'OTR848709', 63, 13, 0, 11, '12000', '20000', -3, -3, 'Barang Ok', '400000', 1, '2020-06-02'),
(197, 'OTR846451', 53, 23, 0, 10, '50000', '75000', -18, -18, 'Barang Ok', '1350000', 1, '2020-06-02'),
(198, 'STR542432-030306062020', 64, 25, 1, 11, '15000', '23000', 50, 50, 'Kondisi Ok', '1150000', 0, '2020-06-03'),
(199, 'STR569867-080806062020', 65, 14, 1, 11, '9000', '15000', 30, 30, 'Kondisi Ok', '450000', 0, '2020-06-08'),
(200, 'STR789209-121206062020', 66, 21, 1, 10, '15000', '20000', 25, 25, 'Kondisi Ok', '500000', 0, '2020-06-12'),
(201, 'STR545022-121206062020', 67, 21, 1, 10, '15000', '20000', 20, 20, 'Kondisi Ok', '400000', 0, '2020-06-12');

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `id` int(11) NOT NULL,
  `kode` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `no_tlp` varchar(15) NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`id`, `kode`, `nama`, `alamat`, `no_tlp`, `created_at`) VALUES
(10, 'SP931467', 'Pusat Batik Surakarta', 'Jl. Slamet Riyadi', '085432234112', '2020-06-01'),
(11, 'SP905567', 'Industri Supra Txt', 'Jl. Kemangi Laweyan', '087766545611', '2020-06-01');

-- --------------------------------------------------------

--
-- Table structure for table `unit`
--

CREATE TABLE `unit` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `unit`
--

INSERT INTO `unit` (`id`, `nama`) VALUES
(23, 'PCS'),
(24, 'Pack'),
(25, 'Lusin'),
(26, 'Seri');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `level` int(11) NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `name`, `email`, `password`, `image`, `level`, `created_at`) VALUES
(1, 'admingudang', 'Ibu Siti GMH', 'admingudang@mail.com', '21232f297a57a5a743894a0e4a801fc3', '', 1, '2020-03-21'),
(2, 'adminkasir', 'Kasir Maharany', 'adminkasir@mail.com', '21232f297a57a5a743894a0e4a801fc3', '', 2, '2020-03-21'),
(3, 'adminowner', 'Ibu Maliyana', 'adminowner@mail.com', '21232f297a57a5a743894a0e4a801fc3', '', 3, '2020-03-21');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `unit` (`unit`),
  ADD KEY `category` (`category`),
  ADD KEY `rak` (`rak`);

--
-- Indexes for table `barang_masuk`
--
ALTER TABLE `barang_masuk`
  ADD PRIMARY KEY (`id`),
  ADD KEY `barang` (`barang`),
  ADD KEY `supplier` (`supplier`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `penjualan`
--
ALTER TABLE `penjualan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user` (`user`);

--
-- Indexes for table `penjualan_detail`
--
ALTER TABLE `penjualan_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `penjualan` (`penjualan`);

--
-- Indexes for table `rak`
--
ALTER TABLE `rak`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `refund`
--
ALTER TABLE `refund`
  ADD PRIMARY KEY (`id`),
  ADD KEY `barang_masuk` (`barang_masuk`),
  ADD KEY `supplier` (`supplier`),
  ADD KEY `user` (`user`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stock`
--
ALTER TABLE `stock`
  ADD PRIMARY KEY (`id`),
  ADD KEY `barang` (`barang`),
  ADD KEY `user` (`user`),
  ADD KEY `barang_masuk``` (`barang_masuk`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `unit`
--
ALTER TABLE `unit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `barang`
--
ALTER TABLE `barang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `barang_masuk`
--
ALTER TABLE `barang_masuk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `penjualan`
--
ALTER TABLE `penjualan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;

--
-- AUTO_INCREMENT for table `penjualan_detail`
--
ALTER TABLE `penjualan_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=125;

--
-- AUTO_INCREMENT for table `rak`
--
ALTER TABLE `rak`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `refund`
--
ALTER TABLE `refund`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stock`
--
ALTER TABLE `stock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=202;

--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `unit`
--
ALTER TABLE `unit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
